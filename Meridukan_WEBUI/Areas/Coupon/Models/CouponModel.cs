﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Coupon.Models
{
    public class CouponModel
    {
        public int Id { get; set; }
        public string CouponCode { get; set; }
        public int DiscountTypeId { get; set; }
        public string DiscountType { get; set; }
        public decimal CouponAmt { get; set; }
        public bool IsfreeShipping { get; set; }
        public DateTime ExpiryDate { get; set; }
        public decimal MinimumSpend { get; set; }
        public decimal MaximumSpend { get; set; }
        public List<CouponProductsModel> couponIncludeProductList { get; set; }
        public List<CouponProductsModel> couponExcludeProductList { get; set; }
        public List<CouponCategoryModel> couponIncludeCategoryList { get; set; }
        public List<CouponCategoryModel> couponExcludeCategoryList { get; set; }

        public int UserId { get; set; }
        public string UserName { get; set; }
    }

    public class CouponProductsModel
    {
        public int Id { get; set; }
        public int CouponId { get; set; }
        public int ProductId { get; set; }
        public int PrdAttributeId { get; set; }        
    }

    public class CouponCategoryModel
    {
        public int Id { get; set; }
        public int CouponId { get; set; }
        public int CategoryId { get; set; }
    }

    public class DeleteCoupon
    {
        public int Id { get; set; }
    }


}