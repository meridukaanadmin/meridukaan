﻿using Meridukan_WEBUI.Areas.Category.Models;
using Meridukan_WEBUI.Areas.Coupon.Models;
using Meridukan_WEBUI.Areas.DiscountType.Models;
using Meridukan_WEBUI.Areas.Sale.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Coupon.Controllers
{
    public class CouponController : BaseController
    {
        // GET: Coupon/Coupon
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult GetCouponList()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Coupon";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
               ViewData["GetCouponList"] = GetCouponDataList();
                return View("GetCouponList");
            }
        }

        private static List<CouponModel> GetCouponDataList()
        {
            List<CouponModel> Model = new List<CouponModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Coupon/GetCouponList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<CouponModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        public ActionResult AddCoupon(int Id)
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Coupon";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }

            CouponModel coupon = new CouponModel();
            coupon.couponIncludeProductList = new List<CouponProductsModel>();
            coupon.couponExcludeProductList = new List<CouponProductsModel>();
            coupon.couponIncludeCategoryList = new List<CouponCategoryModel>();
            coupon.couponExcludeCategoryList = new List<CouponCategoryModel>();
            if (Id > 0)
            {
                coupon = GetCouponDetails(Id);
            }
            else
            {
                coupon.ExpiryDate = DateTime.Now;
            }

            ViewBag.DiscountTypeList = new SelectList(GetDiscountTypeList(), "Id", "Name");
            ViewBag.ProductList = GetProductList();
            ViewBag.CategoryList = GetCategoryList();
            return View("AddCoupon", coupon);
        }

        private static CouponModel GetCouponDetails(int Id)
        {
            CouponModel Model = new CouponModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Coupon/GetCouponDetails?Id=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<CouponModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        private static List<SaleProductModel> GetProductList()
        {
            List<SaleProductModel> Model = new List<SaleProductModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Product/GetSaleProductList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SaleProductModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        private static List<CategoryModel> GetCategoryList()
        {
            List<CategoryModel> catgoryModel = new List<CategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Category/GetCategoryList").Result;

                if (response.IsSuccessStatusCode)
                {
                    catgoryModel = response.Content.ReadAsAsync<List<CategoryModel>>().Result;
                }
                else
                {
                    return catgoryModel;
                }
            }
            catch (Exception ex)
            {
                return catgoryModel;
                throw ex;
            }
            return catgoryModel;
        }

        #region get all DiscountTypeModel list details 
        private static List<DiscountTypeModel> GetDiscountTypeList()
        {
            List<DiscountTypeModel> Model = new List<DiscountTypeModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/DiscountType/GetDiscountTypeList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<DiscountTypeModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        [HttpPost]
        public JsonResult Save(CouponModel coupon)
        {
            ViewBag.UserId = coupon.UserId;
            ViewBag.UserName = coupon.UserName;

            ViewBag.ActivePage = "Coupon";

            long res = 0;

            if (ModelState.IsValid)
            {
                try
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Coupon/AddorUpdateCouponDetails/", coupon, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = 0;
                        }
                        else
                        {
                            var saleEntry = response.Content.ReadAsStringAsync().Result;
                            res = 1;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList();

                //return View("AddPO", purchase);
            }
            return Json(res);
        }

        public object DeleteCoupon(DeleteCoupon delete)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Coupon/DeleteCouponDetails", delete, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }
    }
}