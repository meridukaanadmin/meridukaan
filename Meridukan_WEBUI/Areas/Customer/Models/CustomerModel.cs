﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_WEBUI.Areas.Customer.Models
{
    public class CustomerModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string Address { get; set; }
        public string PinCode { get; set; }
        public string EmailId { get; set; }
    }

    public class DeleteCustomer
    {
        public int Id { get; set; }
    }
}