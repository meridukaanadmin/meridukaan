﻿using Meridukan_WEBUI.Areas.Customer.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Customer.Controllers
{
    public class CustomerController : BaseController
    {
        // GET: Customer/Customer
        public ActionResult Index()
        {
            return View();
        }

        // GET: Customer/Customer
        public ActionResult GetCustomer()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Customer";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetCustomerList"] = GetCustomerList();
                return View("Customer");
            }
        }

        public ActionResult CustomerDetails(int Id)
        {
            CustomerModel customer = new CustomerModel();            
            if (Id > 0)
            {
                customer = GetCustomerDetails(Id);
            }

            return PartialView("CustomerDetails", customer);
        }

        public object AddOrUpdateCustomer(CustomerModel customer)
        {
            bool res = false;

            object data = new object();
            string desc = "";
            try
            {
                if (ModelState.IsValid)
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Customer/AddorUpdateCustomer/", customer, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = false;
                            desc = "Error Occured";
                        }
                        else
                        {
                            var brandEntry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            desc = "Saved Successfully";
                        }
                    }
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList();

                    res = false;
                    desc = "Error Occured";
                    // return View("Brand", brand);
                }

                data = new
                {
                    Result = res,
                    Description = desc
                };

                return res;
            }
            catch (Exception ex)
            {
                data = new
                {
                    Result = res,
                    Description = ex.Message
                };

                return res;
            }
        }

        public object DeleteBrand(DeleteCustomer delete)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Customer/DeleteCustomer", delete, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }

        private static List<CustomerModel> GetCustomerList()
        {
            List<CustomerModel> Model = new List<CustomerModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Customer/GetCustomerList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<CustomerModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        private static CustomerModel GetCustomerDetails(int Id)
        {
            CustomerModel Model = new CustomerModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Customer/GetCustomerDetails?Id=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<CustomerModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
    }
}