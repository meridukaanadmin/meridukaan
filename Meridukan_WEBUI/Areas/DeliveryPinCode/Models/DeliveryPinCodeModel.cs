﻿namespace Meridukan_WEBUI.Areas.DeliveryPinCode.Models
{
    public class DeliveryPinCodeModel
    {
        public int Id { get; set; }
        public string PostalCode { get; set; }
        public string Region { get; set; }
        public string AreaName { get; set; }
    }

    public class DeletePinCode
    {
        public int Id { get; set; }
    }
}