﻿using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.DeliveryPinCode
{
    public class DeliveryPinCodeAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "DeliveryPinCode";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "DeliveryPinCode_default",
                "DeliveryPinCode/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new { controller = "DeliveryPinCode" },
                new[] { "Meridukan_WEBUI.Areas.DeliveryPinCode.Controllers" }
            );
        }
    }
}