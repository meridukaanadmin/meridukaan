﻿using Meridukan_WEBUI.Areas.DeliveryPinCode.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.DeliveryPinCode.Controllers
{
    public class DeliveryPinCodeController : BaseController
    {
        List<DeliveryPinCodeModel> pins;

        public DeliveryPinCodeController()
        {
            pins = new List<DeliveryPinCodeModel>();
        }

        // GET: DeliveryPinCode/DeliveryPinCode
        public ActionResult Index()
        {
            return View();
        }

        // GET: Brand/Brand
        public ActionResult GetDeliveryPinCode()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "DeliveryPinCode";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetPinCodeList"] = pins = GetDeliveryPinList();
                return View();
            }
        }

        public ActionResult PinCodeDetails(int Id)
        {
            DeliveryPinCodeModel brand = new DeliveryPinCodeModel();
            //frnds = db.FriendsInfo.Find(Id);
            if (Id > 0)
            {
                brand = GetDeliveryPinDetails(Id);
            }

            return PartialView("PinCodeDetails", brand);
        }

        public object AddOrUpdateDeliveryPinCode(DeliveryPinCodeModel brand)
        {
            bool res = false;

            object data = new object();
            string desc = "";
            try
            {
                if (ModelState.IsValid)
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/DeliveryPinCode/AddorUpdateDeliveryPinCodeDetails/", brand, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = false;
                            desc = "Error Occured";
                        }
                        else
                        {
                            var brandEntry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            desc = "Saved Successfully";
                        }
                    }
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList();

                    res = false;
                    desc = "Error Occured";
                    // return View("Brand", brand);
                }

                data = new
                {
                    Result = res,
                    Description = desc
                };

                return res;
            }
            catch (Exception ex)
            {
                data = new
                {
                    Result = res,
                    Description = ex.Message
                };

                return res;
            }
        }

        public object DeletePin(DeletePinCode delete)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/DeliveryPinCode/DeleteDeliveryPinCodeDetails", delete, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }       

        #region get all Pin list details 
        private static List<DeliveryPinCodeModel> GetDeliveryPinList()
        {
            List<DeliveryPinCodeModel> Model = new List<DeliveryPinCodeModel>();

            try
            {  

                HttpResponseMessage response = httpClient.GetAsync("api/DeliveryPinCode/GetDeliveryPinCodeList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<DeliveryPinCodeModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get pin details 
        private static DeliveryPinCodeModel GetDeliveryPinDetails(int Id)
        {
            DeliveryPinCodeModel Model = new DeliveryPinCodeModel();

            try
            {  
                HttpResponseMessage response = httpClient.GetAsync("api/DeliveryPinCode/GetDeliveryPinCodeDetails?Id=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<DeliveryPinCodeModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion
    }
}