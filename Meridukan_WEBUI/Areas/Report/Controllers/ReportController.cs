﻿using Meridukan_WEBUI.Areas.Report.Models;
using Meridukan_WEBUI.Controllers;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using ZXing;

namespace Meridukan_WEBUI.Areas.Report.Controllers
{
    public class ReportController : BaseController
    {
        List<CustomerReportModel> customers;
        public ReportController()
        {
            customers = new List<CustomerReportModel>();
        }

        // GET: Report/Report
        public ActionResult Index()
        {
            return View();
        }

        // GET: Report/LedgerReport
        public ActionResult LedgerReport()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Ledger";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetLedgerList"] = GetLedgerData();
                return View();
            }
        }

        private static List<TransactionReportModel> GetLedgerData()
        {
            List<TransactionReportModel> Model = new List<TransactionReportModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Report/GeLedgerReportList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<TransactionReportModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        //GET : Report/CustomerList
        public ActionResult CustomerList()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "CustomerReport";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetCustomerList"] = GetCustomerReportData();
                return View();
            }
        }

        private static List<CustomerReportModel> GetCustomerReportData()
        {
            List<CustomerReportModel> Model = new List<CustomerReportModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Customer/GetCustomerReportList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<CustomerReportModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        public ActionResult GetCustomerListSearch(string Contact)
        {
            customers = GetCustomerReportData();
            customers = customers.Where(d => d.Contact == Contact).ToList();
            return Json(customers, JsonRequestBehavior.AllowGet);
        }

        //QR sample code
        public ActionResult QrGeneration()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "qr";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {   
                return View();
            }
        }

        [HttpPost]
        public ActionResult QrGeneration(string qrcode)
        {
            string folderPath = "~/Uploads/";
            string imagePath = "~/Uploads/QrCode.jpg";
            // If the directory doesn't exist then create it.
            if (!Directory.Exists(Server.MapPath(folderPath)))
            {
                Directory.CreateDirectory(Server.MapPath(folderPath));
            }

            var barcodeWriter = new BarcodeWriter();
            barcodeWriter.Format = BarcodeFormat.QR_CODE;
            var result = barcodeWriter.Write(qrcode);

            string barcodePath = Server.MapPath(imagePath);
            var barcodeBitmap = new Bitmap(result);
            using (MemoryStream memory = new MemoryStream())
            {
                using (FileStream fs = new FileStream(barcodePath, FileMode.Create, FileAccess.ReadWrite))
                {
                    barcodeBitmap.Save(memory, ImageFormat.Jpeg);
                    byte[] bytes = memory.ToArray();
                    fs.Write(bytes, 0, bytes.Length);
                }
            }

            ViewBag.QRCodeImage = imagePath;
            
            string barcodeText = "";
            imagePath = "~/Uploads/QrCode.jpg";
            barcodePath = Server.MapPath(imagePath);
            var barcodeReader = new BarcodeReader();

            var res = barcodeReader.Decode(new Bitmap(barcodePath));
            if (res != null)
            {
                ViewBag.DecodeQRCodeImage = res.Text;
            }

            //QRCodeGenerator ObjQr = new QRCodeGenerator();

            //QRCodeData qrCodeData = ObjQr.CreateQrCode(qrcode, QRCodeGenerator.ECCLevel.Q);

            //Bitmap bitMap = new QRCode(qrCodeData).GetGraphic(20);

            //using (MemoryStream ms = new MemoryStream())
            //{
            //    bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            //    byte[] byteImage = ms.ToArray();
            //    ViewBag.QRCodeImage = "data:image/png;base64," + Convert.ToBase64String(byteImage);
            //}


            return View();
        }
    }
}