﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_WEBUI.Areas.Report.Models
{
    public class ReportModel
    {
    }

    public class TransactionReportModel
    {
        public int TransId { get; set; }
        public DateTime date { get; set; }
        public string Type { get; set; }
        public string Reference { get; set; }
        public int ReferenceId { get; set; }
        public string Supplier { get; set; }
        public string Customer { get; set; }
        public decimal Amount { get; set; }
    }

    public class CustomerReportModel
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string EmailId { get; set; }
        public string Address { get; set; }
        public int Totalorders { get; set; }
        public decimal Totalspend { get; set; }
    }
}