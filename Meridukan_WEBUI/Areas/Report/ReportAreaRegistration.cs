﻿using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Report
{
    public class ReportAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Report";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Report_default",
                "Report/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new { controller = "Report" },
                new[] { "Meridukan_WEBUI.Areas.Report.Controllers" }
            );
        }
    }
}