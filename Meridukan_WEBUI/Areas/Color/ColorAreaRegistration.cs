﻿using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Color
{
    public class ColorAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Color";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Color_default",
                "Color/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new { controller = "Color" },
                new[] { "Meridukan_WEBUI.Areas.Color.Controllers" }
            );
        }
    }
}