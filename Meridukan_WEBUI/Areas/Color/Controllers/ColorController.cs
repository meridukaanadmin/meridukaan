﻿using Meridukan_WEBUI.Areas.Color.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Color.Controllers
{
    public class ColorController : BaseController
    {
        // GET: Color/Color
        public ActionResult Index()
        {
            return View();
        }

        // GET: Color/Color
        public ActionResult GetColor()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Color";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetColorList"] = GetColorList();
                return View("Color");
            }
        }

        public ActionResult ColorDetails(int Id)
        {
            ColorModel color = new ColorModel();
            if (Id > 0)
            {
                color = GetColorDetails(Id);
            }

            return PartialView("ColorDetails", color);
        }

        public object AddOrUpdateColor(ColorModel color)
        {
            bool res = false;

            object data = new object();
            string desc = "";
            try
            {
                if (string.IsNullOrEmpty(color.Name))
                {
                    desc = "Color Name is required";
                    ModelState.AddModelError("Name", "Color Name is required");
                }               

                if (ModelState.IsValid)
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Color/AddorUpdateColor", color, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = false;
                            desc = "Error Occured";
                        }
                        else
                        {
                            var brandEntry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            desc = "Saved Successfully";
                        }
                    }
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList();

                    res = false;
                    desc = "Error Occured";
                    // return View("Brand", brand);
                }

                data = new
                {
                    Result = res,
                    Description = desc
                };

                return res;
            }
            catch (Exception ex)
            {
                data = new
                {
                    Result = res,
                    Description = ex.Message
                };

                return res;
            }
        }

        public object DeleteColor(DeleteColor delete)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Color/DeleteColor", delete, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }

        #region get all Color list details 
        private static List<ColorModel> GetColorList()
        {
            List<ColorModel> sizeModel = new List<ColorModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Color/GetColorList").Result;

                if (response.IsSuccessStatusCode)
                {
                    sizeModel = response.Content.ReadAsAsync<List<ColorModel>>().Result;
                }
                else
                {
                    return sizeModel;
                }
            }
            catch (Exception ex)
            {
                return sizeModel;
                throw ex;
            }
            return sizeModel;
        }
        #endregion

        #region get color details 
        private static ColorModel GetColorDetails(int Id)
        {
            ColorModel Model = new ColorModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Color/GetColorDetails?Id=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<ColorModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion
    }
}