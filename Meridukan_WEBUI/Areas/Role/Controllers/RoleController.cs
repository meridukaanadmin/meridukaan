﻿using Meridukan_WEBUI.Areas.User.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Role.Controllers
{
    public class RoleController : BaseController
    {
        List<RoleModel> roles;

        public RoleController()
        {
            roles = new List<RoleModel>();
        }

        // GET: Role/Role
        public ActionResult Index()
        {
            return View();
        }

        // GET: Role/Role
        public ActionResult Role()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Role";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetRoleList"] = roles = GetRoleList();
                return View();
            }
        }

        public ActionResult RoleDetails(int Id)
        {
            RoleModel role = new RoleModel();            
            if (Id > 0)
            {
                role = GetRoleDetails(Id);
            }

            return PartialView("RoleDetails", role);
        }

        public object AddOrUpdateRole(RoleModel role)
        {
            bool res = false;

            object data = new object();
            string desc = "";
            try
            {
                if (string.IsNullOrEmpty(role.Name))
                {
                    desc = "Role Name is required";
                    
                }

                var lst = roles.Where(d => d.Name == role.Name);

                if (lst.Count() > 0)
                {
                    desc = "Role Name already exists";
                    ModelState.AddModelError("Exists", "Role Name already exists");
                }

                if (ModelState.IsValid)
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Role/AddorUpdateRole/", role, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = false;
                            desc = "Error Occured";
                        }
                        else
                        {
                            var brandEntry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            desc = "Role Saved Successfully";
                        }
                    }
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList();

                    res = false;
                    desc = "Error Occured";
                    // return View("Brand", brand);
                }

                data = new
                {
                    Result = res,
                    Description = desc
                };

                return res;
            }
            catch (Exception ex)
            {
                data = new
                {
                    Result = res,
                    Description = ex.Message
                };

                return res;
            }
        }

        public object DeleteRole(DeleteRole deleterole)
        {
            bool res = false;

            try
            { 

                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Role/DeleteRole", deleterole, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }

        #region get all Role list details 
        private static List<RoleModel> GetRoleList()
        {
            List<RoleModel> roleModel = new List<RoleModel>();

            try
            {               
                HttpResponseMessage response = httpClient.GetAsync("api/Role/GetRoleList").Result;

                if (response.IsSuccessStatusCode)
                {
                    roleModel = response.Content.ReadAsAsync<List<RoleModel>>().Result;
                }
                else
                {
                    return roleModel;
                }
            }
            catch (Exception ex)
            {
                return roleModel;
                throw ex;
            }
            return roleModel;
        }
        #endregion

        #region get Role details 
        private static RoleModel GetRoleDetails(int Id)
        {
            RoleModel roleModel = new RoleModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Role/GetRoleDetails?RoleId=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    roleModel = response.Content.ReadAsAsync<RoleModel>().Result;
                }
                else
                {
                    return roleModel;
                }
            }
            catch (Exception ex)
            {
                return roleModel;
                throw ex;
            }
            return roleModel;
        }
        #endregion
    }
}