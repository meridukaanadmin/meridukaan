﻿using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.GRN
{
    public class GRNAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "GRN";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "GRN_default",
                "GRN/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new { controller = "GRN" },
                new[] { "Meridukan_WEBUI.Areas.GRN.Controllers" }
            );
        }
    }
}