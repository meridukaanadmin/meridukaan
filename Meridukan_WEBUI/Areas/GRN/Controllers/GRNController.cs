﻿using Meridukan_WEBUI.Areas.Brand.Models;
using Meridukan_WEBUI.Areas.Category.Models;
using Meridukan_WEBUI.Areas.Color.Models;
using Meridukan_WEBUI.Areas.GRN.Models;
using Meridukan_WEBUI.Areas.Size.Models;
using Meridukan_WEBUI.Areas.Subcategory.Models;
using Meridukan_WEBUI.Areas.Supplier.Models;
using Meridukan_WEBUI.Areas.Tax.Models;
using Meridukan_WEBUI.Areas.Transport.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.GRN.Controllers
{
    public class GRNController : BaseController
    {
        List<GRNModel> grn;
        List<TransportModel> transportModels;
        public GRNController()
        {
            grn = new List<GRNModel>();
            transportModels = new List<TransportModel>();
        }

        // GET: GRN/GRN
        public ActionResult Index()
        {
            return View();
        }

        // GET: GRN/GRN
        public ActionResult GetGRN()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "GRN";

            GRNListSearch gRNList = new GRNListSearch();

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                grn = GetGRNList();

                DateTime now = DateTime.Now;
                var startDate = new DateTime(now.Year, now.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);

                gRNList.FromDate = Convert.ToDateTime(startDate);
                gRNList.ToDate = Convert.ToDateTime(endDate);

                grn = grn.Where(d => d.GrnDate >= gRNList.FromDate && d.PoDate <= gRNList.ToDate).ToList();

                ViewData["GetGRNList"] = grn;
                return View("GRN", gRNList);
            }
        }

        // GET: GRN
        public ActionResult AddGRN(int GRNId)
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "GRN";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                GRNModel gRN = new GRNModel();
                gRN.GRNDetails = new List<GRNDetailModel>();

                if (GRNId > 0)
                {
                    gRN = GetGRNDetails(GRNId);
                }
                else
                {
                    gRN.GrnDate = DateTime.Today.AddDays(0);
                    //gRN.LR_No = "";
                    //gRN.PoDate = DateTime.Today.AddDays(0);
                    //gRN.SupplierInvDate = gRN.LR_Date = DateTime.Today.AddDays(0);
                }

                ViewBag.POList = new SelectList(GetPurchaseList(), "PoId", "PoNo");
                ViewBag.SupplierList = new SelectList(GetSupplierList(), "Id", "Name");
                ViewBag.CategoryList = new SelectList(GetCategoryList(), "Id", "Name");
                ViewBag.SubCategoryList = new SelectList(GetSubcategoryList(), "Id", "Name");
                ViewBag.BrandList = new SelectList(GetBrandList(), "Id", "Name");
                ViewBag.TaxList = new SelectList(GetTaxList(), "Id", "Name");
                ViewBag.TransportList = new SelectList(GetTransportList(), "Id", "Partner");
                ViewBag.ColorList = new SelectList(GetColorList(), "Id", "Name");
                ViewBag.SizeList = new SelectList(GetSizeList(), "Id", "Name");

                return View("AddGRN", gRN);
            }
        }

        [HttpPost]
        public JsonResult Save(AddGRNEntities grn)
        {
            ViewBag.UserId = grn.UserId;
            ViewBag.UserName = grn.UserName;

            ViewBag.ActivePage = "Purchase";

            bool res = false;

            //if (ModelState.IsValid)
            //{
            try
            {
                using (httpClient)
                {
                    HttpResponseMessage response = httpClient.PostAsync("api/GRN/AddorUpdateGRN/", grn, new JsonMediaTypeFormatter()).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var userEntry = response.Content.ReadAsStringAsync().Result;
                        res = true;
                        // long res = Convert.ToInt64(userEntry);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //}
            //else
            //{
            //    var errors = ModelState.Select(x => x.Value.Errors)
            //               .Where(y => y.Count > 0)
            //               .ToList();

            //    //return View("AddPO", purchase);
            //}
            return Json(res);
        }

        #region 
        private static List<GRNModel> GetPurchaseList()
        {
            List<GRNModel> Model = new List<GRNModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Purchase/GetPurchaseList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<GRNModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        private static List<GRNModel> GetGRNList()
        {
            List<GRNModel> Model = new List<GRNModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/GRN/GetGRNList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<GRNModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        #region get all Size list details 
        private static List<SizeModel> GetSizeList()
        {
            List<SizeModel> sizeModel = new List<SizeModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Size/GetSizeList").Result;

                if (response.IsSuccessStatusCode)
                {
                    sizeModel = response.Content.ReadAsAsync<List<SizeModel>>().Result;
                }
                else
                {
                    return sizeModel;
                }
            }
            catch (Exception ex)
            {
                return sizeModel;
                throw ex;
            }
            return sizeModel;
        }
        #endregion

        #region get all Color list details 
        private static List<ColorModel> GetColorList()
        {
            List<ColorModel> colorModel = new List<ColorModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Color/GetColorList").Result;

                if (response.IsSuccessStatusCode)
                {
                    colorModel = response.Content.ReadAsAsync<List<ColorModel>>().Result;
                }
                else
                {
                    return colorModel;
                }
            }
            catch (Exception ex)
            {
                return colorModel;
                throw ex;
            }
            return colorModel;
        }
        #endregion

        #region get all Supplier list details 
        private static List<SupplierModel> GetSupplierList()
        {
            List<SupplierModel> Model = new List<SupplierModel>();

            try
            {
                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("Application/json"));

                HttpResponseMessage response = httpClient.GetAsync("api/Supplier/GetSupplierList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SupplierModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get all category list details 
        private static List<CategoryModel> GetCategoryList()
        {
            List<CategoryModel> catgoryModel = new List<CategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Category/GetCategoryList").Result;

                if (response.IsSuccessStatusCode)
                {
                    catgoryModel = response.Content.ReadAsAsync<List<CategoryModel>>().Result;
                }
                else
                {
                    return catgoryModel;
                }
            }
            catch (Exception ex)
            {
                return catgoryModel;
                throw ex;
            }
            return catgoryModel;
        }
        #endregion

        #region get all Subcategory list details 
        private static List<SubcategoryModel> GetSubcategoryList()
        {
            List<SubcategoryModel> Model = new List<SubcategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Subcategory/GetSubcategoryList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SubcategoryModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion
        public ActionResult GetSubcategory(int CategoryId)
        {
            List<SubcategoryModel> SubcategoryModel = new List<SubcategoryModel>();

            SubcategoryModel = GetSubcategoryList(CategoryId);
            return Json(SubcategoryModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLRNo(int TransporterId)
        {
            TransportModel transports = new TransportModel();

            transportModels = GetTransportList();

            transports = transportModels.Where(d => d.Id == TransporterId).FirstOrDefault();

            return Json(transports, JsonRequestBehavior.AllowGet);
        }

        #region get all Subcategory list against category details 
        private static List<SubcategoryModel> GetSubcategoryList(int CategoryId)
        {
            List<SubcategoryModel> Model = new List<SubcategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Subcategory/GetSubcategoryByCategoryId?CategoryId=" + CategoryId).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SubcategoryModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get all Brand list details 
        private static List<BrandModel> GetBrandList()
        {
            List<BrandModel> brandModel = new List<BrandModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Brand/GetBrandList").Result;

                if (response.IsSuccessStatusCode)
                {
                    brandModel = response.Content.ReadAsAsync<List<BrandModel>>().Result;
                }
                else
                {
                    return brandModel;
                }
            }
            catch (Exception ex)
            {
                return brandModel;
                throw ex;
            }
            return brandModel;
        }
        #endregion

        public ActionResult GetProducts()
        {
            List<DummyProduct> PrdModel = new List<DummyProduct>();

            PrdModel = GetProductList();
            return Json(PrdModel, JsonRequestBehavior.AllowGet);
        }

        #region get all Product list details 
        private static List<DummyProduct> GetProductList()
        {
            List<DummyProduct> Model = new List<DummyProduct>();

            try
            {
                DummyProduct objPrd;
                objPrd = new DummyProduct();
                objPrd.Id = 1;
                objPrd.Name = "Product-1";
                Model.Add(objPrd);

                objPrd = new DummyProduct();
                objPrd.Id = 2;
                objPrd.Name = "Product-2";
                Model.Add(objPrd);

                objPrd = new DummyProduct();
                objPrd.Id = 3;
                objPrd.Name = "Product-3";
                Model.Add(objPrd);

                objPrd = new DummyProduct();
                objPrd.Id = 4;
                objPrd.Name = "Product-4";
                Model.Add(objPrd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return Model;
        }
        #endregion

        #region get all Tax list details 
        private static List<TaxModel> GetTaxList()
        {
            List<TaxModel> TaxModel = new List<TaxModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Tax/GetTaxList").Result;

                if (response.IsSuccessStatusCode)
                {
                    TaxModel = response.Content.ReadAsAsync<List<TaxModel>>().Result;

                    TaxModel.ForEach(x => x.Name = x.Name + "-" + x.Perc + "%");
                }
                else
                {
                    return TaxModel;
                }
            }
            catch (Exception ex)
            {
                return TaxModel;
                throw ex;
            }
            return TaxModel;
        }
        #endregion

        #region get all Transport list details 
        private static List<TransportModel> GetTransportList()
        {
            List<TransportModel> Model = new List<TransportModel>();

            try
            {

                HttpResponseMessage response = httpClient.GetAsync("api/Transport/GetTransportList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<TransportModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        public ActionResult GetPoDetails(int PoId)
        {
            POModel gRN = new POModel();

            gRN = GetPurchaseDetails(PoId);
            return Json(gRN, JsonRequestBehavior.AllowGet);
        }

        #region get purchase details 
        private static POModel GetPurchaseDetails(int PoId)
        {
            POModel Model = new POModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Purchase/GetPurchaseDetailsForGRN?PoId=" + PoId).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<POModel>().Result;

                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        private static GRNModel GetGRNDetails(int GrnId)
        {
            GRNModel Model = new GRNModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/GRN/GetGRNDetails?GrnId=" + GrnId).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<GRNModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        public object DeleteGRN(DeleteGRN delete)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/GRN/DeleteGRN", delete, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }

        public ActionResult GetGRNListSearch(DateTime fromdate, DateTime todate)
        {
            grn = GetGRNList();

            grn = grn.Where(d => d.GrnDate >= fromdate && d.GrnDate <= todate).ToList();

            return Json(grn, JsonRequestBehavior.AllowGet);
        }
    }
}