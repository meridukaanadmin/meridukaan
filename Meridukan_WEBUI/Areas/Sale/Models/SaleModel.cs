﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_WEBUI.Areas.Sale.Models
{
    public class SaleModel
    {
        public int SaleId { get; set; }
        public DateTime SaleDate { get; set; }
        public string Date { get; set; }
        public string SaleNo { get; set; }
        public string CustomerName { get; set; }
        public string Contact { get; set; }
        public int SalesmanId { get; set; }
        public string SalesmanNo { get; set; }
        public decimal Total { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal GrandTotal { get; set; }        
        public decimal ReceiveAmount { get; set; }
        public decimal PendingAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal Change { get; set; }        
        public decimal Cash { get; set; }
        public string PaymentOption { get; set; }
        public int UserId { get; set; }
        public int OrderStatusId { get; set; }        
        public string UserName { get; set; }
        public List<SaleDetailModel> saleDetails { get; set; }
    }

    public class SaleDetailModel
    {
        public int SaleDetId { get; set; }
        public int SaleId { get; set; }
        public string SaleNo { get; set; }
        public string Barcode { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ImagePath { get; set; }
        public int SizeId { get; set; }
        public string Size { get; set; }        
        public string SaleQty { get; set; }
        public decimal SaleRate { get; set; }
        public int TaxId { get; set; }
        public string TaxName { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Total { get; set; }
        public decimal Discount { get; set; }
        public decimal GrandTotal { get; set; }
    }

    public class DeleteSO
    {
        public int SaleId { get; set; }
    }

    public class SaleRateModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ImagePath { get; set; }
        public int SizeId { get; set; }
        public int TaxId { get; set; }
        public string TaxName { get; set; }
        public decimal TaxValue { get; set; }
        public decimal SaleRate { get; set; }
        public string Stock { get; set; }
    }

    public class SaleProductModel
    {
        public int PrAttributeId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int SizeId { get; set; }
        public string SizeName { get; set; }
        public string Barcode { get; set; }
    }

    public class PopularTenderedValues
    {
        public int Id { get; set; }
        public decimal Value { get; set; }
    }

    public class OrderStatus
    {
        public int SaleId { get; set; }
        public int OrderStatusId { get; set; }
    }

    public class AutocompleteSearch
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class SaleListSearch
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}