﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_WEBUI.Areas.Sale.Models
{
    public class CartModel
    {
        public int CartId { get; set; }
        public string CartNo { get; set; }
        public DateTime date { get; set; }
        public string Contact { get; set; }
        public string CustomerName { get; set; }
        public int CustomerId { get; set; }
        public decimal Total { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal GrandTotal { get; set; }        
        public int UserId { get; set; }
        public string UserName { get; set; }
        public List<CartDetailsModel> cartDetails { get; set; }
    }

    public class CartDetailsModel
    {
        public int CartDetId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int SizeId { get; set; }        
        public decimal Rate { get; set; }
        public string Qty { get; set; }
        public decimal Total { get; set; }
        public int TaxId { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal GrandTotal { get; set; }
    }

    public class CustomerCartDetailsModel
    {
        public string Contact { get; set; }
        public string Name { get; set; }
        public int CartDetId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ImagePath { get; set; }
        public int SizeId { get; set; }
        public string SizeName { get; set; }
        public decimal Rate { get; set; }
        public string Qty { get; set; }
        public decimal Total { get; set; }
        public int TaxId { get; set; }
        public string TaxName { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal FinalTotal { get; set; }
        public decimal FinalTaxAmount { get; set; }
        public decimal FinalGrandTotal { get; set; }
    }

    public class DeleteCartDataByCustomerModel
    {
        public string Contact { get; set; }
    }
}