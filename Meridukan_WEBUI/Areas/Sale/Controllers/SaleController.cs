﻿using Meridukan_WEBUI.Areas.Brand.Models;
using Meridukan_WEBUI.Areas.Category.Models;
using Meridukan_WEBUI.Areas.Product.Models;
using Meridukan_WEBUI.Areas.Sale.Models;
using Meridukan_WEBUI.Areas.Salesman.Models;
using Meridukan_WEBUI.Areas.Size.Models;
using Meridukan_WEBUI.Areas.Subcategory.Models;
using Meridukan_WEBUI.Areas.Tax.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Sale.Controllers
{
    public class SaleController : BaseController 
    {
        List<SaleModel> sales;
        List<ProductModel> products;
        List<SalesmanModel> salesmen;
        List<OrderStatusModel> orderStatuses;
        List<AutocompleteSearch> searches;
        public SaleController()
        {
            sales = new List<SaleModel>();
            products = new List<ProductModel>();
            salesmen = new List<SalesmanModel>();
            orderStatuses = new List<OrderStatusModel>();
            searches = new List<AutocompleteSearch>();
            searches = GetSearchList();
        }

        // GET: Sale/Sale
        public ActionResult Index()
        {
            return View();
        }

        // GET: Sale/Sale
        public ActionResult GetSale()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Sale";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                // ViewData["GetSaleList"] = GetSaleList();

                sales = GetSaleList();

                SaleListSearch saleList = new SaleListSearch();

                DateTime now = DateTime.Now;
                var startDate = new DateTime(now.Year, now.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);

                saleList.FromDate = Convert.ToDateTime(startDate);
                saleList.ToDate = Convert.ToDateTime(endDate);

                sales = sales.Where(d => d.SaleDate >= saleList.FromDate && d.SaleDate <= saleList.ToDate).ToList();

                ViewData["GetSaleList"] = sales;

                return View("Sale",saleList);
            }
        }

        public ActionResult AddSale(int SaleId)
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Sale";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                SaleModel sale = new SaleModel();
                sale.saleDetails = new List<SaleDetailModel>();

                if (SaleId > 0)
                {
                    sale = GetSaleDetails(SaleId);
                    //ViewBag.OrderStatusList = new SelectList(GetOrderStatusList(), "Id", "Name");
                }
                else
                {
                    sale.SaleDate = DateTime.Today.AddDays(0);
                    sale.OrderStatusId = 0;
                }

                ViewBag.SalesmanList = new SelectList(GetSalesmanList(), "Id", "Name");
                //ViewBag.CategoryList = new SelectList(GetCategoryList(), "Id", "Name");
                //ViewBag.BrandList = new SelectList(GetBrandList(), "Id", "Name");
                ViewBag.TaxList = new SelectList(GetTaxList(), "Id", "Name");
                return View("AddSale", sale);
            }
        }

        #region 
        private static List<SaleModel> GetSaleList()
        {
            List<SaleModel> Model = new List<SaleModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Sale/GetSaleList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SaleModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get all category list details 
        private static List<CategoryModel> GetCategoryList()
        {
            List<CategoryModel> catgoryModel = new List<CategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Category/GetCategoryList").Result;

                if (response.IsSuccessStatusCode)
                {
                    catgoryModel = response.Content.ReadAsAsync<List<CategoryModel>>().Result;
                }
                else
                {
                    return catgoryModel;
                }
            }
            catch (Exception ex)
            {
                return catgoryModel;
                throw ex;
            }
            return catgoryModel;
        }
        #endregion

        public ActionResult GetSubcategory(int CategoryId)
        {
            List<SubcategoryModel> SubcategoryModel = new List<SubcategoryModel>();

            SubcategoryModel = GetSubcategoryList(CategoryId);
            return Json(SubcategoryModel, JsonRequestBehavior.AllowGet);
        }

        #region get all Subcategory list against category details 
        private static List<SubcategoryModel> GetSubcategoryList(int CategoryId)
        {
            List<SubcategoryModel> Model = new List<SubcategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Subcategory/GetSubcategoryByCategoryId?CategoryId=" + CategoryId).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SubcategoryModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get all Brand list details 
        private static List<BrandModel> GetBrandList()
        {
            List<BrandModel> brandModel = new List<BrandModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Brand/GetBrandList").Result;

                if (response.IsSuccessStatusCode)
                {
                    brandModel = response.Content.ReadAsAsync<List<BrandModel>>().Result;
                }
                else
                {
                    return brandModel;
                }
            }
            catch (Exception ex)
            {
                return brandModel;
                throw ex;
            }
            return brandModel;
        }
        #endregion

        public ActionResult GetProducts()
        {
            List<SaleProductModel> PrdModel = new List<SaleProductModel>();

            PrdModel = GetSaleProductList();
            return Json(PrdModel, JsonRequestBehavior.AllowGet);
        }

        private static List<SaleProductModel> GetSaleProductList()
        {
            List<SaleProductModel> Model = new List<SaleProductModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Product/GetSaleProductList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SaleProductModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        private static List<ProductModel> GetProductList()
        {
            List<ProductModel> Model = new List<ProductModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Product/GetProductList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<ProductModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        #region get all Tax list details 
        private static List<TaxModel> GetTaxList()
        {
            List<TaxModel> TaxModel = new List<TaxModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Tax/GetTaxList").Result;

                if (response.IsSuccessStatusCode)
                {
                    TaxModel = response.Content.ReadAsAsync<List<TaxModel>>().Result;

                    TaxModel.ForEach(x => x.Name = x.Name + "-" + x.Perc + "%");
                }
                else
                {
                    return TaxModel;
                }
            }
            catch (Exception ex)
            {
                return TaxModel;
                throw ex;
            }
            return TaxModel;
        }
        #endregion

        public ActionResult GetSaleRate(string Barcode)
        {
            SaleRateModel saleRate = new SaleRateModel();

            saleRate = SaleRate(Barcode);
            return Json(saleRate, JsonRequestBehavior.AllowGet);
        }

        private static SaleRateModel SaleRate(string Barcode)
        {
            SaleRateModel saleRateModel = new SaleRateModel();

            string prdvalue = Barcode.Split('(')[1];
            string barvalue = prdvalue.Split(')')[0];

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Sale/GetSaleRateByBarcode?Barcode=" + barvalue).Result;

                if (response.IsSuccessStatusCode)
                {
                    saleRateModel = response.Content.ReadAsAsync<SaleRateModel>().Result;                    
                }
                else
                {
                    return saleRateModel;
                }
            }
            catch (Exception ex)
            {
                return saleRateModel;
                throw ex;
            }
            return saleRateModel;
        }


        private static List<SalesmanModel> GetSalesmanList()
        {
            List<SalesmanModel> Model = new List<SalesmanModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Salesman/GetSalesmanList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SalesmanModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }


        private static List<OrderStatusModel> GetOrderStatusList()
        {
            List<OrderStatusModel> Model = new List<OrderStatusModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/OrderStatus/GetOrderStatusList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<OrderStatusModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        public ActionResult GetSalesman(int Id)
        {
            List<SalesmanModel> salesmanList = new List<SalesmanModel>();

            SalesmanModel salesman = new SalesmanModel();

            salesmanList = GetSalesmanList();

            salesman = salesmanList.Where(d => d.Id == Id).FirstOrDefault();

            return Json(salesman, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCartDataforCustomer(string Contact)
        {
            List<CustomerCartDetailsModel> cartList = new List<CustomerCartDetailsModel>();

            cartList = GetCustomerCartList(Contact);

            return Json(cartList, JsonRequestBehavior.AllowGet);
        }

        public object DeleteCartByCustomer(DeleteCartDataByCustomerModel delete)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Cart/DeleteCartByCustomer", delete, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }

        private static List<CustomerCartDetailsModel> GetCustomerCartList(string Contact)
        {
            List<CustomerCartDetailsModel> Model = new List<CustomerCartDetailsModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Cart/GetCardDetailsByCustomer?Contact=" + Contact).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<CustomerCartDetailsModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        // call for pop up pay functionality
        public ActionResult PayData(decimal grandtotal)
        {
            List<PopularTenderedValues> list = new List<PopularTenderedValues>();

            PopularTenderedValues values = new PopularTenderedValues();
            values.Id = 1;
            values.Value = grandtotal;
            list.Add(values);

            if (grandtotal > 0)
            {
                //create popular values
                for (int i = 0; i < 4; i++)
                {
                    values = new PopularTenderedValues();

                    if (i == 1)
                    {
                        decimal calc = grandtotal;
                        decimal calc1 = (calc / 50);
                        //decimal round = Math.Round(calc1, 0, MidpointRounding.AwayFromZero);
                        decimal round = Math.Ceiling(calc1);
                        decimal finalval = (round * 50);

                        var lst = list.Where(d => d.Value == finalval).FirstOrDefault();

                        if (lst != null)
                        {
                            values.Id = 2;
                            values.Value = finalval + 50;
                            list.Add(values);
                        }
                        else
                        {
                            values.Id = 2;
                            values.Value = finalval;
                            list.Add(values);
                        }
                    }

                    if (i == 2)
                    {
                        decimal calc = grandtotal;
                        decimal calc1 = (calc / 100);
                        decimal round = Math.Ceiling(calc1);
                        decimal finalval = (round * 100);

                        values.Value = finalval;

                        var lst = list.Where(d => d.Value == finalval).FirstOrDefault();

                        if (lst != null)
                        {
                            values.Id = 3;
                            values.Value = finalval + 100;
                            list.Add(values);
                        }
                        else
                        {
                            values.Id = 3;
                            values.Value = finalval;
                            list.Add(values);
                        }
                    }

                    if (i == 3)
                    {
                        decimal calc = grandtotal;
                        decimal calc1 = (calc / 500);
                        decimal round = Math.Ceiling(calc1);
                        decimal finalval = (round * 500);

                        values.Value = finalval;

                        var lst = list.Where(d => d.Value == finalval).FirstOrDefault();

                        if (lst != null)
                        {

                            values.Id = 4;
                            values.Value = finalval + 500;
                            list.Add(values);
                        }
                        else
                        {
                            values.Id = 4;
                            values.Value = finalval;
                            list.Add(values);
                        }


                        //var lst = list.Where(d => d.Value == finalval).FirstOrDefault();

                        //if (lst != null)
                        //{
                        //    values.Id = 4;
                        //    values.Value = finalval + 500;
                        //    list.Add(values);
                        //}                        
                    }
                }
            }

            ViewData["Tenderedvalues"] = list;

            return PartialView("PayData");
        }

        //call for print receipt flow
        public ActionResult PrintSaleData(int SaleId)
        {
            SaleModel sale = new SaleModel();
            sale.saleDetails = new List<SaleDetailModel>();

            if (SaleId > 0)
            {
                ViewData["ViewSalePrint"] = GetSaleDetails(SaleId);
            }

            return PartialView("PrintSaleData");
        }

        //call for print receipt flow
        public ActionResult OrderStatus(int SaleId, int OrderStatusId)
        {
            OrderStatus order = new OrderStatus();
            order.SaleId = SaleId;
            order.OrderStatusId = OrderStatusId;

            ViewBag.OrderStatusList = new SelectList(GetOrderStatusList(), "Id", "Name");

            return PartialView("OrderStatus");
        }

        public object UpdateOrderStatus(OrderStatus order)
        {
            bool res = false;

            object data = new object();
            string desc = "";
            try
            {

                HttpClient httpClient = new HttpClient
                {
                    BaseAddress = new Uri(ConfigurationManager.AppSettings["apiUrl"])
                };

                using (httpClient)
                {
                    HttpResponseMessage response = httpClient.PostAsync("api/Sale/UpdateOrderStatus/", order, new JsonMediaTypeFormatter()).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        res = false;
                        desc = "Error Occured";
                    }
                    else
                    {
                        var brandEntry = response.Content.ReadAsStringAsync().Result;
                        res = true;
                        desc = "Brand Saved Successfully";
                    }
                }



                data = new
                {
                    Result = res,
                    Description = desc
                };

                return res;
            }
            catch (Exception ex)
            {
                data = new
                {
                    Result = res,
                    Description = ex.Message
                };

                return res;
            }
        }

        [HttpPost]
        public JsonResult SaveCart(CartModel carts)
        {
            ViewBag.UserId = carts.UserId;
            ViewBag.UserName = carts.UserName;

            ViewBag.ActivePage = "Sale";

            bool res = false;

            if (ModelState.IsValid)
            {
                try
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Cart/SaveCartAtSuspend/", carts, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = false;
                        }
                        else
                        {
                            var userEntry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            // long res = Convert.ToInt64(userEntry);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList();

                //return View("AddPO", purchase);
            }
            return Json(res);
        }

        [HttpPost]
        public JsonResult Save(SaleModel sale)
        {
            ViewBag.UserId = sale.UserId;
            ViewBag.UserName = sale.UserName;

            ViewBag.ActivePage = "Sale";

            long res = 0;

            if (sale.SalesmanId == 0)
            {
                ModelState.Remove("SalesmanId");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Sale/AddorUpdateSale/", sale, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = 0;
                        }
                        else
                        {
                            var saleEntry = response.Content.ReadAsStringAsync().Result;
                            res = Convert.ToInt64(saleEntry);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList();

                //return View("AddPO", purchase);
            }
            return Json(res);
        }

        #region get sale details 
        private static SaleModel GetSaleDetails(int SaleId)
        {
            SaleModel Model = new SaleModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Sale/GetSaleDetails?SaleId=" + SaleId).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<SaleModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        public object DeleteSale(DeleteSO delete)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Sale/DeleteSale", delete, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }

        private static List<AutocompleteSearch> GetSearchList()
        {
            List<AutocompleteSearch> Model = new List<AutocompleteSearch>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Sale/GetProductSearchData").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<AutocompleteSearch>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        [HttpPost]
        public JsonResult Search(string Prefix)
        {  
            //Searching records from list using LINQ query  
            var Name = (from N in searches
                        where N.Name.StartsWith(Prefix)
                        select new { N.Name });
            return Json(Name, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSaleListSearch(DateTime fromdate, DateTime todate)
        {
            sales = GetSaleList();
            sales = sales.Where(d => d.SaleDate >= fromdate && d.SaleDate <= todate).ToList();

            return Json(sales, JsonRequestBehavior.AllowGet);
        }
    }
}