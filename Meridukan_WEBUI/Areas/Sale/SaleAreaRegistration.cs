﻿using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Sale
{
    public class SaleAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Sale";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Sale_default",
                "Sale/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new { controller = "Sale" },
                new[] { "Meridukan_WEBUI.Areas.Sale.Controllers" }
            );

            context.MapRoute(
                "Salepage_default",
                "Sale/{controller}/{action}/{id}",
                new { action = "Sale", id = UrlParameter.Optional },
                new { controller = "Sale" },
                new[] { "Meridukan_WEBUI.Areas.Sale.Controllers" }
            );
        }
    }
}