﻿using Meridukan_WEBUI.Areas.Brand.Models;
using Meridukan_WEBUI.Areas.Category.Models;
using Meridukan_WEBUI.Areas.Color.Models;
using Meridukan_WEBUI.Areas.Product.Models;
using Meridukan_WEBUI.Areas.Size.Models;
using Meridukan_WEBUI.Areas.Subcategory.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Product.Controllers
{
    public class ProductController : BaseController
    {
        // GET: Product/Product
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetProduct()
        {
            ViewBag.ActivePage = "Product";
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Product";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetProductList"] = GetProductList();
                return View("Product");
            }
             
        }

        public ActionResult ProductDetails(int Id)
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Purchase";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            AddProductModel products = new AddProductModel();
            products.ProductAtributes = new List<ProductAttributes>();

            if (Id > 0)
            {
                products = GetProductDetails(Id);
                ViewData["ProductGRNList"] = GetProductGRNList(Id);
            }
            else
            {
                ViewData["ProductGRNList"] = null;
            }

            ViewBag.BrandList = new SelectList(GetBrandList(), "Id", "Name");
            ViewBag.CategoryList = new SelectList(GetCategoryList(), "Id", "Name");
            ViewBag.SubCategoryList = new SelectList(GetSubcategoryList(), "Id", "Name");
            ViewBag.ColorList = new SelectList(GetColorList(), "Id", "Name");
            ViewBag.SizeList = new SelectList(GetSizeList(), "Id", "Name");
            
            ViewBag.ActivePage = "Product";
            return View("ProductDetails", products);
        }

        #region get all Subcategory list details 
        private static List<SubcategoryModel> GetSubcategoryList()
        {
            List<SubcategoryModel> Model = new List<SubcategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Subcategory/GetSubcategoryList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SubcategoryModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get all Size list details 
        private static List<SizeModel> GetSizeList()
        {
            List<SizeModel> sizeModel = new List<SizeModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Size/GetSizeList").Result;

                if (response.IsSuccessStatusCode)
                {
                    sizeModel = response.Content.ReadAsAsync<List<SizeModel>>().Result;
                }
                else
                {
                    return sizeModel;
                }
            }
            catch (Exception ex)
            {
                return sizeModel;
                throw ex;
            }
            return sizeModel;
        }
        #endregion

        #region get all Color list details 
        private static List<ColorModel> GetColorList()
        {
            List<ColorModel> colorModel = new List<ColorModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Color/GetColorList").Result;

                if (response.IsSuccessStatusCode)
                {
                    colorModel = response.Content.ReadAsAsync<List<ColorModel>>().Result;
                }
                else
                {
                    return colorModel;
                }
            }
            catch (Exception ex)
            {
                return colorModel;
                throw ex;
            }
            return colorModel;
        }
        #endregion


        #region get all Brand list details 
        private static List<BrandModel> GetBrandList()
        {
            List<BrandModel> brandModel = new List<BrandModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Brand/GetBrandList").Result;

                if (response.IsSuccessStatusCode)
                {
                    brandModel = response.Content.ReadAsAsync<List<BrandModel>>().Result;
                }
                else
                {
                    return brandModel;
                }
            }
            catch (Exception ex)
            {
                return brandModel;
                throw ex;
            }
            return brandModel;
        }
        #endregion
        #region get all category list details 
        private static List<CategoryModel> GetCategoryList()
        {
            List<CategoryModel> catgoryModel = new List<CategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Category/GetCategoryList").Result;

                if (response.IsSuccessStatusCode)
                {
                    catgoryModel = response.Content.ReadAsAsync<List<CategoryModel>>().Result;
                }
                else
                {
                    return catgoryModel;
                }
            }
            catch (Exception ex)
            {
                return catgoryModel;
                throw ex;
            }
            return catgoryModel;
        }
        #endregion
        private static List<ProductModel> GetProductList()
        {
            List<ProductModel> Model = new List<ProductModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Product/GetProductList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<ProductModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        #region get Product details 
        private static AddProductModel GetProductDetails(int ProductId)
        {
            AddProductModel Model = new AddProductModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Product/GetProductDetails?ProductId=" + ProductId).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<AddProductModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadFile()
        {
            string _imgname = string.Empty;
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["MyImages"];
                if (pic.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(pic.FileName);
                    var _ext = Path.GetExtension(pic.FileName);

                    _imgname = Guid.NewGuid().ToString();
                    var _comPath = Server.MapPath("/Uploads/PRD_") + _imgname + _ext;
                    _imgname = "PRD_" + _imgname + _ext;

                    ViewBag.Msg = _comPath;
                    var path = _comPath;

                    // Saving Image in Original Mode
                    pic.SaveAs(path);

                    // resizing image
                    MemoryStream ms = new MemoryStream();
                    WebImage img = new WebImage(_comPath);

                    if (img.Width > 200)
                        img.Resize(200, 200);
                    img.Save(_comPath);
                    // end resize
                }
            }
            return Json(Convert.ToString(_imgname), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Save(AddProductModel product)
        {
            string userid = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            string username = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserId = userid;
            ViewBag.UserName = username;

            ViewBag.ActivePage = "Product";

            //string sessionid = (System.Web.HttpContext.Current.Session["SessionId"] != null) ? System.Web.HttpContext.Current.Session["SessionId"].ToString() : String.Empty;
            //if (string.IsNullOrEmpty(sessionid))
            //{
            //    return RedirectToAction("LogOut", "Account");
            //}
            //if (AppUser == null)
            //{
            //    return RedirectToAction("LogOut", "Account");
            //}

            bool res = false;

            if (ModelState.IsValid)
            {
                try
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Product/AddorUpdateProduct/", product, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            throw new ApplicationException("Error occured while updating user.");
                        }
                        else
                        {
                            var userEntry = response.Content.ReadAsStringAsync().Result;
                            // long res = Convert.ToInt64(userEntry);
                            res = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList();
            }
            return Json(res);
        }

        #region get GRN details 
        private static List<ProductGRNListModel> GetProductGRNList(int ProductId)
        {
           List<ProductGRNListModel> Model = new List<ProductGRNListModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Product/GetProductGRNList?ProductId=" + ProductId).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<ProductGRNListModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        public object DeleteProduct(DeleteProduct delete)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Product/DeleteProductData", delete, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }
    }

}