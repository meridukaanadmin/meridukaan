﻿using Meridukan_WEBUI.Areas.Brand.Models;
using Meridukan_WEBUI.Areas.Category.Models;
using Meridukan_WEBUI.Areas.Color.Models;
using Meridukan_WEBUI.Areas.Product.Models;
using Meridukan_WEBUI.Areas.Purchase.Models;
using Meridukan_WEBUI.Areas.Size.Models;
using Meridukan_WEBUI.Areas.Subcategory.Models;
using Meridukan_WEBUI.Areas.Supplier.Models;
using Meridukan_WEBUI.Areas.Tax.Models;
using Meridukan_WEBUI.Areas.Transport.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Purchase.Controllers
{
    public class PurchaseController : BaseController
    {
        List<PurchaseModel> purchases;
        List<TransportModel> transportModels;
        List<ProductModel> products;
        public PurchaseController()
        {
            purchases = new List<PurchaseModel>();
            transportModels = new List<TransportModel>();
            products = new List<ProductModel>();
        }


        // GET: Purchase/Purchase
        public ActionResult Index()
        {
            return View();
        }

        // GET: Purchase/Purchase
        public ActionResult GetPurchase()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Purchase";

            PurchaseListSearch purchaseList = new PurchaseListSearch();

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                purchases = GetPurchaseList();

                DateTime now = DateTime.Now;
                var startDate = new DateTime(now.Year, now.Month, 1);
                var endDate = startDate.AddMonths(1).AddDays(-1);

                purchaseList.FromDate = Convert.ToDateTime(startDate);
                purchaseList.ToDate = Convert.ToDateTime(endDate);

                purchases = purchases.Where(d => d.PoDate >= purchaseList.FromDate && d.PoDate <= purchaseList.ToDate).ToList();

                ViewData["GetPurchaseList"] = purchases;

                return View("Purchase", purchaseList);
            }
        }

        // GET: Purchase/Purchase
        public ActionResult AddPO(int PoId)
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Purchase";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                PurchaseModel purchase = new PurchaseModel();
                purchase.purchaseDetails = new List<PurchaseDetailModel>();

                if (PoId > 0)
                {
                    purchase = GetPurchaseDetails(PoId);
                }
                else
                {
                    purchase.LR_No = "";
                    purchase.PoDate = DateTime.Today.AddDays(0);
                    purchase.SupplierInvDate = purchase.LR_Date = DateTime.Today.AddDays(0);
                }
                
                ViewBag.SupplierList = new SelectList(GetSupplierList(), "Id", "Name");
                ViewBag.CategoryList = new SelectList(GetCategoryList(), "Id", "Name");
                ViewBag.BrandList = new SelectList(GetBrandList(), "Id", "Name");
                ViewBag.TaxList = new SelectList(GetTaxList(), "Id", "Name");
                ViewBag.TransportList = new SelectList(GetTransportList(), "Id", "Partner");
                ViewBag.ColorList = new SelectList(GetColorList(), "Id", "Name");
                ViewBag.SizeList = new SelectList(GetSizeList(), "Id", "Name");
                return View("AddPO", purchase);
            }
        }

        [HttpPost]
        public JsonResult Save(PurchaseModel purchase)
        {   
            ViewBag.UserId = purchase.UserId;
            ViewBag.UserName = purchase.UserName;

            ViewBag.ActivePage = "Purchase";

            bool res = false;

            if (ModelState.IsValid)
            {
                try
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Purchase/AddorUpdatePO/", purchase, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            throw new ApplicationException("Error occured while updating user.");
                        }
                        else
                        {
                            var userEntry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            // long res = Convert.ToInt64(userEntry);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList();

                //return View("AddPO", purchase);
            }
            return Json(res);
        }

        #region 
        private static List<PurchaseModel> GetPurchaseList()
        {
            List<PurchaseModel> Model = new List<PurchaseModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Purchase/GetPurchaseList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<PurchaseModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get all Supplier list details 
        private static List<SupplierModel> GetSupplierList()
        {
            List<SupplierModel> Model = new List<SupplierModel>();

            try
            {
                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("Application/json"));

                HttpResponseMessage response = httpClient.GetAsync("api/Supplier/GetSupplierList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SupplierModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get all category list details 
        private static List<CategoryModel> GetCategoryList()
        {
            List<CategoryModel> catgoryModel = new List<CategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Category/GetCategoryList").Result;

                if (response.IsSuccessStatusCode)
                {
                    catgoryModel = response.Content.ReadAsAsync<List<CategoryModel>>().Result;
                }
                else
                {
                    return catgoryModel;
                }
            }
            catch (Exception ex)
            {
                return catgoryModel;
                throw ex;
            }
            return catgoryModel;
        }
        #endregion

        public ActionResult GetSubcategory(int CategoryId)
        {
            List<SubcategoryModel> SubcategoryModel = new List<SubcategoryModel>();

            SubcategoryModel = GetSubcategoryList(CategoryId);
            return Json(SubcategoryModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLRNo(int TransporterId)
        {
            TransportModel transports = new TransportModel();

            transportModels = GetTransportList();

            transports = transportModels.Where(d => d.Id == TransporterId).FirstOrDefault();
            
            return Json(transports, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPurchaseListSearch(DateTime fromdate,DateTime todate)
        {

            purchases = GetPurchaseList();

            purchases = purchases.Where(d => d.PoDate >= fromdate && d.PoDate <= todate).ToList();

            return Json(purchases, JsonRequestBehavior.AllowGet);
        }

        #region get all Subcategory list against category details 
        private static List<SubcategoryModel> GetSubcategoryList(int CategoryId)
        {
            List<SubcategoryModel> Model = new List<SubcategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Subcategory/GetSubcategoryByCategoryId?CategoryId=" + CategoryId).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SubcategoryModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get all Brand list details 
        private static List<BrandModel> GetBrandList()
        {
            List<BrandModel> brandModel = new List<BrandModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Brand/GetBrandList").Result;

                if (response.IsSuccessStatusCode)
                {
                    brandModel = response.Content.ReadAsAsync<List<BrandModel>>().Result;
                }
                else
                {
                    return brandModel;
                }
            }
            catch (Exception ex)
            {
                return brandModel;
                throw ex;
            }
            return brandModel;
        }
        #endregion

        public ActionResult GetProducts()
        {
            List<ProductModel> PrdModel = new List<ProductModel>();

            PrdModel = GetProductList();
            return Json(PrdModel, JsonRequestBehavior.AllowGet);
        }

        private static List<ProductModel> GetProductList()
        {
            List<ProductModel> Model = new List<ProductModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Product/GetProductList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<ProductModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        //#region get all Product list details 
        //private static List<DummyProduct> GetProductList()
        //{
        //    List<DummyProduct> Model = new List<DummyProduct>();

        //    try
        //    {
        //        DummyProduct objPrd;
        //        objPrd = new DummyProduct();
        //        objPrd.Id = 1;
        //        objPrd.Name = "Product-1";
        //        Model.Add(objPrd);

        //        objPrd = new DummyProduct();
        //        objPrd.Id = 2;
        //        objPrd.Name = "Product-2";
        //        Model.Add(objPrd);

        //        objPrd = new DummyProduct();
        //        objPrd.Id = 3;
        //        objPrd.Name = "Product-3";
        //        Model.Add(objPrd);

        //        objPrd = new DummyProduct();
        //        objPrd.Id = 4;
        //        objPrd.Name = "Product-4";
        //        Model.Add(objPrd);
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //    return Model;
        //}
        //#endregion

        #region get all Tax list details 
        private static List<TaxModel> GetTaxList()
        {
            List<TaxModel> TaxModel = new List<TaxModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Tax/GetTaxList").Result;

                if (response.IsSuccessStatusCode)
                {
                    TaxModel = response.Content.ReadAsAsync<List<TaxModel>>().Result;

                    TaxModel.ForEach(x => x.Name = x.Name + "-" + x.Perc + "%");
                }
                else
                {
                    return TaxModel;
                }
            }
            catch (Exception ex)
            {
                return TaxModel;
                throw ex;
            }
            return TaxModel;
        }
        #endregion

        #region get all Transport list details 
        private static List<TransportModel> GetTransportList()
        {
            List<TransportModel> Model = new List<TransportModel>();

            try
            {

                HttpResponseMessage response = httpClient.GetAsync("api/Transport/GetTransportList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<TransportModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get purchase details 
        private static PurchaseModel GetPurchaseDetails(int PoId)
        {
            PurchaseModel Model = new PurchaseModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Purchase/GetPurchaseDetails?PoId=" + PoId).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<PurchaseModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        public object DeletePo(DeletePO delete)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Purchase/DeletePO", delete, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }

        public ActionResult AddProductDetails()
        {
            PrdModel product = new PrdModel();

            ViewBag.BrandList = new SelectList(GetBrandList(), "Id", "Name");
            ViewBag.CategoryList = new SelectList(GetCategoryList(), "Id", "Name");
            //ViewBag.SubCategoryList = new SelectList(GetCategoryList(), "Id", "Name");
            ViewBag.ColorList = new SelectList(GetColorList(), "Id", "Name");
            ViewBag.SizeList = new SelectList(GetSizeList(), "Id", "Name");
            return PartialView("AddProductDetails");
        }

        #region get all Size list details 
        private static List<SizeModel> GetSizeList()
        {
            List<SizeModel> sizeModel = new List<SizeModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Size/GetSizeList").Result;

                if (response.IsSuccessStatusCode)
                {
                    sizeModel = response.Content.ReadAsAsync<List<SizeModel>>().Result;
                }
                else
                {
                    return sizeModel;
                }
            }
            catch (Exception ex)
            {
                return sizeModel;
                throw ex;
            }
            return sizeModel;
        }
        #endregion

        #region get all Color list details 
        private static List<ColorModel> GetColorList()
        {
            List<ColorModel> colorModel = new List<ColorModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Color/GetColorList").Result;

                if (response.IsSuccessStatusCode)
                {
                    colorModel = response.Content.ReadAsAsync<List<ColorModel>>().Result;
                }
                else
                {
                    return colorModel;
                }
            }
            catch (Exception ex)
            {
                return colorModel;
                throw ex;
            }
            return colorModel;
        }
        #endregion
        
        public object AddProduct(AddPrdModel product)
        {
            string userid = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            string username = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserId = userid;
            ViewBag.UserName = username;

            bool res = false;

            object data = new object();
            string desc = "";

            if (ModelState.IsValid)
            {
                try
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Product/AddorUpdateProduct/", product, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            throw new ApplicationException("Error occured while updating user.");
                        }
                        else
                        {
                            var userEntry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            res = true;
                            desc = "Saved Successfully";
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList();

                res = false;
                desc = "Error Occured";
            }

            data = new
            {
                Result = res,
                Description = desc
            };

            return res;
        }
    }
}