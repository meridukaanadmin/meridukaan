﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_WEBUI.Areas.Purchase.Models
{
    public class PrdModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string Tags { get; set; }
        public int UserId { get; set; }
        public string Barcode { get; set; }

        public string SKU { get; set; }
        public float MRP { get; set; }
        public float DiscountPerc { get; set; }
        public float DiscountPrice { get; set; }
        public int CategoryId { get; set; }
        public string Category { get; set; }
        public int SubCategoryId { get; set; }
        public string SubCategory { get; set; }
        public int BrandId { get; set; }
        public string Brand { get; set; }
        public float StockQty { get; set; }
        public float MinimumStockQty { get; set; }
        public float MaximumOrderQty { get; set; }
        public bool IsAllowOrder { get; set; }
        public bool IsPublished { get; set; }
        public List<PrdAttributes> ProductAttribute { get; set; }
    }

    public class AddPrdModel
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string ShortDescription { get; set; }
        public string ProductDescription { get; set; }
        public string Tags { get; set; }
        public int UserId { get; set; }
        public string Barcode { get; set; }       
        public int CategoryId { get; set; }
        public int SubCategoryId { get; set; }
        public int BrandId { get; set; }

        public List<PrdAttributes> ProductAtributes { get; set; }
    }

    public class PrdAttributes
    {
        public int Id { get; set; }
        public int PrAttribute_Id { get; set; }
        public string SKU { get; set; }
        public float Weight { get; set; }
        public float Length { get; set; }
        public float Height { get; set; }
        public float Width { get; set; }

        public string Tags { get; set; }
        public decimal MRP { get; set; }
        public decimal DiscountPerc { get; set; }
        public decimal DiscountPrice { get; set; }

        public decimal SalePrice { get; set; }
        public DateTime SaleStart { get; set; }
        public DateTime SaleEnd { get; set; }
        public int StockQty { get; set; }
        public int MaximumOrderQty { get; set; }
        public int MinimumStockQty { get; set; }

        public bool IsAllowOrder { get; set; }
        public bool IsPublished { get; set; }

        public int PrColorId { get; set; }
        public int PrSizeId { get; set; }

    }

}