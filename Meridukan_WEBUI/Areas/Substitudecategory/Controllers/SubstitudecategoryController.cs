﻿using Meridukan_WEBUI.Areas.Subcategory.Models;
using Meridukan_WEBUI.Areas.Substitudecategory.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Substitudecategory.Controllers
{
    public class SubstitudecategoryController : BaseController
    {
        List<SubstitudeCategoryModel> subcats;

        public SubstitudecategoryController()
        {
            subcats = new List<SubstitudeCategoryModel>();
        }

        // GET: Substitudecategory/Substitudecategory
        public ActionResult Index()
        {
            return View();
        }

        // GET: Substitudecategory/Substitudecategory
        public ActionResult GetSubstitude()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Substitudecategory";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetSubcategoryList"] = subcats = GetSubstitudecategoryList();
                return View("Substitude");
            }
        }

        public ActionResult SubstitudecatDetails(int Id)
        {
            SubstitudeCategoryModel subcategory = new SubstitudeCategoryModel();
            ViewBag.SubstitudecatList = new SelectList(GetSubcategoryList(), "Id", "Name");
            if (Id > 0)
            {
                subcategory = GetSubstitudecategoryDetails(Id);
            }

            return PartialView("SubstitudecatDetails", subcategory);
        }

        #region get all Substitudecategory list details 
        private static List<SubstitudeCategoryModel> GetSubstitudecategoryList()
        {
            List<SubstitudeCategoryModel> Model = new List<SubstitudeCategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/SubstitudeCategory/GetSubstitudecategoryList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SubstitudeCategoryModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get all Subcategory list details 
        private static List<SubcategoryModel> GetSubcategoryList()
        {
            List<SubcategoryModel> Model = new List<SubcategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Subcategory/GetSubcategoryList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SubcategoryModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get Substitudecategory details 
        private static SubstitudeCategoryModel GetSubstitudecategoryDetails(int Id)
        {
            SubstitudeCategoryModel model = new SubstitudeCategoryModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/SubstitudeCategory/GetSubstitudecategoryDetails?substitudecategoryId=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    model = response.Content.ReadAsAsync<SubstitudeCategoryModel>().Result;
                }
                else
                {
                    return model;
                }
            }
            catch (Exception ex)
            {
                return model;
                throw ex;
            }
            return model;
        }
        #endregion

        public object AddOrUpdateSubstitudecategory(SubstitudeCategoryModel subcategory)
        {
            bool res = false;

            object data = new object();
            string desc = "";
            try
            {
                if (string.IsNullOrEmpty(subcategory.Name))
                {
                    desc = "Subcategory is required";
                    ModelState.AddModelError("Name", "Substitude category is required");
                }

                var lst = subcats.Where(d => d.Name == subcategory.Name);

                if (lst.Count() > 0)
                {
                    desc = "Subcategory already exists";
                    ModelState.AddModelError("Exists", "Substitude category already exists");
                }

                if (ModelState.IsValid)
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/SubstitudeCategory/AddorUpdateSubstitudecategory/", subcategory, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = false;
                            desc = "Error Occured";
                        }
                        else
                        {
                            var brandEntry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            desc = "Subcategory Saved Successfully";
                        }
                    }
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList();

                    res = false;
                    desc = "Error Occured";
                    // return View("Brand", brand);
                }

                data = new
                {
                    Result = res,
                    Description = desc
                };

                return res;
            }
            catch (Exception ex)
            {
                data = new
                {
                    Result = res,
                    Description = ex.Message
                };

                return res;
            }
        }

        public object DeleteSubcat(DeleteSubstitudecategory deletesubcat)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/SubstitudeCategory/DeleteSubstitudecategory", deletesubcat, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }
    }
}