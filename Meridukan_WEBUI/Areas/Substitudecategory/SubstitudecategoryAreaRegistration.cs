﻿using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Substitudecategory
{
    public class SubstitudecategoryAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Substitudecategory";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Substitudecategory_default",
                "Substitudecategory/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new { controller = "Substitudecategory" },
                new[] { "Meridukan_WEBUI.Areas.Substitudecategory.Controllers" }
            );
        }
    }
}