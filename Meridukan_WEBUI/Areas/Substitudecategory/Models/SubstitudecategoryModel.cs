﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_WEBUI.Areas.Substitudecategory.Models
{
    public class SubstitudeCategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SubcategoryId { get; set; }
        public string SubcategoryName { get; set; }
    }

    public class DeleteSubstitudecategory
    {
        public int Id { get; set; }
    }
}