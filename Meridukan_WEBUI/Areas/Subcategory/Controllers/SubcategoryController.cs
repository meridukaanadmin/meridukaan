﻿using Meridukan_WEBUI.Areas.Category.Models;
using Meridukan_WEBUI.Areas.Subcategory.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Subcategory.Controllers
{
    public class SubcategoryController : BaseController
    {
        List<SubcategoryModel> subcats;

        public SubcategoryController()
        {
            subcats = new List<SubcategoryModel>();
        }

        // GET: Subcategory/Subcategory
        public ActionResult Index()
        {
            return View();
        }

        // GET: Subcategory/Subcategory
        public ActionResult GetSubcategory()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Subcategory";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetSubcategoryList"] = subcats = GetSubcategoryList();
                return View("Subcategory");
            }
            
        }

        public ActionResult SubcatDetails(int Id)
        {
            SubcategoryModel subcategory = new SubcategoryModel();
            ViewBag.CategoryList = new SelectList(GetCategoryList(), "Id", "Name");
            if (Id > 0)
            {
                subcategory = GetSubcategoryDetails(Id);
            }

            return PartialView("SubcatDetails", subcategory);
        }

        #region get all Subcategory list details 
        private static List<SubcategoryModel> GetSubcategoryList()
        {
            List<SubcategoryModel> Model = new List<SubcategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Subcategory/GetSubcategoryList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SubcategoryModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get Subcategory details 
        private static SubcategoryModel GetSubcategoryDetails(int Id)
        {
            SubcategoryModel model = new SubcategoryModel();

            try
            {  
                HttpResponseMessage response = httpClient.GetAsync("api/Subcategory/GetSubcategoryDetails?subcategoryId=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    model = response.Content.ReadAsAsync<SubcategoryModel>().Result;
                }
                else
                {
                    return model;
                }
            }
            catch (Exception ex)
            {
                return model;
                throw ex;
            }
            return model;
        }
        #endregion

        #region get all category list details 
        private static List<CategoryModel> GetCategoryList()
        {
            List<CategoryModel> Model = new List<CategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Category/GetCategoryList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<CategoryModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        public object AddOrUpdateSubcategory(SubcategoryModel subcategory)
        {
            bool res = false;

            object data = new object();
            string desc = "";
            try
            {
                if (string.IsNullOrEmpty(subcategory.Name))
                {
                    desc = "Subcategory is required";
                    ModelState.AddModelError("Name", "Subcategory is required");
                }

                var lst = subcats.Where(d => d.Name == subcategory.Name);

                if (lst.Count() > 0)
                {
                    desc = "Subcategory already exists";
                    ModelState.AddModelError("Exists", "Subcategory already exists");
                }

                if (ModelState.IsValid)
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Subcategory/AddorUpdateSubcategory/", subcategory, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = false;
                            desc = "Error Occured";
                        }
                        else
                        {
                            var brandEntry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            desc = "Subcategory Saved Successfully";
                        }
                    }
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList();

                    res = false;
                    desc = "Error Occured";
                    // return View("Brand", brand);
                }

                data = new
                {
                    Result = res,
                    Description = desc
                };

                return res;
            }
            catch (Exception ex)
            {
                data = new
                {
                    Result = res,
                    Description = ex.Message
                };

                return res;
            }
        }

        public object DeleteSubcat(DeleteSubcategory deletesubcat)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Brand/DeleteBrand", deletesubcat, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }
    }
}