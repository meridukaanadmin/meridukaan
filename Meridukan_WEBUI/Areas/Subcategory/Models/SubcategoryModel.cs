﻿namespace Meridukan_WEBUI.Areas.Subcategory.Models
{
    public class SubcategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }

    public class DeleteSubcategory
    {
        public int Id { get; set; }
    }
}