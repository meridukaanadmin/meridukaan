﻿using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Subcategory
{
    public class SubcategoryAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Subcategory";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Subcategory_default",
                "Subcategory/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                 new { controller = "Subcategory" },
                new[] { "Meridukan_WEBUI.Areas.Subcategory.Controllers" }
            );
        }
    }
}