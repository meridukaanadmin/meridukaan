﻿using Meridukan_WEBUI.Areas.Supplier.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Supplier.Controllers
{
    public class SupplierController : BaseController
    {
        List<SupplierModel> suppliers;

        public SupplierController()
        {
            suppliers = new List<SupplierModel>();
        }

        // GET: Supplier/Supplier
        public ActionResult Index()
        {
            return View();
        }

        // GET: Supplier/Supplier
        public ActionResult GetSupplier()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Supplier";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetSupplierList"] = suppliers = GetSupplierList();
                return View("Supplier");
            }
        }

        // GET: Supplier/Supplier
        public ActionResult AddSupplier(int Id)
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Supplier";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                SupplierModel supplier = new SupplierModel();

                if (Id > 0)
                {
                    supplier = GetSupplierDetails(Id);
                }
                return View("AddSupplier", supplier);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Save(SupplierModel supplier)
        {
            string userid = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            string username = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserId = userid;
            ViewBag.UserName = username;

            ViewBag.ActivePage = "Settings";


            string sessionid = (System.Web.HttpContext.Current.Session["SessionId"] != null) ? System.Web.HttpContext.Current.Session["SessionId"].ToString() : String.Empty;
            if (string.IsNullOrEmpty(sessionid))
            {
                return RedirectToAction("LogOut", "Account");
            }
            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }

            if (supplier.Id == 0)
            {
                ModelState.Remove("Id");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Supplier/AddorUpdateSupplier/", supplier, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            throw new ApplicationException("Error occured while updating user.");
                        }
                        else
                        {
                            var userEntry = response.Content.ReadAsStringAsync().Result;
                            // long res = Convert.ToInt64(userEntry);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList();

                return View("AddSupplier", supplier);
            }
            return RedirectToAction("GetSupplier", new { userId = userid, userName = username });
        }

        public object DeleteSupplier(DeleteSupplier delete)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Supplier/DeleteSupplier", delete, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }

        #region get all Supplier list details 
        private static List<SupplierModel> GetSupplierList()
        {
            List<SupplierModel> Model = new List<SupplierModel>();

            try
            {
                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("Application/json"));

                HttpResponseMessage response = httpClient.GetAsync("api/Supplier/GetSupplierList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SupplierModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get supplier details 
        private static SupplierModel GetSupplierDetails(int Id)
        {
            SupplierModel Model = new SupplierModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Supplier/GetSupplierDetails?SupplierId=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<SupplierModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion
    }
}