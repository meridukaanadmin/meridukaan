﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Meridukan_WEBUI.Areas.Supplier.Models
{
    public class SupplierModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [MaxLength(50)]        
        [Required(ErrorMessage = "Please enter First Name")]        
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string MiddleName { get; set; }

        [MaxLength(50)]
        [Required(ErrorMessage = "Please enter Last Name")]
        public string LastName { get; set; }

        [MaxLength(50)]
        [EmailAddress(ErrorMessage = "Please enter valid Email Id")]
        public string Email { get; set; }

        [StringLength(10,ErrorMessage = "Maximum 10 digits allowed")]
        [DataType(DataType.PhoneNumber)]
        [Phone(ErrorMessage = "Please enter Valid Mobile Number")]
        [Required(ErrorMessage = "Please enter Mobile Number")]        
        public string Mobile { get; set; }

        [MaxLength(200)]
        public string Address { get; set; }

        [MaxLength(20)]
        public string GST { get; set; }
    }

    public class DeleteSupplier
    {
        public int Id { get; set; }
    }
}