﻿using Meridukan_WEBUI.Areas.Store.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Store.Controllers
{
    public class StoreController : BaseController
    {
        // GET: Store/Store
        public ActionResult Index()
        {
            return View();
        }

        // GET: Store/Store
        public ActionResult GetStore()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Store";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetStoreList"] = GetStoreList();
                return View("Store");
            }
        }

        public ActionResult StoreDetails(int Id)
        {
            StoreModel store = new StoreModel();
            
            if (Id > 0)
            {
                store = GetStoreDetails(Id);
            }

            return PartialView("StoreDetails", store);
        }

        #region get all store list details 
        private static List<StoreModel> GetStoreList()
        {
            List<StoreModel> storeModel = new List<StoreModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Store/GetStoreList").Result;

                if (response.IsSuccessStatusCode)
                {
                    storeModel = response.Content.ReadAsAsync<List<StoreModel>>().Result;
                }
                else
                {
                    return storeModel;
                }
            }
            catch (Exception ex)
            {
                return storeModel;
                throw ex;
            }
            return storeModel;
        }
        #endregion

        #region get store details 
        private static StoreModel GetStoreDetails(int Id)
        {
            StoreModel storeModel = new StoreModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Store/GetStoreDetails?StoreId=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    storeModel = response.Content.ReadAsAsync<StoreModel>().Result;
                }
                else
                {
                    return storeModel;
                }
            }
            catch (Exception ex)
            {
                return storeModel;
                throw ex;
            }
            return storeModel;
        }
        #endregion

        public object AddOrUpdateStore(StoreModel store)
        {
            bool res = false;

            object data = new object();
            string desc = "";
            try
            {
                if (string.IsNullOrEmpty(store.Name))
                {
                    desc = "Store Name is required";
                    ModelState.AddModelError("Name", "Store Name is required");
                }

                if (ModelState.IsValid)
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Store/AddorUpdateStore/", store, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = false;
                            desc = "Error Occured";
                        }
                        else
                        {
                            var Entry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            desc = "Store Saved Successfully";
                        }
                    }
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList();

                    res = false;
                    desc = "Error Occured";
                    // return View("Brand", brand);
                }

                data = new
                {
                    Result = res,
                    Description = desc
                };

                return res;
            }
            catch (Exception ex)
            {
                data = new
                {
                    Result = res,
                    Description = ex.Message
                };

                return res;
            }
        }

        public object DeleteStore(DeleteStore deleteStore)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Store/DeleteStore", deleteStore, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }
    }
}