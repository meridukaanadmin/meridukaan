﻿using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Store
{
    public class StoreAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Store";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Store_default",
                "Store/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new { controller = "Store" },
                new[] { "Meridukan_WEBUI.Areas.Store.Controllers" }
            );
        }
    }
}