﻿namespace Meridukan_WEBUI.Areas.Size.Models
{
    public class SizeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class DeleteSize
    {
        public int Id { get; set; }        
    }
}