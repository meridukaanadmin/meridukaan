﻿using Meridukan_WEBUI.Areas.Size.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Size.Controllers
{
    public class SizeController : BaseController
    {
        List<SizeModel> sizes;

        public SizeController()
        {
            sizes = new List<SizeModel>();
        }

        // GET: Size/Size
        public ActionResult Index()
        {
            return View();
        }

        // GET: Size/Size
        public ActionResult GetSize()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Size";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetSizeList"] = sizes = GetSizeList();
                return View("Size");
            }
        }

        public ActionResult SizeDetails(int Id)
        {
            SizeModel size = new SizeModel();            
            if (Id > 0)
            {
                size = GetSizeDetails(Id);
            }

            return PartialView("SizeDetails", size);
        }

        public object AddOrUpdateSize(SizeModel size)
        {
            bool res = false;

            object data = new object();
            string desc = "";
            try
            {
                if (string.IsNullOrEmpty(size.Name))
                {
                    desc = "Size Name is required";
                    ModelState.AddModelError("Name", "Size Name is required");
                }

                var lst = sizes.Where(d => d.Name == size.Name);

                if (lst.Count() > 0)
                {
                    desc = "Size Name already exists";
                    ModelState.AddModelError("Exists", "Size Name already exists");
                }

                if (ModelState.IsValid)
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Size/AddorUpdateSize/", size, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = false;
                            desc = "Error Occured";
                        }
                        else
                        {
                            var brandEntry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            desc = "Saved Successfully";
                        }
                    }
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList();

                    res = false;
                    desc = "Error Occured";
                    // return View("Brand", brand);
                }

                data = new
                {
                    Result = res,
                    Description = desc
                };

                return res;
            }
            catch (Exception ex)
            {
                data = new
                {
                    Result = res,
                    Description = ex.Message
                };

                return res;
            }
        }

        public object DeleteSize(DeleteSize delete)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Size/DeleteSize", delete, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }
        
        #region get all Size list details 
        private static List<SizeModel> GetSizeList()
        {
            List<SizeModel> sizeModel = new List<SizeModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Size/GetSizeList").Result;

                if (response.IsSuccessStatusCode)
                {
                    sizeModel = response.Content.ReadAsAsync<List<SizeModel>>().Result;
                }
                else
                {
                    return sizeModel;
                }
            }
            catch (Exception ex)
            {
                return sizeModel;
                throw ex;
            }
            return sizeModel;
        }
        #endregion

        #region get Size details 
        private static SizeModel GetSizeDetails(int Id)
        {
            SizeModel Model = new SizeModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Size/GetSizeDetails?Id=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<SizeModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion
    }
}