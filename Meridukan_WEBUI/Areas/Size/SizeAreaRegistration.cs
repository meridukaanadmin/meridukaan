﻿using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Size
{
    public class SizeAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Size";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Size_default",
                "Size/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new { controller = "Size" },
                new[] { "Meridukan_WEBUI.Areas.Size.Controllers" }
            );

            context.MapRoute(
              "GetSize_default",
              "Size/{controller}/{action}/{id}",
              new { action = "GetSize", id = UrlParameter.Optional },
              new { controller = "Size" },
              new[] { "Meridukan_WEBUI.Areas.Size.Controllers" }
          );
        }
    }
}