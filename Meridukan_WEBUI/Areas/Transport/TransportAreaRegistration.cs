﻿using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Transport
{
    public class TransportAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Transport";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Transport_default",
                "Transport/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new { controller = "Transport" },
                new[] { "Meridukan_WEBUI.Areas.Transport.Controllers" }
            );
        }
    }
}