﻿using Meridukan_WEBUI.Areas.Transport.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Transport.Controllers
{
    public class TransportController : BaseController
    {
        // GET: Transport/Transport
        public ActionResult Index()
        {
            return View();
        }

        // GET: Transport/Transport
        public ActionResult GetTransport()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Transport";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetTransportList"] = GetTransportList();
                return View("Transport");
            }
        }

        public ActionResult TransportDetails(int Id)
        {
            TransportModel transport = new TransportModel();           
            if (Id > 0)
            {
                transport = GetTransportDetails(Id);
            }

            return PartialView("TransportDetails", transport);
        }

        public object AddOrUpdateTransport(TransportModel transport)
        {
            bool res = false;

            object data = new object();
            string desc = "";
            try
            {
                if (ModelState.IsValid)
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Transport/AddorUpdateTransportDetails/", transport, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = false;
                            desc = "Error Occured";
                        }
                        else
                        {
                            var brandEntry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            desc = "Saved Successfully";
                        }
                    }
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList();

                    res = false;
                    desc = "Error Occured";
                    // return View("Brand", brand);
                }

                data = new
                {
                    Result = res,
                    Description = desc
                };

                return res;
            }
            catch (Exception ex)
            {
                data = new
                {
                    Result = res,
                    Description = ex.Message
                };

                return res;
            }
        }

        public object DeleteTransport(DeleteTransport delete)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Transport/DeleteTransportDetails", delete, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }

        #region get all Transport list details 
        private static List<TransportModel> GetTransportList()
        {
            List<TransportModel> Model = new List<TransportModel>();

            try
            {  

                HttpResponseMessage response = httpClient.GetAsync("api/Transport/GetTransportList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<TransportModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get Transport details 
        private static TransportModel GetTransportDetails(int Id)
        {
            TransportModel Model = new TransportModel();

            try
            {
             
                HttpResponseMessage response = httpClient.GetAsync("api/Transport/GetTransportDetails?Id=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<TransportModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion
    }
}