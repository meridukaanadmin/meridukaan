﻿using Meridukan_WEBUI.Areas.Tax.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Tax.Controllers
{
    public class TaxController : BaseController
    {
        List<TaxModel> Taxs;
        public TaxController()
        {
            Taxs = new List<TaxModel>();
        }

        // GET: Tax/Tax
        public ActionResult Index()
        {
            return View();
        }

        // GET: Tax/Tax
        public ActionResult GetTax()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Tax";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetTaxList"] = Taxs = GetTaxList();
                return View("Tax");
            }
        }


        public ActionResult TaxDetails(int Id)
        {
            TaxModel Tax = new TaxModel();
            //frnds = db.FriendsInfo.Find(Id);
            if (Id > 0)
            {
                Tax = GetTaxDetails(Id);
            }

            return PartialView("TaxDetails", Tax);
        }


        public object AddOrUpdateTax(TaxModel Tax)
        {
            bool res = false;

            object data = new object();
            string desc = "";
            try
            {
                if (string.IsNullOrEmpty(Tax.Name))
                {
                    desc = "Tax Name is required";
                    ModelState.AddModelError("Name", "Tax Name is required");
                }

                var lst = Taxs.Where(d => d.Name == Tax.Name);

                if (lst.Count() > 0)
                {
                    desc = "Tax Name already exists";
                    ModelState.AddModelError("Exists", "Tax Name already exists");
                }

                if (ModelState.IsValid)
                {
                    HttpClient httpClient = new HttpClient
                    {
                        BaseAddress = new Uri(ConfigurationManager.AppSettings["apiUrl"])
                    };

                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Tax/AddorUpdateTax/", Tax, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = false;
                            desc = "Error Occured";
                        }
                        else
                        {
                            var TaxEntry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            desc = "Tax Saved Successfully";
                        }
                    }
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList();

                    res = false;
                    desc = "Error Occured";
                    // return View("Tax", Tax);
                }

                data = new
                {
                    Result = res,
                    Description = desc
                };

                return res;
            }
            catch (Exception ex)
            {
                data = new
                {
                    Result = res,
                    Description = ex.Message
                };

                return res;
            }
        }

        public object DeleteTax(DeleteTax deleteTax)
        {
            bool res = false;

            try
            {
                HttpClient httpClient = new HttpClient
                {
                    BaseAddress = new Uri(ConfigurationManager.AppSettings["apiUrl"])
                };

                TaxModel Tax = new TaxModel();
                Tax.Id = deleteTax.Id; 

                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Tax/DeleteTax", Tax, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var TaxEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }



        #region get all Tax list details 
        private static List<TaxModel> GetTaxList()
        {
            List<TaxModel> TaxModel = new List<TaxModel>();

            try
            {
                HttpClient httpClient = new HttpClient
                {
                    BaseAddress = new Uri(ConfigurationManager.AppSettings["apiUrl"])

                };

                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("Application/json"));

                HttpResponseMessage response = httpClient.GetAsync("api/Tax/GetTaxList").Result;

                if (response.IsSuccessStatusCode)
                {
                    TaxModel = response.Content.ReadAsAsync<List<TaxModel>>().Result;
                }
                else
                {
                    return TaxModel;
                }
            }
            catch (Exception ex)
            {
                return TaxModel;
                throw ex;
            }
            return TaxModel;
        }
        #endregion


        #region get Tax details 
        private static TaxModel GetTaxDetails(int Id)
        {
            TaxModel TaxModel = new TaxModel();

            try
            {
                HttpClient httpClient = new HttpClient
                {
                    BaseAddress = new Uri(ConfigurationManager.AppSettings["apiUrl"])
                };
                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("Application/json"));

                HttpResponseMessage response = httpClient.GetAsync("api/Tax/GetTaxDetails?TaxId=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    TaxModel = response.Content.ReadAsAsync<TaxModel>().Result;
                }
                else
                {
                    return TaxModel;
                }
            }
            catch (Exception ex)
            {
                return TaxModel;
                throw ex;
            }
            return TaxModel;
        }
        #endregion
    }
}