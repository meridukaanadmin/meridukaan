﻿using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Tax
{
    public class TaxAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Tax";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Tax_default",
                "Tax/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new { controller = "Tax" },
                new[] { "Meridukan_WEBUI.Areas.Tax.Controllers" }
               
            );
        }
    }
}