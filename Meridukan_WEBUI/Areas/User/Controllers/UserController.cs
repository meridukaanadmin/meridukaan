﻿using Meridukan_WEBUI.Areas.Store.Models;
using Meridukan_WEBUI.Areas.User.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.User.Controllers
{
    [OutputCache(Duration = 0)]
    public class UserController : BaseController
    {
        // GET: User/User
        public ActionResult Index()
        {
            return View();
        }

        // GET: User/User
        public ActionResult User()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "User";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetUserList"] = GetuserList();
                return View();
            }
        }

        // GET: User/User
        public ActionResult AddUser(int Id)
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "User";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                UserModel user = new UserModel();
                user.Roles = new List<RoleModel>();

                if (Id > 0)
                {
                    user = GetUserDetails(Id);
                }
                else
                {
                    user.Roles = GetRoleList();
                }
                
                ViewBag.StoreList = new SelectList(GetStoreList(), "Id", "Name");

                return View("AddUser", user);
            }
        }

        [HttpPost]
        public async Task<ActionResult> AddUser(UserModel user)
        {
            string userid = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            string username = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserId = userid;
            ViewBag.UserName = username;           

            string sessionid = (System.Web.HttpContext.Current.Session["SessionId"] != null) ? System.Web.HttpContext.Current.Session["SessionId"].ToString() : String.Empty;
            if (string.IsNullOrEmpty(sessionid))
            {
                return RedirectToAction("LogOut", "Account");
            }
            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                if(userid == "")
                {
                    userid = Convert.ToString(AppUser.Id);
                    username = AppUser.UserName;
                }
            }
           

            if (ModelState.IsValid)
            {

                var itemToRemove = user.Roles.Where(r => r.IsSelected == false).ToList();

                foreach (var item in itemToRemove)
                {
                    user.Roles.Remove(item);
                }                

                try
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/User/AddorUpdateUser/", user, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            throw new ApplicationException("Error occured while updating user.");
                        }
                        else
                        {
                            var userEntry = response.Content.ReadAsStringAsync().Result;
                            // long res = Convert.ToInt64(userEntry);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList();

                user.Roles = GetRoleList();
                ViewBag.StoreList = new SelectList(GetStoreList(), "Id", "Name");

                return View(user);
            }
            return RedirectToAction("User", new { userId = userid, userName = username });
        }

        public object DeleteUser(DeleteUser delete)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/User/DeleteUser", delete, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }

        private static List<RoleModel> GetRoleList()
        {
            List<RoleModel> Model = new List<RoleModel>();

            try
            {
                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("Application/json"));

                HttpResponseMessage response = httpClient.GetAsync("api/Role/GetRoleList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<RoleModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        private static List<StoreModel> GetStoreList()
        {
            List<StoreModel> storeModel = new List<StoreModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Store/GetStoreList").Result;

                if (response.IsSuccessStatusCode)
                {
                    storeModel = response.Content.ReadAsAsync<List<StoreModel>>().Result;
                }
                else
                {
                    return storeModel;
                }
            }
            catch (Exception ex)
            {
                return storeModel;
                throw ex;
            }
            return storeModel;
        }

        #region get all User list details 
        private static List<UserModel> GetuserList()
        {
            List<UserModel> Model = new List<UserModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/User/GetUserList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<UserModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get user details 
        private static UserModel GetUserDetails(int Id)
        {
            UserModel Model = new UserModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/User/GetUserDetails?UserId=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<UserModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion
    }
}