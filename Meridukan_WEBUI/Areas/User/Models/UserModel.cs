﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Meridukan_WEBUI.Areas.User.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter the User Name")]
        public string Username { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Please enter First Name")]
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Please enter Last Name")]
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }

        [Required]
        [Display(Name = "Mobile Number")]
        [DataType(DataType.PhoneNumber)]
        [StringLength(13, ErrorMessage = "Enter Valid Phone Number", MinimumLength = 10)]
        public string Phone { get; set; }
        
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string GSTNumer { get; set; }
        public DateTime LastLoginDate { get; set; }

        [Required(ErrorMessage = "Please select the store")]
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public int MainUserId { get; set; }        
        
        public List<RoleModel> Roles { get; set; }      
    }

    public class RoleModel
    {
        public bool IsSelected { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class DeleteRole
    {   
        public int Id { get; set; }     
    }

    public class DeleteUser
    {
        public int UserId { get; set; }
    }
}