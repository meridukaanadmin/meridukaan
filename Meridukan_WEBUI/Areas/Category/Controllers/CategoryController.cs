﻿using Meridukan_WEBUI.Areas.Category.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Category.Controllers
{
    public class CategoryController : BaseController
    {
        List<CategoryModel> categories;

        public CategoryController()
        {
            categories = new List<CategoryModel>();
        }
        
        // GET: Category/Category
        public ActionResult Index()
        {
            return View();
        }

        // GET: Category/Category
        public ActionResult GetCategory()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Category";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetCategoryList"] = GetCategoryList();
                return View("Category");
            }

        }

        #region get all category list details 
        private static List<CategoryModel> GetCategoryList()
        {
            List<CategoryModel> catgoryModel = new List<CategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Category/GetCategoryList").Result;

                if (response.IsSuccessStatusCode)
                {
                    catgoryModel = response.Content.ReadAsAsync<List<CategoryModel>>().Result;
                }
                else
                {
                    return catgoryModel;
                }
            }
            catch (Exception ex)
            {
                return catgoryModel;
                throw ex;
            }
            return catgoryModel;
        }
        #endregion

        public ActionResult CategoryDetails(int Id)
        {
            CategoryModel Category = new CategoryModel();
            //frnds = db.FriendsInfo.Find(Id);
            if (Id > 0)
            {
                Category = GetCategoryDetails(Id);
            } 

            return PartialView("CategoryDetails", Category);
        }

        public object AddOrUpdateCategory(CategoryModel category)
        {
            bool res = false;

            object data = new object();
            string desc = "";
            try
            {
                if (string.IsNullOrEmpty(category.Name))
                {
                    desc = "Brand Name is required";
                    ModelState.AddModelError("Name", "Category Name is required");
                }

                var lst = categories.Where(d => d.Name == category.Name);

                if (lst.Count() > 0)
                {
                    desc = "Brand Name already exists";
                    ModelState.AddModelError("Exists", "Category Name already exists");
                }

                if (ModelState.IsValid)
                {
                    HttpClient httpClient = new HttpClient
                    {
                        BaseAddress = new Uri(ConfigurationManager.AppSettings["apiUrl"])
                    };

                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Category/AddorUpdateCategory/", category, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = false;
                            desc = "Error Occured";
                        }
                        else
                        {
                            var categoryEntry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            desc = "Category Saved Successfully";
                        }
                    }
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList();

                    res = false;
                    desc = "Error Occured";
                    // return View("Brand", brand);
                }

                data = new
                {
                    Result = res,
                    Description = desc
                };

                return res;
            }
            catch (Exception ex)
            {
                data = new
                {
                    Result = res,
                    Description = ex.Message
                };

                return res;
            }
        }

        public object DeleteCategory(DeleteCategory deleteCategory)
        {
            bool res = false;

            try
            {
                HttpClient httpClient = new HttpClient
                {
                    BaseAddress = new Uri(ConfigurationManager.AppSettings["apiUrl"])
                };

                CategoryModel category = new CategoryModel();
                category.Id = deleteCategory.CategoryId;
                category.ImagePath = "";
                category.Name = "";

                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Category/DeleteCategory", deleteCategory, new JsonMediaTypeFormatter()).Result;
                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadFile()
        {
            string _imgname = string.Empty;
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["MyImages"];
                if (pic.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(pic.FileName);
                    var _ext = Path.GetExtension(pic.FileName);

                    _imgname = Guid.NewGuid().ToString();
                    var _comPath = Server.MapPath("/Uploads/category_") + _imgname + _ext;
                    _imgname = "category_" + _imgname + _ext;

                    ViewBag.Msg = _comPath;
                    var path = _comPath;

                    // Saving Image in Original Mode
                    pic.SaveAs(path);

                    // resizing image
                    MemoryStream ms = new MemoryStream();
                    WebImage img = new WebImage(_comPath);

                    if (img.Width > 200)
                        img.Resize(200, 200);
                    img.Save(_comPath);
                    // end resize
                }
            }
            return Json(Convert.ToString(_imgname), JsonRequestBehavior.AllowGet);
        }

        #region get Category details 
        private static CategoryModel GetCategoryDetails(int Id)
        {
            CategoryModel categoryModel = new CategoryModel();

            try
            {
                HttpClient httpClient = new HttpClient
                {
                    BaseAddress = new Uri(ConfigurationManager.AppSettings["apiUrl"])
                };
                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("Application/json"));

                HttpResponseMessage response = httpClient.GetAsync("api/Category/GetCategoryDetails?CategoryId=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    categoryModel = response.Content.ReadAsAsync<CategoryModel>().Result;
                }
                else
                {
                    return categoryModel;
                }
            }
            catch (Exception ex)
            {
                return categoryModel;
                throw ex;
            }
            return categoryModel;
        }
        #endregion


    }
}