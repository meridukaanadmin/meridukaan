﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_WEBUI.Areas.Category.Models
{
    public class CategoryModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
    }
    public class DeleteCategory
    {
        public int CategoryId { get; set; }
    }
}