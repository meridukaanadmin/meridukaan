﻿using Meridukan_WEBUI.Areas.Brand.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Brand.Controllers
{
    public class BrandController : BaseController
    {
        List<BrandModel> brands;

        public BrandController()
        {
            brands = new List<BrandModel>();
        }

        // GET: Brand/Brand
        public ActionResult Index()
        {
            return View();
        }


        // GET: Brand/Brand
        public ActionResult GetBrand()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Brand";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetBrandList"] = brands = GetBrandList();
                return View("Brand");
            }
        }

        public ActionResult BrandDetails(int Id)
        {
            BrandModel brand = new BrandModel();
            //frnds = db.FriendsInfo.Find(Id);
            if (Id > 0)
            {
                brand = GetBrandDetails(Id);
            }

            return PartialView("BrandDetails", brand);
        }
         
        public object AddOrUpdateBrand(BrandModel brand)
        {
            bool res = false;

            object data = new object();
            string desc = "";
            try
            {   
                if (string.IsNullOrEmpty(brand.Name))
                {
                    desc = "Brand Name is required";
                    ModelState.AddModelError("Name", "Brand Name is required");
                }

                var lst = brands.Where(d => d.Name == brand.Name);

                if (lst.Count() > 0)
                {
                    desc = "Brand Name already exists";
                    ModelState.AddModelError("Exists", "Brand Name already exists");
                }

                if (ModelState.IsValid)
                {
                    HttpClient httpClient = new HttpClient
                    {
                        BaseAddress = new Uri(ConfigurationManager.AppSettings["apiUrl"])
                    };

                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Brand/AddorUpdateBrand/", brand, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = false;
                            desc = "Error Occured";
                        }
                        else
                        {
                            var brandEntry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            desc = "Brand Saved Successfully";
                        }
                    }
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList();

                    res = false;
                    desc = "Error Occured";
                   // return View("Brand", brand);
                }

                data = new
                {
                    Result = res,
                    Description = desc
                };

                return res;
            }
            catch(Exception ex)
            {
                data = new
                {
                    Result = res,
                    Description = ex.Message
                };

                return res;
            }
        }

        public object DeleteBrand(DeleteBrand deleteBrand)
        {
            bool res = false;
           
            try
            {
                HttpClient httpClient = new HttpClient
                {
                    BaseAddress = new Uri(ConfigurationManager.AppSettings["apiUrl"])
                };

                BrandModel brand = new BrandModel();
                brand.Id = deleteBrand.BrandId;
                brand.ImagePath = "";
                brand.Name = "";

                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Brand/DeleteBrand", brand, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {                        
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadFile()
        {
            string _imgname = string.Empty;
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["MyImages"];
                if (pic.ContentLength > 0)
                 {
                    var fileName = Path.GetFileName(pic.FileName);
                    var _ext = Path.GetExtension(pic.FileName);

                    _imgname = Guid.NewGuid().ToString();
                    var _comPath = Server.MapPath("/Uploads/Brand_") + _imgname + _ext;
                    _imgname = "Brand_" + _imgname + _ext;

                    ViewBag.Msg = _comPath;
                    var path = _comPath;

                    // Saving Image in Original Mode
                    pic.SaveAs(path);

                    // resizing image
                    MemoryStream ms = new MemoryStream();
                    WebImage img = new WebImage(_comPath);

                    if (img.Width > 200)
                        img.Resize(200, 200);
                    img.Save(_comPath);
                    // end resize
                }
            }
            return Json(Convert.ToString(_imgname), JsonRequestBehavior.AllowGet);
        }

        #region get all Brand list details 
        private static List<BrandModel> GetBrandList()
        {
            List<BrandModel> brandModel = new List<BrandModel>();
            
            try
            {
                HttpClient httpClient = new HttpClient
                {
                    BaseAddress = new Uri(ConfigurationManager.AppSettings["apiUrl"])
                    
                };
                
                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("Application/json"));

                HttpResponseMessage response = httpClient.GetAsync("api/Brand/GetBrandList").Result;

                if (response.IsSuccessStatusCode)
                {
                    brandModel = response.Content.ReadAsAsync<List<BrandModel>>().Result;
                }
                else
                {
                    return brandModel;
                }
            }
            catch (Exception ex)
            {
                return brandModel;
                throw ex;
            }
            return brandModel;
        }
        #endregion

        #region get Brand details 
        private static BrandModel GetBrandDetails(int Id)
        {
            BrandModel brandModel = new BrandModel();

            try
            {
                HttpClient httpClient = new HttpClient
                {
                    BaseAddress = new Uri(ConfigurationManager.AppSettings["apiUrl"])
                };
                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("Application/json"));

                HttpResponseMessage response = httpClient.GetAsync("api/Brand/GetBrandDetails?BrandId=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    brandModel = response.Content.ReadAsAsync<BrandModel>().Result;
                }
                else
                {
                    return brandModel;
                }
            }
            catch (Exception ex)
            {
                return brandModel;
                throw ex;
            }
            return brandModel;
        }
        #endregion
    }
}