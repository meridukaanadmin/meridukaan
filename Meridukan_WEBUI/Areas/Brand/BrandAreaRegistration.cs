﻿using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Brand
{
    public class BrandAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Brand";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Brand_default",
                "Brand/{controller}/{action}/{id}",
                //"Brand/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new { controller = "Brand" },
                new[] { "Meridukan_WEBUI.Areas.Brand.Controllers" }
            );
        }
    }
}