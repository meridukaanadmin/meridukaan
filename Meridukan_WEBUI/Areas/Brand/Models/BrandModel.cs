﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Meridukan_WEBUI.Areas.Brand.Models
{
    public class BrandModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter Brand Name")]
        [MaxLength(50)]
        public string Name { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public int brandId { get; set; }
    }

    public class DeleteBrand
    {
        public int BrandId { get; set; }
    }
}