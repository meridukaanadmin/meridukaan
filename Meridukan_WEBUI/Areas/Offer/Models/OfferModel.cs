﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_WEBUI.Areas.Offer.Models
{
    public class OfferModel
    {
        public int Id { get; set; }
        public string OfferName { get; set; }
        public string OfferImagePath { get; set; }
        public DateTime EnteredDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public List<OfferDetailModel> offerDetails { get; set; }
    }

    public class OfferDetailModel
    {
        public int OfferId { get; set; }
        public int BrandId { get; set; }
        public string Brand { get; set; }
        public int CategoryId { get; set; }
        public string Category { get; set; }
        public decimal DiscountInPerc { get; set; }
        public decimal DiscountInAmount { get; set; }
    }

    public class DeleteOffer
    {
        public int OfferId { get; set; }
    }
}