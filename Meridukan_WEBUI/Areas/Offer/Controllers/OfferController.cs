﻿using Meridukan_WEBUI.Areas.Brand.Models;
using Meridukan_WEBUI.Areas.Category.Models;
using Meridukan_WEBUI.Areas.Offer.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Offer.Controllers
{
    public class OfferController : BaseController
    {
        // GET: Offer/Offer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetOfferList()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Offer";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetOfferList"] = GetOfferDataList();
                return View("GetOfferList");
            }
        }

        private static List<OfferModel> GetOfferDataList()
        {
            List<OfferModel> Model = new List<OfferModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Offer/GetOfferList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<OfferModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        public ActionResult AddOffer(int Id)
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Offer";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                OfferModel offer = new OfferModel();
                offer.offerDetails = new List<OfferDetailModel>();

                if (Id > 0)
                {
                    offer = GetOfferDetails(Id);
                }
                else
                {
                    offer.ExpiryDate = DateTime.Today.AddDays(0);                    
                }

                ViewBag.BrandList = new SelectList(GetBrandList(), "Id", "Name");
                ViewBag.CategoryList = new SelectList(GetCategoryList(), "Id", "Name");
                return View("AddOffer", offer);
            }
        }

        private static OfferModel GetOfferDetails(int Id)
        {
            OfferModel Model = new OfferModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Offer/GetOfferDetails?Id=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<OfferModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        #region get all Brand list details 
        private static List<BrandModel> GetBrandList()
        {
            List<BrandModel> brandModel = new List<BrandModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Brand/GetBrandList").Result;

                if (response.IsSuccessStatusCode)
                {
                    brandModel = response.Content.ReadAsAsync<List<BrandModel>>().Result;
                }
                else
                {
                    return brandModel;
                }
            }
            catch (Exception ex)
            {
                return brandModel;
                throw ex;
            }
            return brandModel;
        }
        #endregion

        #region get all category list details 
        private static List<CategoryModel> GetCategoryList()
        {
            List<CategoryModel> catgoryModel = new List<CategoryModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Category/GetCategoryList").Result;

                if (response.IsSuccessStatusCode)
                {
                    catgoryModel = response.Content.ReadAsAsync<List<CategoryModel>>().Result;
                }
                else
                {
                    return catgoryModel;
                }
            }
            catch (Exception ex)
            {
                return catgoryModel;
                throw ex;
            }
            return catgoryModel;
        }
        #endregion

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadFile()
        {
            string _imgname = string.Empty;
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var pic = System.Web.HttpContext.Current.Request.Files["MyImages"];
                if (pic.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(pic.FileName);
                    var _ext = Path.GetExtension(pic.FileName);

                    _imgname = Guid.NewGuid().ToString();
                    var _comPath = Server.MapPath("/OfferBanner/offer_") + _imgname + _ext;
                    _imgname = "offer_" + _imgname + _ext;

                    ViewBag.Msg = _comPath;
                    var path = _comPath;

                    // Saving Image in Original Mode
                    pic.SaveAs(path);

                    // resizing image
                    MemoryStream ms = new MemoryStream();
                    WebImage img = new WebImage(_comPath);

                    if (img.Width > 200)
                        img.Resize(200, 200);
                    img.Save(_comPath);
                    // end resize
                }
            }
            return Json(Convert.ToString(_imgname), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Save(OfferModel offer)
        {
            ViewBag.UserId = offer.UserId;
            ViewBag.UserName = offer.UserName;

            ViewBag.ActivePage = "Offer";

            bool res = false;

            //if (ModelState.IsValid)
            //{
            try
            {
                offer.EnteredDate = DateTime.Now;

                using (httpClient)
                {
                    HttpResponseMessage response = httpClient.PostAsync("api/Offer/AddorUpdateOffer/", offer, new JsonMediaTypeFormatter()).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var userEntry = response.Content.ReadAsStringAsync().Result;
                        res = true;
                        // long res = Convert.ToInt64(userEntry);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //}
            //else
            //{
            //    var errors = ModelState.Select(x => x.Value.Errors)
            //               .Where(y => y.Count > 0)
            //               .ToList();

            //    //return View("AddPO", purchase);
            //}
            return Json(res);
        }
    }
}