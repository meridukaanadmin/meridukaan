﻿namespace Meridukan_WEBUI.Areas.Salesman.Models
{
    public class SalesmanModel
    {
        public int Id { get; set; }
        public string SalesmanNo { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
    }

    public class DeleteSalesman
    {
        public int Id { get; set; }
    }
}