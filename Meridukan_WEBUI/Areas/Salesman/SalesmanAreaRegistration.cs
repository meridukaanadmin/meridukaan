﻿using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Salesman
{
    public class SalesmanAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Salesman";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Salesman_default",
                "Salesman/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new { controller = "Salesman" },
                new[] { "Meridukan_WEBUI.Areas.Salesman.Controllers" }
            );
        }
    }
}