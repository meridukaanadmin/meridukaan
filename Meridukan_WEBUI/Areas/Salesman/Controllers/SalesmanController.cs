﻿using Meridukan_WEBUI.Areas.Salesman.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.Salesman.Controllers
{
    public class SalesmanController : BaseController
    {
        // GET: Salesman/Salesman
        public ActionResult Index()
        {
            return View();
        }

        // GET: Salesman/Salesman
        public ActionResult GetSalesman()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Salesman";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetSalesmanList"] = GetSalesmanList();
                return View("Salesman");
            }
        }

        public ActionResult SalesmanDetails(int Id)
        {
            SalesmanModel salesman = new SalesmanModel();            
            if (Id > 0)
            {
                salesman = GetSalesmanDetails(Id);
            }

            return PartialView("SalesmanDetails", salesman);
        }

        public object AddOrUpdateSalesman(SalesmanModel salesman)
        {
            bool res = false;

            object data = new object();
            string desc = "";
            try
            {
                if (ModelState.IsValid)
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/Salesman/AddorUpdateSalesman/", salesman, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = false;
                            desc = "Error Occured";
                        }
                        else
                        {
                            var brandEntry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            desc = "Saved Successfully";
                        }
                    }
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList();

                    res = false;
                    desc = "Error Occured";
                    // return View("Brand", brand);
                }

                data = new
                {
                    Result = res,
                    Description = desc
                };

                return res;
            }
            catch (Exception ex)
            {
                data = new
                {
                    Result = res,
                    Description = ex.Message
                };

                return res;
            }
        }

        public object DeleteSalesman(DeleteSalesman delete)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/Salesman/DeleteSalesman", delete, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }


        private static List<SalesmanModel> GetSalesmanList()
        {
            List<SalesmanModel> Model = new List<SalesmanModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Salesman/GetSalesmanList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<SalesmanModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }

        private static SalesmanModel GetSalesmanDetails(int Id)
        {
            SalesmanModel Model = new SalesmanModel();

            try
            {  

                HttpResponseMessage response = httpClient.GetAsync("api/Salesman/GetSalesmanDetails?Id=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<SalesmanModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
    }
}