﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_WEBUI.Areas.DiscountType.Models
{
    public class DiscountTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class DeleteDiscountType
    {
        public int Id { get; set; }
    }
}