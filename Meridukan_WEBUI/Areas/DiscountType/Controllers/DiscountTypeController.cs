﻿using Meridukan_WEBUI.Areas.DiscountType.Models;
using Meridukan_WEBUI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.DiscountType.Controllers
{
    public class DiscountTypeController : BaseController
    {
        // GET: DiscountType/DiscountType
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetDiscountType()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "DiscountType";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                ViewData["GetDiscountTypeList"] = GetDiscountTypeList();
                return View("GetDiscountType");
            }
        }

        public ActionResult DiscountTypeDetails(int Id)
        {
            DiscountTypeModel model = new DiscountTypeModel();
            //frnds = db.FriendsInfo.Find(Id);
            if (Id > 0)
            {
                model = GetDiscountTypeDetails(Id);
            }

            return PartialView("DiscountTypeDetails", model);
        }

        public object AddOrUpdateDiscountType(DiscountTypeModel discount)
        {
            bool res = false;

            object data = new object();
            string desc = "";
            try
            {
                if (ModelState.IsValid)
                {
                    using (httpClient)
                    {
                        HttpResponseMessage response = httpClient.PostAsync("api/DiscountType/AddorUpdateDiscountType/", discount, new JsonMediaTypeFormatter()).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            res = false;
                            desc = "Error Occured";
                        }
                        else
                        {
                            var Entry = response.Content.ReadAsStringAsync().Result;
                            res = true;
                            desc = "Saved Successfully";
                        }
                    }
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors)
                               .Where(y => y.Count > 0)
                               .ToList();

                    res = false;
                    desc = "Error Occured";
                    // return View("Brand", brand);
                }

                data = new
                {
                    Result = res,
                    Description = desc
                };

                return res;
            }
            catch (Exception ex)
            {
                data = new
                {
                    Result = res,
                    Description = ex.Message
                };

                return res;
            }
        }

        public object DeleteDiscountType(DeleteDiscountType delete)
        {
            bool res = false;

            try
            {
                using (httpClient)
                {
                    HttpResponseMessage result = httpClient.PostAsync("api/DiscountType/DeleteDiscountTypeDetails", delete, new JsonMediaTypeFormatter()).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        res = false;
                    }
                    else
                    {
                        var brandEntry = result.Content.ReadAsStringAsync().Result;
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                return res;
            }
            return Json(res);
        }

        #region get all DiscountTypeModel list details 
        private static List<DiscountTypeModel> GetDiscountTypeList()
        {
            List<DiscountTypeModel> Model = new List<DiscountTypeModel>();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/DiscountType/GetDiscountTypeList").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<List<DiscountTypeModel>>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion

        #region get DiscountTypeModel details 
        private static DiscountTypeModel GetDiscountTypeDetails(int Id)
        {
            DiscountTypeModel Model = new DiscountTypeModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/DiscountType/GetDiscountTypeDetails?Id=" + Id).Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<DiscountTypeModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
        #endregion
    }
}