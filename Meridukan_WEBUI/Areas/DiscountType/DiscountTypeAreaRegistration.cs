﻿using System.Web.Mvc;

namespace Meridukan_WEBUI.Areas.DiscountType
{
    public class DiscountTypeAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "DiscountType";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "DiscountType_default",
                "DiscountType/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new { controller = "DiscountType" },
                new[] { "Meridukan_WEBUI.Areas.DiscountType.Controllers" }
            );
        }
    }
}