﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Meridukan_WEBUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );

            //------- masters ---------------//
            routes.MapRoute(
               name: "Store",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Store", action = "Store", id = UrlParameter.Optional },
               namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
           ).DataTokens["Area"] = "Store";

            routes.MapRoute(
               name: "Brand",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Brand", action = "Brand", id = UrlParameter.Optional },
               namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
           ).DataTokens["Area"] = "Brand";

            routes.MapRoute(
              name: "Category",
              url: "{controller}/{action}/{id}",
              defaults: new { controller = "Category", action = "Category", id = UrlParameter.Optional },
              namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
          ).DataTokens["Area"] = "Category";

            routes.MapRoute(
             name: "Subcategory",
             url: "{controller}/{action}/{id}",
             defaults: new { controller = "Subcategory", action = "Subcategory", id = UrlParameter.Optional },
             namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
         ).DataTokens["Area"] = "Subcategory";

            routes.MapRoute(
          name: "Substitudecategory",
          url: "{controller}/{action}/{id}",
          defaults: new { controller = "Substitudecategory", action = "Substitude", id = UrlParameter.Optional },
          namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
          ).DataTokens["Area"] = "Substitudecategory";

            routes.MapRoute(
         name: "Transport",
         url: "{controller}/{action}/{id}",
         defaults: new { controller = "Transport", action = "Transport", id = UrlParameter.Optional },
         namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
         ).DataTokens["Area"] = "Transport";

            routes.MapRoute(
        name: "Size",
        url: "{controller}/{action}/{id}",
        defaults: new { controller = "Size", action = "Size", id = UrlParameter.Optional },
        namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
        ).DataTokens["Area"] = "Size";

            routes.MapRoute(
          name: "Color",
          url: "{controller}/{action}/{id}",
          defaults: new { controller = "Color", action = "Color", id = UrlParameter.Optional },
          namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
          ).DataTokens["Area"] = "Color";

            routes.MapRoute(
         name: "Salesman",
         url: "{controller}/{action}/{id}",
         defaults: new { controller = "Salesman", action = "Salesman", id = UrlParameter.Optional },
         namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
         ).DataTokens["Area"] = "Salesman";

            routes.MapRoute(
        name: "Customer",
        url: "{controller}/{action}/{id}",
        defaults: new { controller = "Customer", action = "Customer", id = UrlParameter.Optional },
        namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
        ).DataTokens["Area"] = "Customer";

            //coupon
            routes.MapRoute(
           name: "DiscountType",
           url: "{controller}/{action}/{id}",
           defaults: new { controller = "DiscountType", action = "Index", id = UrlParameter.Optional },
           namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
           ).DataTokens["Area"] = "DiscountType";

            routes.MapRoute(
          name: "Coupon",
          url: "{controller}/{action}/{id}",
          defaults: new { controller = "Coupon", action = "Index", id = UrlParameter.Optional },
          namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
          ).DataTokens["Area"] = "Coupon";

            //Offer
            routes.MapRoute(
         name: "Offer",
         url: "{controller}/{action}/{id}",
         defaults: new { controller = "Offer", action = "Index", id = UrlParameter.Optional },
         namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
         ).DataTokens["Area"] = "Offer";


            // ------------- Inventory --------

            routes.MapRoute(
            name: "Tax",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "Tax", action = "Tax", id = UrlParameter.Optional },
            namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
            ).DataTokens["Area"] = "Tax";

            routes.MapRoute(
            name: "Product",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "Product", action = "Product", id = UrlParameter.Optional },
            namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
            ).DataTokens["Area"] = "Product";

            routes.MapRoute(
           name: "Supplier",
           url: "{controller}/{action}/{id}",
           defaults: new { controller = "Supplier", action = "Supplier", id = UrlParameter.Optional },
           namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
           ).DataTokens["Area"] = "Supplier";

            routes.MapRoute(
             name: "Purchase",
             url: "{controller}/{action}/{id}",
             defaults: new { controller = "Purchase", action = "Purchase", id = UrlParameter.Optional },
             namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
             ).DataTokens["Area"] = "Purchase";

            routes.MapRoute(
             name: "GRN",
             url: "{controller}/{action}/{id}",
             defaults: new { controller = "GRN", action = "GRN", id = UrlParameter.Optional },
             namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
             ).DataTokens["Area"] = "GRN";

            routes.MapRoute(
            name: "Sale",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "Sale", action = "Sale", id = UrlParameter.Optional },
            namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
            ).DataTokens["Area"] = "Sale";

            // ------------- User Setting --------

            routes.MapRoute(
            name: "Role",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "Role", action = "Role", id = UrlParameter.Optional },
            namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
            ).DataTokens["Area"] = "Role";

            routes.MapRoute(
            name: "User",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "User", action = "User", id = UrlParameter.Optional },
            namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
            ).DataTokens["Area"] = "User";

            routes.MapRoute(
            name: "DeliveryPinCode",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "DeliveryPinCode", action = "Index", id = UrlParameter.Optional },
            namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
            ).DataTokens["Area"] = "DeliveryPinCode";

            // ------------- Reports--------
            routes.MapRoute(
            name: "Report",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "Report", action = "Index", id = UrlParameter.Optional },
            namespaces: new string[] { "Meridukan_WEBUI.Controllers" }
            ).DataTokens["Area"] = "Report";

        }
    }
}
