﻿using Meridukan_WEBUI.Models;
using Meridukan_WEBUI.Models.Account;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Meridukan_WEBUI.Controllers.Account
{
    public class AccountController : BaseController
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        // GET: Account
        [HttpGet]
        public ActionResult Login()
        {
            ViewBag.Ip = "";
            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            ViewBag.Message = "";
            return View();
        }

        [HttpPost, ValidateInput(false)]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel log, string returnUrl)
        {

           // return RedirectToAction("Index", "Home", new { userId = 1, userName = log.UserName });

            try
            {
                string userName = log.UserName;
                string CacheKeyNameUser = userName;
                ApplicationCache applicationCache = new ApplicationCache();
                applicationCache.RemoveMyCachedItem(CacheKeyNameUser);
                AppUser = null;

                if (ModelState.IsValid)
                {
                    HttpResponseMessage response = httpClient.PostAsync("api/Login/GetLogin", log, new JsonMediaTypeFormatter()).Result; //Blocking call
                    if (response.IsSuccessStatusCode)
                    {
                        var user = response.Content.ReadAsAsync<UserModel>().Result;
                        if (user != null)
                        {
                            System.Web.HttpContext.Current.Session["SessionId"] = System.Web.HttpContext.Current.Session.SessionID;
                            // Keep User and User Roles in Cache
                            applicationCache = new ApplicationCache();
                            CacheKeyNameUser = user.UserName;
                            applicationCache.AddtoCache(CacheKeyNameUser, user, AppCachePriority.Default, 3600.00);
                            ViewBag.UserId = user.Id;
                            ViewBag.UserName = user.UserName;

                            return RedirectToAction("Index", "Home", new { userId = ViewBag.UserId, userName = ViewBag.UserName });

                        }
                        else
                            ModelState.AddModelError("", "Please enter the valid Credentials");
                    }
                    else
                    {
                        var user = response.Content.ReadAsAsync<UserModel>().Result;

                        string responseContent = await response.Content.ReadAsStringAsync();
                        ExceptionResponse exceptionResponse = JsonConvert.DeserializeObject<ExceptionResponse>(responseContent);

                        ModelState.AddModelError("", exceptionResponse.ExceptionMessage);
                    }
                }
            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);
            }

            return View(log);
        }

        public ActionResult LogOut()
        {
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            FormsAuthentication.SignOut();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddHours(-1));
            Response.Cache.SetNoServerCaching();
            Response.Cache.SetNoStore();
            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            System.Web.HttpContext.Current.Session.Clear();
           // string userName = System.Web.HttpContext.Current.Request["userName"];
            string CacheKeyNameUser = userName;
            ApplicationCache applicationCache = new ApplicationCache();
            applicationCache.RemoveMyCachedItem(CacheKeyNameUser);
            AppUser = null;
            return RedirectToAction("Login");
        }
    }
}