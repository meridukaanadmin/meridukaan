﻿using Meridukan_WEBUI.Models.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace Meridukan_WEBUI.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            string userId = (Request["userId"] == null) ? "" : Request["userId"].ToString();
            ViewBag.UserId = userId;
            string userName = (Request["userName"] == null) ? "" : Request["userName"].ToString();
            ViewBag.UserName = userName;

            ViewBag.ActivePage = "Dashboard";

            if (AppUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            else
            {
                DashboardModel dashboard = new DashboardModel();
                dashboard = GetDashboardDetails();
                return View("Index", dashboard);
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        private static DashboardModel GetDashboardDetails()
        {
            DashboardModel Model = new DashboardModel();

            try
            {
                HttpResponseMessage response = httpClient.GetAsync("api/Dashboard/GetDashboardDetails").Result;

                if (response.IsSuccessStatusCode)
                {
                    Model = response.Content.ReadAsAsync<DashboardModel>().Result;
                }
                else
                {
                    return Model;
                }
            }
            catch (Exception ex)
            {
                return Model;
                throw ex;
            }
            return Model;
        }
    }
}