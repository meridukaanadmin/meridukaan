﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.GRN;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class GRNService : IGRNService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        DbConnection conn = null;
        public GRNService()
        {
            conn = new DbConnection();
        }

        public object GetGRNList()
        {
            List<GRNEntities> List = new List<GRNEntities>();
            string baseadder = ConfigurationManager.AppSettings["WebUIUrl"];
            try
            {
                var list1 = db.GRNTBs.Where(d=>d.IsActive == true).ToList();


                var list = (from i in list1
                            join k in db.SupplierTBs on i.SupplierId equals k.Id

                            select new
                            {
                                i.GrnId,
                                i.GrnNo,
                                i.GrnDate,
                                i.PoId,
                                i.PoNo,
                                i.SupplierId,
                                SupplierName = k.FirstName + " " + k.LastName,
                                i.SupplierInvNo,
                                i.SupplierInvAmt,
                                i.SupplierInvDate,
                                i.City,
                                i.TransportId,
                                i.LR_No,
                                i.LR_Date,
                                i.No_of_Packages,
                                i.Total,
                                i.TaxAmount,
                                i.GrandTotal,
                                i.PendAmount,
                                i.RecAmount,
                                i.Discount


                            }).ToList();

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GRNEntities GetGRNDetails(int GrnId)
        {

            GRNEntities grn = new GRNEntities();
            grn.GRNDetails = new List<GRNDetailsEntities>();

            string baseadder = ConfigurationManager.AppSettings["WebUIUrl"];
            try
            {
                var list1 = db.GRNTBs.Where(c => c.GrnId == GrnId).FirstOrDefault();

                var list2 = db.GRNDetailsTBs.Join(db.ProductTBs, j => j.ProductId, p => p.Id, (j, p) => new { j = j, p = p }).Where(w => w.j.GrnId == GrnId).ToList();

                grn.GrnId = list1.GrnId;
                grn.UserId = Convert.ToInt32(list1.UserId);
                grn.GrnNo = list1.GrnNo;
                grn.PoId = Convert.ToInt32(list1.PoId);
                grn.PoNo = list1.PoNo;
                grn.PoDate = Convert.ToDateTime(list1.PoDate); 
                
                grn.SupplierId = Convert.ToInt32(list1.SupplierId);
                grn.SupplierInvNo = list1.SupplierInvNo;
                grn.SupplierInvAmt = Convert.ToDecimal(list1.SupplierInvAmt);
                grn.SupplierInvDate = Convert.ToDateTime(list1.SupplierInvDate);
                grn.LR_Date = Convert.ToDateTime(list1.LR_Date);
                grn.City = list1.City;
                grn.TransportId = Convert.ToInt32(list1.TransportId);
                grn.LR_No = list1.LR_No;
                grn.No_of_Packages = list1.No_of_Packages;
                grn.Total = Convert.ToDecimal(list1.Total);
                grn.TaxAmount = Convert.ToDecimal(list1.TaxAmount);
                grn.GrandTotal = Convert.ToDecimal(list1.GrandTotal);
                grn.PendAmount = Convert.ToDecimal(list1.PendAmount);
                grn.RecAmount = Convert.ToDecimal(list1.RecAmount);
                grn.Discount = Convert.ToDecimal(list1.Discount);


                foreach (var c in list2)
                {
                    GRNDetailsEntities attribute = new GRNDetailsEntities();
                    attribute.GrnDetId = c.j.GrnDetId;
                    attribute.GrnId = Convert.ToInt32(c.j.GrnId);
                    attribute.GrnNo = c.j.GrnNo;
                    attribute.ProductId = Convert.ToInt32(c.j.ProductId);
                    attribute.CategoryId = Convert.ToInt32(c.j.CategoryId);
                    attribute.SubcategoryId = Convert.ToInt32(c.j.SubcategoryId);
                    attribute.BrandId = Convert.ToInt32(c.j.BrandId);
                    attribute.SizeId = Convert.ToInt32(c.j.SizeId);
                    attribute.Size = Convert.ToString(db.SizeTBs.Where(d=>d.Id == c.j.SizeId).FirstOrDefault().Name);
                    attribute.ColorId = Convert.ToInt32(c.j.ColorId);
                    attribute.Color = Convert.ToString(db.ColorTBs.Where(d => d.Id == c.j.ColorId).FirstOrDefault().Name);
                    attribute.Unit = c.j.Unit;
                    attribute.PurchaseQty = c.j.PurchaseQty;
                    attribute.TaxId = Convert.ToInt32(c.j.TaxId);
                    attribute.TaxName = Convert.ToString(db.TaxTBs.Where(d => d.TaxId == c.j.TaxId).FirstOrDefault().Name) + "-" + Convert.ToString(db.TaxTBs.Where(d => d.TaxId == c.j.TaxId).FirstOrDefault().Perc);
                    attribute.RecieveQty = c.j.RecieveQty;                     
                    attribute.TaxAmount = Convert.ToDecimal(c.j.TaxAmount);
                    attribute.Total = Convert.ToDecimal(c.j.Total);
                    attribute.GrandAmount = Convert.ToDecimal(c.j.GrandAmount);

                    attribute.MRP = Convert.ToDecimal(c.j.MRP);
                    attribute.Discount = Convert.ToDecimal(c.j.Discount);
                    attribute.SaleRate = Convert.ToDecimal(c.j.SaleRate);
                     
                    attribute.TaxInclusive = Convert.ToInt32(c.j.TaxInclusive); 
                    grn.GRNDetails.Add(attribute);
                } 
                return grn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public GRNEntities GetGRNDetailsNew(int GrnId)
        {

            GRNEntities grn = new GRNEntities();
            grn.GRNDetails = new List<GRNDetailsEntities>();

            try
            {

                SqlConnection con = conn.OpenDbConnection();
                SqlCommand cmd = new SqlCommand("GetGRNDetail", con);
                cmd.Parameters.Add("@GrnId", SqlDbType.Int).Value = GrnId;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    grn.GrnId = Convert.ToInt32(dr["GrnId"]);
                    grn.GrnNo = Convert.ToString(dr["GrnNo"]);
                    grn.GrnDate = Convert.ToDateTime(dr["GrnDate"]);
                    grn.PoId = Convert.ToInt32(dr["PoId"]);
                    grn.PoDate = Convert.ToDateTime(dr["PoDate"]); 
                    grn.SupplierId = Convert.ToInt32(dr["SupplierId"]);
                    grn.TransportId = Convert.ToInt32(dr["TransportId"]);
                    grn.SupplierInvNo = Convert.ToString(dr["SupplierInvNo"]);
                    grn.SupplierInvAmt = Convert.ToDecimal(dr["SupplierInvAmt"]);
                    grn.SupplierInvDate = Convert.ToDateTime(dr["SupplierInvDate"]);                    
                    grn.City = Convert.ToString(dr["City"]);                    
                    grn.LR_No = Convert.ToString(dr["LR_No"]);
                    grn.LR_Date = Convert.ToDateTime(dr["LR_Date"]);                    
                    grn.No_of_Packages = Convert.ToString(dr["No_of_Packages"]);
                    grn.Total = Convert.ToDecimal(dr["Total"]);
                    grn.TaxAmount = Convert.ToDecimal(dr["TaxAmount"]);
                    grn.GrandTotal = Convert.ToDecimal(dr["GrandTotal"]);
                    grn.UserId = Convert.ToInt32(dr["UserId"]);
                }


                cmd = new SqlCommand("GetGRNItemDetail", con);
                cmd.Parameters.Add("@GrnId", SqlDbType.Int).Value = GrnId;
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    GRNDetailsEntities enitities = new GRNDetailsEntities();

                    enitities.GrnDetId = Convert.ToInt32(dr["GrnDetId"]);
                    enitities.GrnId = Convert.ToInt32(dr["GrnId"]);
                    enitities.GrnNo = Convert.ToString(dr["GrnNo"]);
                    enitities.CategoryId = Convert.ToInt32(dr["CategoryId"]);
                    enitities.CategoryName = Convert.ToString(dr["CategoryName"]);
                    enitities.SubcategoryId = Convert.ToInt32(dr["SubcategoryId"]);
                    enitities.SubCategoryName = Convert.ToString(dr["SubCategoryName"]);
                    enitities.BrandId = Convert.ToInt32(dr["BrandId"]);
                    enitities.BrandName = Convert.ToString(dr["BrandName"]);
                    enitities.ProductId = Convert.ToInt32(dr["ProductId"]);
                    enitities.ProductName = Convert.ToString(dr["ProductName"]);
                    enitities.Unit = Convert.ToString(dr["Unit"]);
                    enitities.SizeId = Convert.ToInt32(dr["SizeId"]);
                    enitities.Size = Convert.ToString(dr["Size"]);                    
                    enitities.Color = Convert.ToString(dr["Color"]);
                    if(enitities.Color != "")
                    {
                        enitities.ColorId = Convert.ToInt32(dr["ColorId"]);
                    }
                    else
                    {
                        enitities.ColorId = 0;
                    }

                    enitities.PurchaseQty = Convert.ToString(dr["PurchaseQty"]);
                    enitities.PurchaseRate = Convert.ToDecimal(dr["PurchaseRate"]);
                    enitities.RecieveQty = Convert.ToString(dr["RecieveQty"]);
                    enitities.MRP = Convert.ToDecimal(dr["MRP"]);
                    enitities.Discount = Convert.ToDecimal(dr["Discount"]);
                    enitities.SaleRate = Convert.ToDecimal(dr["SaleRate"]);
                    enitities.TaxId = Convert.ToInt32(dr["TaxId"]);
                    enitities.TaxName = Convert.ToString(dr["TaxName"]);
                    enitities.TaxAmount = Convert.ToDecimal(dr["TaxAmount"]);
                    enitities.Total = Convert.ToDecimal(dr["Total"]);
                    enitities.GrandAmount = Convert.ToDecimal(dr["GrandAmount"]);
                    grn.GRNDetails.Add(enitities);
                }


                con.Close();



                return grn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateGRN(GRNEntities grn)
        {

            try
            {
                GRNTB tB = new GRNTB();

                if (grn.GrnId > 0)
                {
                    tB.GrnId = grn.GrnId;
                    tB.GrnNo = grn.GrnNo;
                    tB.GrnDate = grn.GrnDate;
                    tB.PoId = grn.PoId;
                    tB.PoDate = grn.PoDate;
                    tB.PoNo = grn.PoNo;
                    tB.UserId = grn.UserId;
                    tB.SupplierId = grn.SupplierId;
                    tB.SupplierInvNo = grn.SupplierInvNo;
                    tB.SupplierInvAmt =  grn.SupplierInvAmt;
                    tB.SupplierInvDate = grn.SupplierInvDate;
                    tB.City = grn.City;
                    tB.TransportId = grn.TransportId;
                    tB.LR_No = grn.LR_No;
                    tB.LR_Date = grn.LR_Date;
                    tB.No_of_Packages = grn.No_of_Packages;
                    tB.Total = grn.Total;
                    tB.TaxAmount = grn.TaxAmount;
                    tB.GrandTotal = grn.GrandTotal;
                    tB.PendAmount = grn.PendAmount;
                    tB.RecAmount = grn.RecAmount;
                    tB.Discount = grn.Discount;

                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();

                    var Check = db.GRNDetailsTBs.Where(d => d.GrnId == grn.GrnId).ToList();
                    foreach (var item in Check)
                    {
                        db.GRNDetailsTBs.Remove(item);
                        db.SaveChanges();
                    }

                    GRNDetailsTB ProTB = new GRNDetailsTB();
                    foreach (var i in grn.GRNDetails)
                    {
                        ProTB.GrnDetId = i.GrnDetId;
                        ProTB.GrnId = tB.GrnId;
                        ProTB.GrnNo = tB.GrnNo;
                        ProTB.ProductId = i.ProductId;
                        ProTB.CategoryId = i.CategoryId;
                        ProTB.SubcategoryId = i.SubcategoryId;
                        ProTB.BrandId = i.BrandId;
                        ProTB.SizeId = i.SizeId;
                        ProTB.Unit = i.Unit;
                        ProTB.MRP = i.MRP;
                        ProTB.PurchaseQty = i.PurchaseQty;
                        ProTB.PurchaseRate = i.PurchaseRate;
                        ProTB.TaxId = i.TaxId;
                        ProTB.TaxAmount = i.TaxAmount;
                        ProTB.Total = i.Total;
                        ProTB.GrandAmount = i.GrandAmount;
                        ProTB.RecieveQty = i.RecieveQty;
                        ProTB.MRP = i.MRP;
                        ProTB.Discount = i.Discount;
                        ProTB.SaleRate = i.SaleRate;
                        ProTB.TaxInclusive = i.TaxInclusive;

                        db.GRNDetailsTBs.Add(ProTB);
                        db.SaveChanges();

                        BatchMgmtTB batch = new BatchMgmtTB();
                        var Checkbatch = db.BatchMgmtTBs.Where(d => d.GrnId == grn.GrnId && d.ProductId == i.ProductId).FirstOrDefault();
                        if(Checkbatch != null)
                        {
                            //Checkbatch.ReceiveQty = i.RecieveQty;
                            //batch.SaleId = Checkbatch.SaleId;
                            //batch.SaleNo = Checkbatch.SaleNo;
                            //batch.SaleQty = Checkbatch.SaleQty;

                            //if(Convert.ToInt32(Checkbatch.Stock) > 0)
                            //{
                            //    if(Convert.ToInt32(i.RecieveQty) <= Convert.ToInt32(Checkbatch.SaleQty))
                            //    {
                            //        int stk = Convert.ToInt32(Checkbatch.SaleQty)
                            //    }
                                
                            //}

                            //batch.Stock = Checkbatch.Stock;
                            //db.Entry(batch).State = EntityState.Modified;
                            //db.SaveChanges();
                        }
                        else
                        {
                            //save in batch table                           
                            batch.GrnId = grn.GrnId;
                            batch.GrnNo = grn.GrnNo;
                            batch.ProductId = i.ProductId;
                            batch.SizeId = i.SizeId;
                            batch.ReceiveQty = i.RecieveQty;
                            batch.SaleId = 0;
                            batch.SaleNo = "";
                            batch.SaleQty = "0";
                            batch.Stock = i.RecieveQty;
                            db.BatchMgmtTBs.Add(batch);
                            db.SaveChanges();
                        }
                    }                    
                }
                else
                {

                    tB.GrnId = grn.GrnId;

                    var prd = db.GRNTBs.OrderByDescending(d => d.GrnId).FirstOrDefault();
                    string uniqueId = "";

                    if (prd != null)
                    {
                        uniqueId = Convert.ToString(prd.GrnId);
                        int newNumber = Convert.ToInt32(uniqueId) + 1;
                        uniqueId = "GRN-" + newNumber.ToString("000000");
                    }
                    else
                    {
                        uniqueId = "000000";
                        int newNumber = Convert.ToInt32(uniqueId) + 1;
                        uniqueId = "GRN-" + newNumber.ToString("000000");
                    }

                    tB.GrnNo = uniqueId;
                    tB.GrnDate = grn.GrnDate;
                    tB.PoId = grn.PoId;
                    tB.PoDate = grn.PoDate;
                    tB.PoNo = grn.PoNo;
                    tB.UserId = grn.UserId;
                    tB.SupplierId = grn.SupplierId;
                    tB.SupplierInvNo = grn.SupplierInvNo;
                    tB.SupplierInvAmt = grn.SupplierInvAmt;
                    tB.SupplierInvDate = grn.SupplierInvDate;
                    tB.City = grn.City;
                    tB.TransportId = grn.TransportId;
                    tB.LR_No = grn.LR_No;
                    tB.LR_Date = grn.LR_Date;
                    tB.No_of_Packages = grn.No_of_Packages;
                    tB.Total = grn.Total;
                    tB.TaxAmount = grn.TaxAmount;
                    tB.GrandTotal = grn.GrandTotal;
                    tB.PendAmount = grn.PendAmount;
                    tB.RecAmount = grn.RecAmount;
                    tB.Discount = grn.Discount;
                     
                    db.GRNTBs.Add(tB);
                    db.SaveChanges();

                    GRNDetailsTB ProTB = new GRNDetailsTB();
                    foreach (var i in grn.GRNDetails)
                    {                        
                        ProTB.GrnId = tB.GrnId;
                        ProTB.GrnNo = tB.GrnNo;
                        ProTB.ProductId = i.ProductId;
                        ProTB.CategoryId = i.CategoryId;
                        ProTB.SubcategoryId = i.SubcategoryId;
                        ProTB.BrandId = i.BrandId;
                        ProTB.SizeId = i.SizeId;
                        ProTB.ColorId = i.ColorId;
                        ProTB.Unit = i.Unit;                        
                        ProTB.PurchaseQty = i.PurchaseQty;
                        ProTB.PurchaseRate = i.PurchaseRate;                        
                        ProTB.RecieveQty = i.RecieveQty;
                        ProTB.MRP = i.MRP;
                        ProTB.Discount = i.Discount;
                        ProTB.SaleRate = i.SaleRate;

                        if(i.Expiry != null)
                        {
                            ProTB.Expiry = Convert.ToDateTime(i.Expiry);
                        }
                        else
                        {
                            ProTB.Expiry = (DateTime?) null;
                        }
                        
                        ProTB.TaxId = i.TaxId;
                        ProTB.TaxAmount = i.TaxAmount;
                        ProTB.Total = i.Total;
                        ProTB.GrandAmount = i.GrandAmount;
                        ProTB.TaxInclusive = i.TaxInclusive;

                        db.GRNDetailsTBs.Add(ProTB);
                        db.SaveChanges();

                        Product_AttributeTB _AttributeTB = new Product_AttributeTB();
                        var attribute = db.Product_AttributeTB.Where(d => d.ProductId == i.ProductId && d.PrSizeId == i.SizeId).FirstOrDefault();
                        if (attribute != null)
                        {
                            int stk = Convert.ToInt32(attribute.StockQty) + Convert.ToInt32(i.RecieveQty);
                            attribute.StockQty = stk;

                            db.Entry(attribute).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        StockEntryTB STK = new StockEntryTB();
                        STK.ProductId = i.ProductId;
                        STK.SizeId = i.SizeId;
                        STK.EntryDate = DateTime.Now.AddDays(0);
                        var supplier = db.SupplierTBs.Where(d => d.Id == grn.SupplierId).FirstOrDefault();
                        STK.Reference = "GRN-" + supplier.FirstName + " " + supplier.LastName;
                        STK.ReferenceNo = grn.GrnNo;
                        STK.PurchaseQty = i.RecieveQty;
                        STK.SaleQty = "0";
                        db.StockEntryTBs.Add(STK);
                        db.SaveChanges();

                        //save in batch table
                        BatchMgmtTB batch = new BatchMgmtTB();

                       // var Checkbatch = db.BatchMgmtTBs.Where(d => d.ProductId == i.ProductId && d.SizeId == i.SizeId).LastOrDefault();                       

                        batch.GrnId = tB.GrnId;
                        batch.GrnNo = tB.GrnNo;
                        batch.ProductId = i.ProductId;
                        batch.SizeId = i.SizeId;
                        batch.ReceiveQty = i.RecieveQty;
                        batch.SaleId = 0;
                        batch.SaleNo = "";
                        batch.SaleQty = "0";
                        batch.Stock = i.RecieveQty;
                        //if (Checkbatch != null)
                        //{
                        //    int stk = Convert.ToInt32(Checkbatch.Stock) + Convert.ToInt32(i.RecieveQty);
                        //    batch.Stock = Convert.ToString(stk);
                        //}
                        //else
                        //{
                        //    batch.Stock = i.RecieveQty;
                        //}
                           
                        db.BatchMgmtTBs.Add(batch);
                        db.SaveChanges();

                        //save in transaction table
                        TransactionsTB transactions = new TransactionsTB();
                        transactions.Date = grn.GrnDate;
                        transactions.Type = "GRN";
                        transactions.Reference = grn.GrnNo;
                        transactions.ReferenceId = grn.GrnId;
                        transactions.SupplierId = grn.SupplierId;
                        transactions.Amount = grn.GrandTotal;
                        db.TransactionsTBs.Add(transactions);
                        db.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteGRNDetails(int Id)
        {
            try
            {
                GRNTB tB = new GRNTB();
                var Check = db.GRNTBs.Where(d => d.GrnId == Id).FirstOrDefault();

                if (Check.GrnId > 0)
                {
                    Check.IsActive = false;
                    db.Entry(Check).State = EntityState.Modified;
                    db.SaveChanges();                    
                }

                //var SubCheck = db.GRNDetailsTBs.Where(gr => gr.GrnId == Id).ToList();
                //foreach (var item in SubCheck)
                //{
                //    db.GRNDetailsTBs.Remove(item);
                //    db.SaveChanges();
                //}                
                //if (Check.GrnId > 0)
                //{
                //    db.GRNTBs.Remove(Check);
                //    db.SaveChanges();
                //}
                //else
                //{
                //    return false;
                //}

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

    }
}