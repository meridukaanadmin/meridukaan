﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Tax;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.ClassesTax
{
    public class TaxService : ITaxService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<TaxEntities> GetTaxList()
        {
            List<TaxEntities> TaxList = new List<TaxEntities>();
             
            try
            {
                var list = db.TaxTBs.OrderBy(d => d.Name).ToList();

                foreach (var item in list)
                {
                    TaxEntities Tax = new TaxEntities();
                    Tax.Id = item.TaxId;
                    Tax.Name = item.Name;
                    Tax.Perc= Convert.ToDecimal(item.Perc);
                    TaxList.Add(Tax);
                }

                return TaxList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         
        public TaxEntities GetTaxDetails(int TaxId)
        {
            try
            {
                TaxEntities Tax = new TaxEntities();

                var taxdata = db.TaxTBs.Where(d => d.TaxId == TaxId).FirstOrDefault();

                Tax.Id = taxdata.TaxId;
                Tax.Name = taxdata.Name;
                Tax.Perc = Convert.ToDecimal(taxdata.Perc);

                return Tax;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateTaxDetails(TaxEntities Tax)
        {

            try
            {
                TaxTB tB = new TaxTB();

                if (Tax.Id > 0)
                {

                    tB.TaxId = Tax.Id;
                    tB.Name = Tax.Name; 
                    tB.Perc = Tax.Perc;
                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    tB.TaxId = Tax.Id;
                    tB.Name = Tax.Name;
                    tB.Name = Tax.Name;
                    tB.Perc = Tax.Perc;
                    db.TaxTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteTaxDetails(int Id)
        {
            try
            {
                TaxTB tB = new TaxTB();
                var CheckTax = db.TaxTBs.Where(d => d.TaxId == Id).FirstOrDefault();
                if (CheckTax.TaxId > 0)
                {
                    db.TaxTBs.Remove(CheckTax);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

    }
}