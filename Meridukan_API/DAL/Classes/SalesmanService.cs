﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Salesman;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Meridukan_API.DAL.Classes
{
    public class SalesmanService : ISalesmanService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<SalesmanEnitities> GetSalesmanList()
        {
            List<SalesmanEnitities> salemanList = new List<SalesmanEnitities>();
            
            try
            {
                var list = db.SalesmanTBs.OrderBy(d => d.Name).ToList();

                foreach (var item in list)
                {
                    SalesmanEnitities salesman = new SalesmanEnitities();
                    salesman.Id = item.Id;
                    salesman.Name = item.Name;
                    salesman.SalesmanNo = item.SalesmanNo;
                    salesman.Contact = item.Contact;
                    salemanList.Add(salesman);
                }

                return salemanList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SalesmanEnitities GetSalesmanDetails(int Id)
        {
            try
            {
                SalesmanEnitities salesman = new SalesmanEnitities();

                var data = db.SalesmanTBs.Where(d => d.Id == Id).FirstOrDefault();

                salesman.Id = data.Id;
                salesman.Name = data.Name;
                salesman.SalesmanNo = data.SalesmanNo;
                salesman.Contact = data.Contact;

                return salesman;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateSalesmanDetails(SalesmanEnitities salesman)
        {
            try
            {
                SalesmanTB tB = new SalesmanTB();

                if (salesman.Id > 0)
                {
                    tB.Id = salesman.Id;
                    tB.Name = salesman.Name;
                    tB.SalesmanNo = salesman.SalesmanNo;
                    tB.Contact = salesman.Contact;                   
                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    tB.Id = salesman.Id;
                    tB.Name = salesman.Name;
                    tB.SalesmanNo = salesman.SalesmanNo;
                    tB.Contact = salesman.Contact;
                    db.SalesmanTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteSalesmanDetails(int Id)
        {
            try
            {
                SalesmanTB tB = new SalesmanTB();
                var Check = db.SalesmanTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (Check.Id > 0)
                {
                    db.SalesmanTBs.Remove(Check);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
    }
}