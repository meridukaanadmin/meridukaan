﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Category;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class CategoryService : ICategoryService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<CategoryEntities> GetCategory()
        {
            List<CategoryEntities> categoryList = new List<CategoryEntities>();

            try
            {
                var list = from c in db.CategoryTBs                           
                           select new
                           {
                               c.Id,
                               c.Name,
                               c.ImagePath,
                              // BrandId =b.Id,
                              // BrandName = b.Name
                           };

                foreach (var item in list)
                {
                    CategoryEntities cat = new CategoryEntities();
                    cat.Id = (long)item.Id;
                    cat.Name = item.Name;
                    cat.ImagePath = item.ImagePath;
                    //cat.BrandId = item.BrandId;
                    //cat.BrandName = item.BrandName;
                    categoryList.Add(cat);
                }

                return categoryList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CategoryEntities GetCategoryDetails(int CategoryId)
        {
            try
            {
                CategoryEntities Category = new CategoryEntities();

                var categorydata = db.CategoryTBs.Where(d => d.Id == CategoryId).FirstOrDefault();

                Category.Id = categorydata.Id;
                Category.Name = categorydata.Name;
                Category.ImagePath = categorydata.ImagePath;

                return Category;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateCategoryDetails(CategoryEntities Category)
        {
            try
            {
                CategoryTB tB = new CategoryTB();

                if (Category.Id > 0)
                {

                    tB.Id = (int) Category.Id;
                    tB.Name = Category.Name;
                    tB.ImagePath = Category.ImagePath;
                   // tB.BrandId = Category.BrandId;
                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    tB.Id = (int) Category.Id;
                    tB.Name = Category.Name;
                    tB.ImagePath = Category.ImagePath;
                   // tB.BrandId = Category.BrandId;
                    db.CategoryTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteCategoryDetails(long Id)
        {
            try
            {
                CategoryTB tB = new CategoryTB();
                var Checkcategory = db.CategoryTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (Checkcategory.Id > 0)
                {
                    db.CategoryTBs.Remove(Checkcategory);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

    }
}