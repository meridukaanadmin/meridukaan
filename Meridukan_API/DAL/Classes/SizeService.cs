﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Size;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class SizeService : ISizeService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<SizeEntities> GetSizeList()
        {
            List<SizeEntities> sizeList = new List<SizeEntities>();
            try
            {
                var list = db.SizeTBs.OrderBy(d => d.Name).ToList();

                foreach (var item in list)
                {
                    SizeEntities size = new SizeEntities();
                    size.Id = item.Id;
                    size.Name = item.Name;
                    sizeList.Add(size);
                }

                return sizeList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SizeEntities GetSizeDetails(int Id)
        {
            try
            {
                SizeEntities size = new SizeEntities();

                var data = db.SizeTBs.Where(d => d.Id == Id).FirstOrDefault();

                size.Id = data.Id;
                size.Name = data.Name;


                return size;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateSize(SizeEntities size)
        {
            try
            {
                SizeTB tB = new SizeTB();

                if (size.Id > 0)
                {

                    tB.Id = size.Id;
                    tB.Name = size.Name;                   
                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {   
                    tB.Name = size.Name;
                    db.SizeTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteSizeDetails(int Id)
        {
            try
            {
                SizeTB tB = new SizeTB();
                var CheckSize = db.SizeTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (CheckSize.Id > 0)
                {
                    db.SizeTBs.Remove(CheckSize);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
    }
}