﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Dashboard;
using Meridukan_API.Models;
using System;
using System.Linq;

namespace Meridukan_API.DAL.Classes
{
    public class DashboardService : IDashboardService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public DashboardEntities GetDashboardData()
        {
            DashboardEntities dashboard = new DashboardEntities();

            try
            {
                var stores = db.StoreTBs.Count();
                var suppliers = db.SupplierTBs.Count();
                var customers = db.CustomerTBs.Count();
                var products = db.ProductTBs.Count();

                var purchasedt = from d in db.PurchaseTBs
                               select d;

                decimal purchase = 0;
                foreach (var item in purchasedt)
                {
                    purchase += Convert.ToDecimal(item.GrandTotal);
                }

                var saledt = from d in db.SaleOrderTBs
                                 select d;

                decimal sale = 0;
                foreach (var item in saledt)
                {
                    sale += Convert.ToDecimal(item.GrandTotal);
                }

                dashboard.TotalStores = stores;
                dashboard.TotalSuppliers = suppliers;
                dashboard.TotalCustomers = customers;
                dashboard.TotalProducts = products;
                dashboard.TotalPurchase = purchase;
                dashboard.TotalSale = sale;

                return dashboard;
            }
            catch
            {
                return dashboard;
            }
        }
    }
}