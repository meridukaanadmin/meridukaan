﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Role;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class RoleService : IRoleService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<RoleEntities> GetRoleList()
        {
            List<RoleEntities> roleList = new List<RoleEntities>();

            try
            {
                var list = db.RoleTBs.OrderBy(d => d.Id).ToList();

                foreach (var item in list)
                {
                    RoleEntities role = new RoleEntities();
                    role.Id = item.Id;
                    role.Name = item.Name;
                    roleList.Add(role);
                }

                return roleList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public RoleEntities GetRoleDetails(int Id)
        {
            try
            {
                RoleEntities role = new RoleEntities();

                var data = db.RoleTBs.Where(d => d.Id == Id).FirstOrDefault();

                if(data != null)
                {
                    role.Id = data.Id;
                    role.Name = data.Name;
                }
                
                return role;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateRoleDetails(RoleEntities role)
        {
            try
            {
                RoleTB tB = new RoleTB();

                if (role.Id > 0)
                {
                    tB.Id = role.Id;
                    tB.Name = role.Name;
                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    tB.Id = role.Id;
                    tB.Name = role.Name;                   
                    db.RoleTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteRoleDetails(int Id)
        {
            try
            {
                RoleTB tB = new RoleTB();
                var Checkrole = db.RoleTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (Checkrole.Id > 0)
                {
                    db.RoleTBs.Remove(Checkrole);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
        
    }
}