﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Store;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Meridukan_API.DAL.Classes
{
    public class StoreService : IStoreService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<StoreEntities> GetStores()
        {
            List<StoreEntities> storeList = new List<StoreEntities>();
            
            try
            {
                var list = db.StoreTBs.OrderBy(d => d.Name).ToList();

                foreach (var item in list)
                {
                    StoreEntities store = new StoreEntities();
                    store.Id = item.Id;
                    store.Name = item.Name;
                    store.Address = item.Address;
                    store.Description = item.Description;
                    storeList.Add(store);
                }

                return storeList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public StoreEntities GetStoreDetails(int Id)
        {
            try
            {
                StoreEntities store = new StoreEntities();

                var storedata = db.StoreTBs.Where(d => d.Id == Id).FirstOrDefault();

                store.Id = storedata.Id;
                store.Name = storedata.Name;
                store.Address = storedata.Address;
                store.Description = storedata.Description;

                return store;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateStoreDetails(StoreEntities store)
        {
            try
            {
                StoreTB tB = new StoreTB();

                if (store.Id > 0)
                {
                    tB.Id = store.Id;
                    tB.Name = store.Name;
                    tB.Address = store.Address;
                    tB.Description = store.Description;
                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                   // tB.Id = store.Id;
                    tB.Name = store.Name;
                    tB.Address = store.Address;
                    tB.Description = store.Description;
                    db.StoreTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteStoreDetails(int Id)
        {
            try
            {
                StoreTB tB = new StoreTB();
                var Checkstore = db.StoreTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (Checkstore.Id > 0)
                {
                    db.StoreTBs.Remove(Checkstore);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
    }
}