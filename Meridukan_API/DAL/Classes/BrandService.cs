﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Brand;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class BrandService : IBrandService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<BrandEntities> GetBrandList()
        {
            List<BrandEntities> brandList = new List<BrandEntities>();
            string baseadder = ConfigurationManager.AppSettings["WebUIUrl"];
            try
            {
                var list = db.BrandTBs.OrderBy(d => d.Name).ToList();

                foreach (var item in list)
                {
                    BrandEntities brand = new BrandEntities();
                    brand.Id = item.Id;
                    brand.Name = item.Name;
                    brand.ImageName = item.ImageName;
                    brand.ImagePath = baseadder+ item.ImagePath;
                    brandList.Add(brand);
                }

                return brandList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BrandEntities GetBrandDetails(int brandId)
        {
            try
            {
                BrandEntities brand = new BrandEntities();

                var brnddata = db.BrandTBs.Where(d => d.Id == brandId).FirstOrDefault();

                brand.Id = brnddata.Id;
                brand.Name = brnddata.Name;
                brand.ImageName = brnddata.ImageName;
                brand.ImagePath = brnddata.ImagePath;

                return brand;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateBrandDetails(BrandEntities brand)
        {
            try
            {
                BrandTB tB = new BrandTB();

                if (brand.Id > 0)
                {

                    tB.Id = brand.Id;
                    tB.Name = brand.Name;
                    tB.ImageName = brand.ImageName;
                    tB.ImagePath = brand.ImagePath;
                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    tB.Id = brand.Id;
                    tB.Name = brand.Name;
                    tB.ImageName = brand.ImageName;
                    tB.ImagePath = brand.ImagePath;
                    db.BrandTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteBrandDetails(int Id)
        {
            try
            {
                BrandTB tB = new BrandTB();
                var Checkbrand = db.BrandTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (Checkbrand.Id > 0)
                {
                    db.BrandTBs.Remove(Checkbrand);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

    }
}