﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Report;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class ReportService : IReportService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<TransactionReportEntities> GetTransactions()
        {
            List<TransactionReportEntities> transactionsList = new List<TransactionReportEntities>();
            
            try
            {
                var list = db.TransactionsTBs.OrderByDescending(d => d.Date).ToList();

                foreach (var item in list)
                {
                    TransactionReportEntities transaction = new TransactionReportEntities();
                    transaction.TransId = item.TransId;
                    transaction.date = Convert.ToDateTime(item.Date);
                    transaction.Type = item.Type;
                    transaction.Reference = item.Reference;
                    if(item.SupplierId != null)
                    {
                        transaction.Supplier = db.SupplierTBs.Where(d => d.Id == item.SupplierId).Select(d => d.FirstName + " " + d.LastName).FirstOrDefault();
                    }

                    transaction.Customer = item.Customer;
                    transaction.Amount = Convert.ToDecimal(item.Amount);
                    transactionsList.Add(transaction);
                }

                return transactionsList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OrderReportEntities> OrderReportData(DateTime fromdate, DateTime todate)
        {
            throw new NotImplementedException();
        }
    }
}