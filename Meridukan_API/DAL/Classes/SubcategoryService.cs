﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Subcategory;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class SubcategoryService : ISubcategoryService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<SubcategoryEntities> GetSubcategoryList()
        {
            List<SubcategoryEntities> subcatList = new List<SubcategoryEntities>();

            try
            {
                var list = (from d in db.SubcategoryTBs
                           join c in db.CategoryTBs on d.CategoryId equals c.Id
                           select new
                           {
                               d.Id,
                               d.Name,
                               d.ImageName,
                               d.ImagePath,
                               CategoryId = c.Id,
                               CategoryName = c.Name
                           }).OrderBy(d=>d.Name).ToList();

                foreach (var item in list)
                {
                    SubcategoryEntities brand = new SubcategoryEntities();
                    brand.Id = item.Id;
                    brand.Name = item.Name;
                    brand.ImageName = item.ImageName;
                    brand.ImagePath = item.ImagePath;
                    brand.CategoryId = item.CategoryId;
                    brand.CategoryName = item.CategoryName;
                    subcatList.Add(brand);
                }

                return subcatList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SubcategoryEntities GetSubcategoryDetails(int Id)
        {
            try
            {
                SubcategoryEntities subcategory = new SubcategoryEntities();

                var subcat = (from d in db.SubcategoryTBs
                            join c in db.CategoryTBs on d.CategoryId equals c.Id
                            where d.Id == Id
                            select new
                            {
                                d.Id,
                                d.Name,
                                d.ImageName,
                                d.ImagePath,
                                CategoryId = c.Id,
                                CategoryName = c.Name
                            }).FirstOrDefault();

                subcategory.Id = subcat.Id;
                subcategory.Name = subcat.Name;
                subcategory.ImageName = subcat.ImageName;
                subcategory.ImagePath = subcat.ImagePath;
                subcategory.CategoryId = subcat.CategoryId;
                subcategory.CategoryName = subcat.CategoryName;

                return subcategory;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateSubcategoyDetails(SubcategoryEntities subcategory)
        {
            try
            {
                SubcategoryTB tB = new SubcategoryTB();

                if (subcategory.Id > 0)
                {

                    tB.Id = subcategory.Id;
                    tB.Name = subcategory.Name;
                    tB.ImageName = subcategory.ImageName;
                    tB.ImagePath = subcategory.ImagePath;
                    tB.CategoryId = subcategory.CategoryId;
                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    tB.Id = subcategory.Id;
                    tB.Name = subcategory.Name;
                    tB.ImageName = subcategory.ImageName;
                    tB.ImagePath = subcategory.ImagePath;
                    tB.CategoryId = subcategory.CategoryId;
                    db.SubcategoryTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteSubcategoryDetails(int Id)
        {
            try
            {
                SubcategoryTB tB = new SubcategoryTB();
                var Checksubcat = db.SubcategoryTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (Checksubcat.Id > 0)
                {
                    db.SubcategoryTBs.Remove(Checksubcat);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<SubcategoryEntities> GetSubcategoryByCategoryId(int CategoryId)
        {
            List<SubcategoryEntities> subcatList = new List<SubcategoryEntities>();

            try
            {
                var list = (from d in db.SubcategoryTBs
                            join c in db.CategoryTBs on d.CategoryId equals c.Id
                            where c.Id == CategoryId
                            select new
                            {
                                d.Id,
                                d.Name,
                                d.ImageName,
                                d.ImagePath,
                                CategoryId = c.Id,
                                CategoryName = c.Name
                            }).OrderBy(d => d.Name).ToList();

                foreach (var item in list)
                {
                    SubcategoryEntities brand = new SubcategoryEntities();
                    brand.Id = item.Id;
                    brand.Name = item.Name;
                    brand.ImageName = item.ImageName;
                    brand.ImagePath = item.ImagePath;
                    brand.CategoryId = item.CategoryId;
                    brand.CategoryName = item.CategoryName;
                    subcatList.Add(brand);
                }

                return subcatList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}