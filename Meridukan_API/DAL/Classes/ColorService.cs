﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Color;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class ColorService : IColorService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public bool AddorUpdateColorDetails(ColorEntities color)
        {
            try
            {
                ColorTB tB = new ColorTB();

                if (color.Id > 0)
                {

                    tB.Id = color.Id;
                    tB.Name = color.Name;
                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    tB.Name = color.Name;
                    db.ColorTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteColorDetails(int Id)
        {
            try
            {
                ColorTB tB = new ColorTB();
                var Checkcolor = db.ColorTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (Checkcolor.Id > 0)
                {
                    db.ColorTBs.Remove(Checkcolor);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public ColorEntities GetColorDetails(int Id)
        {
            try
            {
                ColorEntities color = new ColorEntities();

                var data = db.ColorTBs.Where(d => d.Id == Id).FirstOrDefault();

                color.Id = data.Id;
                color.Name = data.Name;

                return color;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ColorEntities> GetColorList()
        {
            List<ColorEntities> colorList = new List<ColorEntities>();            
            try
            {
                var list = db.ColorTBs.OrderBy(d => d.Name).ToList();

                foreach (var item in list)
                {
                    ColorEntities color = new ColorEntities();
                    color.Id = item.Id;
                    color.Name = item.Name;
                    colorList.Add(color);
                }

                return colorList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}