﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Subcategory;
using Meridukan_API.Entities.SubstitudeCategory;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class SubstitudeCategoryService : ISubstitudeCategoryService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<SubstitudeCategoryEnitites> GetSubstitudecategoryList()
        {
            List<SubstitudeCategoryEnitites> subcatList = new List<SubstitudeCategoryEnitites>();

            try
            {
                var list = (from d in db.SubstitudeCategoryTBs
                            join c in db.SubcategoryTBs on d.SubcategoryId equals c.Id
                            select new
                            {
                                d.Id,
                                d.Name,
                                SubcategoryId = c.Id,
                                SubcategoryName = c.Name
                            }).OrderBy(d => d.Name).ToList();

                foreach (var item in list)
                {
                    SubstitudeCategoryEnitites substitude = new SubstitudeCategoryEnitites();
                    substitude.Id = item.Id;
                    substitude.Name = item.Name;
                    substitude.SubcategoryId = item.SubcategoryId;
                    substitude.SubcategoryName = item.SubcategoryName;
                    subcatList.Add(substitude);
                }

                return subcatList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SubstitudeCategoryEnitites GetSubstitudecategoryDetails(int Id)
        {
            SubstitudeCategoryEnitites subcategory = new SubstitudeCategoryEnitites();

            try
            {               

                var subcat = (from d in db.SubstitudeCategoryTBs
                              join c in db.SubcategoryTBs on d.SubcategoryId equals c.Id
                              where d.Id == Id
                              select new
                              {
                                  d.Id,
                                  d.Name,
                                  SubcategoryId = c.Id,
                                  SubcategoryName = c.Name
                              }).FirstOrDefault();

                subcategory.Id = subcat.Id;
                subcategory.Name = subcat.Name;
                subcategory.SubcategoryId = subcat.SubcategoryId;
                subcategory.SubcategoryName = subcat.SubcategoryName;


                return subcategory;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         public bool AddorUpdateSubstitudecategoyDetails(SubstitudeCategoryEnitites subcategory)
         {
            try
            {
                SubstitudeCategoryTB tB = new SubstitudeCategoryTB();

                if (subcategory.Id > 0)
                {
                    tB.Id = subcategory.Id;
                    tB.Name = subcategory.Name;                   
                    tB.SubcategoryId = subcategory.SubcategoryId;
                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    tB.Id = subcategory.Id;
                    tB.Name = subcategory.Name;
                    tB.SubcategoryId = subcategory.SubcategoryId;
                    db.SubstitudeCategoryTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }

         }

        public bool DeleteSubstitudecategoryDetails(int Id)
        {
            try
            {
                SubstitudeCategoryTB tB = new SubstitudeCategoryTB();
                var Checksubcat = db.SubstitudeCategoryTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (Checksubcat.Id > 0)
                {
                    db.SubstitudeCategoryTBs.Remove(Checksubcat);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

      

      
    }
}