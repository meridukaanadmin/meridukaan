﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Cart;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Meridukan_API.DAL.Classes
{
    public class CartService : ICartService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        DbConnection conn = null;
        public CartService()
        {
            conn = new DbConnection();
        }

        public List<CustomerCartDetailsEntities> GetCartDetailsByCustomer(string contact)
        {
            List<CustomerCartDetailsEntities> cartList = new List<CustomerCartDetailsEntities>();
            string baseadder = ConfigurationManager.AppSettings["WebUIUrl"];

            try
            {

                SqlConnection con = conn.OpenDbConnection();
                SqlCommand cmd = new SqlCommand("GetCustomerCartDetail", con);
                cmd.Parameters.Add("@Contact", SqlDbType.VarChar).Value = contact;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    CustomerCartDetailsEntities cart = new CustomerCartDetailsEntities();
                    cart.Contact = Convert.ToString(dr["Contact"]);
                    cart.Name = Convert.ToString(dr["CustName"]);
                    cart.CartDetId = Convert.ToInt32(dr["CartDetId"]);
                    cart.ProductId = Convert.ToInt32(dr["ProductId"]);
                    cart.ProductName = Convert.ToString(dr["ProductName"]);
                    cart.SizeId = Convert.ToInt32(dr["SizeId"]);

                    var prd = db.Product_AttributeTB.Where(x => x.ProductId == cart.ProductId && x.PrSizeId == cart.SizeId).FirstOrDefault();
                    cart.ImagePath = baseadder + prd.ImagePath;

                    cart.Rate = Convert.ToDecimal(dr["Rate"]);
                    cart.Qty = Convert.ToString(dr["Qty"]);
                    cart.Total = Convert.ToDecimal(dr["Total"]);
                    cart.TaxId = Convert.ToInt32(dr["TaxId"]);
                    cart.TaxName = Convert.ToString(dr["TaxName"]);
                    cart.TaxAmount = Convert.ToDecimal(dr["TaxAmount"]);
                    cart.GrandTotal = Convert.ToDecimal(dr["GrandTotal"]);

                    cart.FinalTotal += Convert.ToDecimal(dr["Total"]);
                    cart.FinalTaxAmount += Convert.ToDecimal(dr["TaxAmount"]);
                    cart.FinalGrandTotal += Convert.ToDecimal(dr["GrandTotal"]);
                    cartList.Add(cart);
                }

                con.Close();

                return cartList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveCartAtSuspend(CartEntities cart)
        {
            try
            {
                CartTB tB = new CartTB();

                var checkcart = db.CartTBs.Where(d => d.Contact == cart.Contact).FirstOrDefault();

                if (checkcart != null)
                {
                    checkcart.Total += cart.Total;
                    checkcart.TaxAmount += cart.TaxAmount;
                    checkcart.GrandTotal += cart.GrandTotal;
                    checkcart.Discount += cart.Discount;
                    db.Entry(checkcart).State = EntityState.Modified;
                    db.SaveChanges();

                    CartDetailsTB cartDetails = new CartDetailsTB();
                    foreach (var i in cart.cartDetails)
                    {
                        cartDetails.CartId = checkcart.CartId;
                        cartDetails.ProductId = i.ProductId;
                        cartDetails.SizeId = i.SizeId;
                        cartDetails.Rate = i.Rate;
                        cartDetails.Qty = i.Qty;
                        cartDetails.Total = i.Total;
                        cartDetails.TaxId = i.TaxId;
                        cartDetails.TaxAmount = i.TaxAmount;
                        cartDetails.GrandTotal = i.GrandTotal;
                        cartDetails.Discount = i.Discount;
                        db.CartDetailsTBs.Add(cartDetails);
                        db.SaveChanges();
                    }
                }
                else
                {
                    var prd = db.CartTBs.OrderByDescending(d => d.CartId).FirstOrDefault();
                    string uniqueId = "";

                    if (prd != null)
                    {
                        uniqueId = Convert.ToString(prd.CartId);
                        int newNumber = Convert.ToInt32(uniqueId) + 1;
                        uniqueId = newNumber.ToString("0000");
                    }
                    else
                    {
                        uniqueId = "0000";
                        int newNumber = Convert.ToInt32(uniqueId) + 1;
                        uniqueId = newNumber.ToString("0000");
                    }

                    tB.CartNo = uniqueId;
                    tB.CartDate = DateTime.Now.AddDays(0);
                    tB.Contact = cart.Contact;
                    tB.Total = cart.Total;
                    tB.TaxAmount = cart.TaxAmount;
                    tB.GrandTotal = cart.GrandTotal;
                    tB.Discount = cart.Discount;
                    db.CartTBs.Add(tB);
                    db.SaveChanges();

                    CartDetailsTB cartDetails = new CartDetailsTB();
                    foreach (var i in cart.cartDetails)
                    {
                        cartDetails.CartId = tB.CartId;
                        cartDetails.ProductId = i.ProductId;
                        cartDetails.SizeId = i.SizeId;
                        cartDetails.Rate = i.Rate;
                        cartDetails.Qty = i.Qty;
                        cartDetails.Total = i.Total;
                        cartDetails.TaxId = i.TaxId;
                        cartDetails.TaxAmount = i.TaxAmount;
                        cartDetails.GrandTotal = i.GrandTotal;
                        cartDetails.Discount = i.Discount;
                        db.CartDetailsTBs.Add(cartDetails);
                        db.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteCartByCustomer(string Contact)
        {
            try
            {
                CartTB tB = new CartTB();

                var CheckCart = db.CartTBs.Where(d => d.Contact == Contact).ToList();

                if (CheckCart.Count() > 0)
                {
                    foreach (var item in CheckCart)
                    {
                        var Checkitems = db.CartDetailsTBs.Where(d => d.CartId == item.CartId).ToList();

                        foreach (var items in Checkitems)
                        {
                            db.CartDetailsTBs.Remove(items);
                            db.SaveChanges();
                        }

                        db.CartTBs.Remove(item);
                        db.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool SaveCustomerCart(CartEntities cart)
        {
            try
            {
                CartTB tB = new CartTB();

                var checkcart = db.CartTBs.Where(d => d.Contact == cart.Contact).FirstOrDefault();

                if (checkcart != null)
                {
                    checkcart.Total += cart.Total;
                    checkcart.TaxAmount += cart.TaxAmount;
                    checkcart.GrandTotal += cart.GrandTotal;
                    checkcart.Discount += cart.Discount;
                    db.Entry(checkcart).State = EntityState.Modified;
                    db.SaveChanges();

                    CartDetailsTB cartDetails = new CartDetailsTB();
                    foreach (var i in cart.cartDetails)
                    {
                        cartDetails.CartId = checkcart.CartId;
                        cartDetails.ProductId = i.ProductId;
                        cartDetails.SizeId = i.SizeId;
                        cartDetails.Rate = i.Rate;
                        cartDetails.Qty = i.Qty;
                        cartDetails.Total = i.Total;
                        cartDetails.TaxId = i.TaxId;
                        cartDetails.TaxAmount = i.TaxAmount;
                        cartDetails.GrandTotal = i.GrandTotal;
                        cartDetails.Discount = i.Discount;
                        db.CartDetailsTBs.Add(cartDetails);
                        db.SaveChanges();
                    }
                }
                else
                {
                    var prd = db.CartTBs.OrderByDescending(d => d.CartId).FirstOrDefault();
                    string uniqueId = "";

                    if (prd != null)
                    {
                        uniqueId = Convert.ToString(prd.CartId);
                        int newNumber = Convert.ToInt32(uniqueId) + 1;
                        uniqueId = newNumber.ToString("0000");
                    }
                    else
                    {
                        uniqueId = "0000";
                        int newNumber = Convert.ToInt32(uniqueId) + 1;
                        uniqueId = newNumber.ToString("0000");
                    }

                    tB.CartNo = uniqueId;
                    tB.CartDate = DateTime.Now.AddDays(0);
                    tB.Contact = cart.Contact;
                    tB.Total = cart.Total;
                    tB.TaxAmount = cart.TaxAmount;
                    tB.GrandTotal = cart.GrandTotal;
                    tB.Discount = cart.Discount;
                    db.CartTBs.Add(tB);
                    db.SaveChanges();

                    CartDetailsTB cartDetails = new CartDetailsTB();
                    foreach (var i in cart.cartDetails)
                    {
                        cartDetails.CartId = tB.CartId;
                        cartDetails.ProductId = i.ProductId;
                        cartDetails.SizeId = i.SizeId;
                        cartDetails.Rate = i.Rate;
                        cartDetails.Qty = i.Qty;
                        cartDetails.Total = i.Total;
                        cartDetails.TaxId = i.TaxId;
                        cartDetails.TaxAmount = i.TaxAmount;
                        cartDetails.GrandTotal = i.GrandTotal;
                        cartDetails.Discount = i.Discount;
                        db.CartDetailsTBs.Add(cartDetails);
                        db.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteCustomerCartItems(DeleteCustomerCartItemsEntities cart)
        {
            try
            {
                bool res = false;

                CartTB tB = new CartTB();

                var checkcartdetails = db.CartDetailsTBs.Where(d => d.CartDetId == cart.CartDetId).FirstOrDefault();

                if (checkcartdetails != null)
                {
                    var checkcart = db.CartTBs.Where(d => d.CartId == checkcartdetails.CartId).FirstOrDefault();

                    if (checkcart != null)
                    {
                        checkcart.Total -= checkcart.Total;
                        checkcart.TaxAmount -= checkcart.TaxAmount;
                        checkcart.GrandTotal -= checkcart.GrandTotal;
                        checkcart.Discount -= checkcart.Discount;
                        db.Entry(checkcart).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    res = true;
                }
                else
                {
                    res = false;
                }
                
                return res;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
    }
}