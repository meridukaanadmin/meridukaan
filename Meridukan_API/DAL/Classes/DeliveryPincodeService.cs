﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.DeliveryPinCode;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class DeliveryPincodeService : IDeliveryPincodeService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<DeliveryPinCodeEntities> GetDeliveryPinCodeEntities()
        {
            List<DeliveryPinCodeEntities> List = new List<DeliveryPinCodeEntities>();            
            try
            {
                var list = db.DeliveryPostalCodeTBs.OrderBy(d => d.AreaName).ToList();

                foreach (var item in list)
                {
                    DeliveryPinCodeEntities pincodes = new DeliveryPinCodeEntities();
                    pincodes.Id = item.Id;
                    pincodes.PostalCode = item.PostalCode;
                    pincodes.Region = item.Region;
                    pincodes.AreaName = item.AreaName;
                    List.Add(pincodes);
                }

                return List;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DeliveryPinCodeEntities GetDeliveryPinCodeDetails(int Id)
        {
            try
            {
                DeliveryPinCodeEntities delivery = new DeliveryPinCodeEntities();

                var data = db.DeliveryPostalCodeTBs.Where(d => d.Id == Id).FirstOrDefault();

                delivery.Id = data.Id;
                delivery.PostalCode = data.PostalCode;
                delivery.Region = data.Region;
                delivery.AreaName = data.AreaName;

                return delivery;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateDeliveryPinCodeDetails(DeliveryPinCodeEntities delivery)
        {
            try
            {
                DeliveryPostalCodeTB tB = new DeliveryPostalCodeTB();

                if (delivery.Id > 0)
                {

                    tB.Id = delivery.Id;
                    tB.PostalCode = delivery.PostalCode;
                    tB.AreaName = delivery.AreaName;
                    tB.Region = delivery.Region;
                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    tB.PostalCode = delivery.PostalCode;
                    tB.AreaName = delivery.AreaName;
                    tB.Region = delivery.Region;
                    db.DeliveryPostalCodeTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteDeliveryPinCodeDetails(int Id)
        {
            try
            {
                DeliveryPostalCodeTB tB = new DeliveryPostalCodeTB();
                var Check = db.DeliveryPostalCodeTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (Check.Id > 0)
                {
                    db.DeliveryPostalCodeTBs.Remove(Check);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
    }
}