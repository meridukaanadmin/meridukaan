﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.OrderStatus;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class OrderStatusService : IOrderStatusService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<OrderStatusEntities> getOrderStatusList()
        {
            List<OrderStatusEntities> List = new List<OrderStatusEntities>();            
            try
            {
                var list = db.OrderStatusTBs.OrderBy(d => d.Name).ToList();

                foreach (var item in list)
                {
                    OrderStatusEntities orderStatus = new OrderStatusEntities();
                    orderStatus.Id = item.Id;
                    orderStatus.Name = item.Name;
                    List.Add(orderStatus);
                }

                return List;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}