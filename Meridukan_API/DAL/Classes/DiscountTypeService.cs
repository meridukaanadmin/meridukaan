﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Coupon;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class DiscountTypeService : IDiscountTypeService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public bool AddorUpdateDiscountType(DiscountType discount)
        {
            try
            {
                DiscountTypeTB tB = new DiscountTypeTB();

                if (discount.Id > 0)
                {

                    tB.Id = discount.Id;
                    tB.DiscountType = discount.Name;                   
                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    tB.DiscountType = discount.Name;                    
                    db.DiscountTypeTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteDiscountTypeDetails(int Id)
        {
            try
            {
                DiscountTypeTB tB = new DiscountTypeTB();
                var Check = db.DiscountTypeTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (Check.Id > 0)
                {
                    db.DiscountTypeTBs.Remove(Check);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public DiscountType GetDiscountTypeDetails(int Id)
        {
            try
            {
                DiscountType discount = new DiscountType();

                var data = db.DiscountTypeTBs.Where(d => d.Id == Id).FirstOrDefault();

                discount.Id = data.Id;
                discount.Name = data.DiscountType;
                
                return discount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DiscountType> GetDiscountTypeList()
        {
            List<DiscountType> discountList = new List<DiscountType>();
            try
            {
                var list = db.DiscountTypeTBs.OrderByDescending(d => d.Id).ToList();

                foreach (var item in list)
                {
                    DiscountType discount = new DiscountType();
                    discount.Id = item.Id;
                    discount.Name = item.DiscountType;
                    discountList.Add(discount);
                }

                return discountList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}