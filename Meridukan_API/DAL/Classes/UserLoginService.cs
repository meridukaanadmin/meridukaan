﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities;
using Meridukan_API.Entities.User;
using Meridukan_API.Models;

namespace Meridukan_API.DAL.Classes
{
    public class UserLoginServices : IUserLoginService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        DbConnection conn = null;
        public UserLoginServices()
        {
            conn = new DbConnection();
        }

        public UserLoginEntities GetLogin(LoginEntity Login)
        {
            UserLoginEntities data = new UserLoginEntities();
            var UserData= db.UserTBs.Where(c => c.UserName == Login.Username && c.Password == Login.Password).FirstOrDefault();

            if (UserData != null)
            {
                data.Id = UserData.Id;
                data.Name = UserData.FirstName + " " + UserData.MiddleName + " " + UserData.LastName;
                data.FirstName = UserData.FirstName;
                data.MiddleName = UserData.MiddleName;
                data.LastName = UserData.LastName;
                data.Phone = UserData.Phone;
                data.Email = UserData.Email;
                data.BirthDate = Convert.ToDateTime(UserData.BirthDate);
                data.Username = UserData.UserName;
                data.Password = UserData.Password;
                data.Address = UserData.Address;
                data.StoreId = (int) UserData.StoreId; 
                data.GSTNumber = UserData.GstNumber;
                data.LastLoginDate = Convert.ToDateTime(UserData.LastLoginDate);
            }
            else
            {
                throw new EntityNotFoundException("User", "Invalid UserName or Password", new Exception());
            }
            return data;

        }

        public bool addorUpdateUser(UserLoginEntities User)
        {
             
            try
            {
                UserTB ut = new UserTB();

                if (User.Id == 0)
                {
                    ut.FirstName = User.FirstName;
                    ut.MiddleName = User.MiddleName;
                    ut.LastName = User.LastName;
                    ut.BirthDate = User.BirthDate;
                    ut.Phone = User.Phone;
                    ut.Email = User.Email;
                    ut.UserName = User.Username;
                    ut.Password = User.Password;
                    ut.Address = User.Address;
                    ut.GstNumber = User.GSTNumber;
                    ut.StoreId = User.StoreId;
                    ut.LastLoginDate = DateTime.Now;

                    db.UserTBs.Add(ut);
                    db.SaveChanges();

                    foreach (var item in User.Roles)
                    {
                        AccessRoleTB tB = new AccessRoleTB();
                        tB.RoleId = item.Id;
                        tB.UserId = ut.Id;
                        db.AccessRoleTBs.Add(tB);
                        db.SaveChanges();
                    }
                }
                else
                {
                    ut.Id = User.Id;
                    ut.FirstName = User.FirstName;
                    ut.MiddleName = User.MiddleName;
                    ut.LastName = User.LastName;
                    ut.BirthDate = User.BirthDate;
                    ut.Phone = User.Phone;
                    ut.Email = User.Email;
                    ut.UserName = User.Username;
                    ut.Password = User.Password;
                    ut.Address = User.Address;
                    ut.GstNumber = User.GSTNumber;
                    ut.StoreId = User.StoreId;
                    ut.LastLoginDate = DateTime.Now;

                    db.Entry(ut).State = EntityState.Modified;
                    db.SaveChanges();


                    var Checkrole = db.AccessRoleTBs.Where(d => d.UserId == User.Id).ToList();
                    foreach (var item in Checkrole)
                    {
                        db.AccessRoleTBs.Remove(item);
                        db.SaveChanges();
                    }

                    foreach (var item in User.Roles)
                    {
                        AccessRoleTB tB = new AccessRoleTB();
                        tB.RoleId = item.Id;
                        tB.UserId = ut.Id;
                        db.AccessRoleTBs.Add(tB);
                        db.SaveChanges();
                    }
                } 
                return true;

            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            } 
        }

        public List<UserLoginEntities> GetUserList()
        {
            List<UserLoginEntities> userList = new List<UserLoginEntities>();

            try
            {

                SqlConnection con = conn.OpenDbConnection();
                SqlCommand cmd = new SqlCommand("GetUserList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    UserLoginEntities user = new UserLoginEntities();
                    user.Id = Convert.ToInt32(dr["Id"]);
                    user.Name = Convert.ToString(dr["FirstName"]) + " " + Convert.ToString(dr["MiddleName"]) + " " + Convert.ToString(dr["LastName"]);
                    user.FirstName = Convert.ToString(dr["FirstName"]);
                    user.MiddleName = Convert.ToString(dr["MiddleName"]);
                    user.LastName = Convert.ToString(dr["LastName"]);
                    user.Phone = Convert.ToString(dr["Phone"]);
                    user.Email = Convert.ToString(dr["Email"]);
                    user.BirthDate = Convert.ToDateTime(dr["BirthDate"]);
                    user.Gender = Convert.ToString(dr["Gender"]);
                    user.Username = Convert.ToString(dr["UserName"]);
                    user.Password = Convert.ToString(dr["Password"]);
                    user.Address = Convert.ToString(dr["LastName"]);
                    user.GSTNumber = Convert.ToString(dr["GstNumber"]);
                    user.LastLoginDate = Convert.ToDateTime(dr["LastLoginDate"]);
                    user.StoreId = Convert.ToInt32(dr["StoreId"]);
                    user.StoreName = Convert.ToString(dr["StoreName"]);
                    userList.Add(user);
                }

                con.Close();

                //var DataResult = (from d in db.UserTBs
                //                  join s in db.StoreTBs on d.StoreId equals s.Id
                //                  select new
                //                  {
                //                      d.Id,
                //                      Name = d.FirstName + ' ' + d.MiddleName + ' ' + d.LastName,
                //                      d.FirstName,
                //                      d.MiddleName,
                //                      d.LastName,
                //                      d.Email,
                //                      d.Phone,
                //                      d.BirthDate,
                //                      d.Gender,
                //                      d.UserName,
                //                      d.Password,
                //                      d.Address,
                //                      d.GstNumber,
                //                      d.LastLoginDate,
                //                      //d.StoreId,
                //                      storeId = s.Id,
                //                      storeName = s.Name

                //                  }).ToList();



                //var data = (from d in db.UserTBs
                //                // join s in db.StoreTBs on d.StoreId equals s.Id                           
                //            select new
                //            {
                //                d.Id,
                //                Name = d.FirstName + ' ' + d.MiddleName + ' ' + d.LastName,
                //                d.FirstName,
                //                d.MiddleName,
                //                d.LastName,
                //                d.Email,
                //                d.Phone,
                //                d.BirthDate,
                //                d.Gender,
                //                d.UserName,
                //                d.Password,
                //                d.Address,
                //                d.GstNumber,
                //                d.LastLoginDate,
                //                d.StoreId,
                //                // storeId = s.Id,
                //                // storeName = s.Name
                //            });

                //foreach (var item in DataResult)
                //{
                //    UserLoginEntities user = new UserLoginEntities();
                //    user.Id = item.Id;
                //    user.Name = item.FirstName + ' ' + item.LastName;
                //    user.FirstName = item.FirstName;
                //    user.MiddleName = item.MiddleName;
                //    user.LastName = item.LastName;
                //    user.Phone = item.Phone;
                //    user.Email = item.Email;
                //    user.BirthDate = Convert.ToDateTime(item.BirthDate);
                //    user.Gender = item.Gender;
                //    user.Username = item.UserName;
                //    user.Password = item.Password;
                //    user.Address = item.Address;
                //    user.GSTNumber = item.GstNumber;
                //    user.LastLoginDate = Convert.ToDateTime(item.LastLoginDate);
                //    user.StoreId = item.storeId;
                //    user.StoreName = item.storeName;

                //    //var storedt = (from d in db.StoreTBs where d.Id == item.StoreId select new { d.Id, d.Name }).FirstOrDefault();
                //    //user.StoreId = storedt.Id;
                //    //user.StoreName = storedt.Name;
                //    userList.Add(user);
                //}

                return userList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserLoginEntities GetUserDetails(int UserId)
        {
            try
            {
                UserLoginEntities user = new UserLoginEntities();

                SqlConnection con = conn.OpenDbConnection();
                SqlCommand cmd = new SqlCommand("GetUserDetails", con);
                cmd.Parameters.Add("UserId", SqlDbType.Int).Value = UserId;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {   
                    user.Id = Convert.ToInt32(dr["Id"]);
                    user.Name = Convert.ToString(dr["FirstName"]) + " " + Convert.ToString(dr["MiddleName"]) + " " + Convert.ToString(dr["LastName"]);
                    user.FirstName = Convert.ToString(dr["FirstName"]);
                    user.MiddleName = Convert.ToString(dr["MiddleName"]);
                    user.LastName = Convert.ToString(dr["LastName"]);
                    user.Phone = Convert.ToString(dr["Phone"]);
                    user.Email = Convert.ToString(dr["Email"]);
                    user.BirthDate = Convert.ToDateTime(dr["BirthDate"]);
                    user.Gender = Convert.ToString(dr["Gender"]);
                    user.Username = Convert.ToString(dr["UserName"]);
                    user.Password = Convert.ToString(dr["Password"]);
                    user.Address = Convert.ToString(dr["LastName"]);
                    user.GSTNumber = Convert.ToString(dr["GstNumber"]);
                    user.LastLoginDate = Convert.ToDateTime(dr["LastLoginDate"]);
                    user.StoreId = Convert.ToInt32(dr["StoreId"]);
                    user.StoreName = Convert.ToString(dr["StoreName"]);                    
                }               

                

                var role = db.RoleTBs.ToList();

                user.Roles = new List<AccessRoleModel>();
               // var role = db.RoleTBs.ToList();

                foreach (var item in role)
                {
                    var roledt = db.AccessRoleTBs.Where(d => d.RoleId == item.Id).FirstOrDefault();

                    AccessRoleModel accessRole = new AccessRoleModel();                  

                    if(roledt != null)
                    {
                        if (item.Id == roledt.RoleId)
                        {
                            accessRole.IsSelected = true;
                        }
                        else
                        {
                            accessRole.IsSelected = false;
                        }
                    }
                    else
                    {
                        accessRole.IsSelected = false;
                    }                   

                    accessRole.Id = item.Id;
                    accessRole.Name = item.Name;

                    user.Roles.Add(accessRole);
                }

                con.Close();

                //var item = (from d in db.UserTBs
                //            join s in db.StoreTBs on d.StoreId equals s.Id
                //            where d.Id == UserId
                //            select new
                //            {
                //                d.Id,
                //                Name = d.FirstName + ' ' + d.MiddleName + ' ' + d.LastName,
                //                d.FirstName,
                //                d.MiddleName,
                //                d.LastName,
                //                d.Email,
                //                d.Phone,
                //                d.BirthDate,
                //                d.Gender,
                //                d.UserName,
                //                d.Password,
                //                d.Address,
                //                d.GstNumber,
                //                d.LastLoginDate,
                //                storeId = s.Id,
                //                storeName = s.Name
                //            }).FirstOrDefault();

                //user.Id = item.Id;
                //user.Name = item.Name;
                //user.FirstName = item.FirstName;
                //user.MiddleName = item.MiddleName;
                //user.LastName = item.LastName;
                //user.Phone = item.Phone;
                //user.Email = item.Email;
                //user.BirthDate = Convert.ToDateTime(item.BirthDate);
                //user.Gender = item.Gender;
                //user.Username = item.UserName;
                //user.Password = item.Password;
                //user.Address = item.Address;
                //user.GSTNumber = item.GstNumber;
                //user.LastLoginDate = Convert.ToDateTime(item.LastLoginDate);
                //user.StoreId = item.storeId;
                //user.StoreName = item.storeName;

                return user;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteUserDetails(int UserId)
        {
            try
            {
                UserTB tB = new UserTB();

                var Checkrole = db.AccessRoleTBs.Where(d => d.UserId == UserId).ToList();
              
                var CheckUser = db.UserTBs.Where(d => d.Id == UserId).FirstOrDefault();
                if (CheckUser.Id > 0)
                {
                    foreach (var item in Checkrole)
                    {
                        db.AccessRoleTBs.Remove(item);
                        db.SaveChanges();
                    }

                    db.UserTBs.Remove(CheckUser);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
    }
}