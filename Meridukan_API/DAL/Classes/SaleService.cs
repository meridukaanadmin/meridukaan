﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Sale;
using Meridukan_API.Entities.Size;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class SaleService : ISaleService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        DbConnection conn = null;
        public SaleService()
        {
            conn = new DbConnection();
        }

        public List<SaleEntities> GetSaleList()
        {
            List<SaleEntities> saleList = new List<SaleEntities>();

            try
            {

                SqlConnection con = conn.OpenDbConnection();
                SqlCommand cmd = new SqlCommand("GetSaleList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    SaleEntities sale = new SaleEntities();
                    sale.SaleId = Convert.ToInt32(dr["SaleId"]);
                    sale.SaleDate = Convert.ToDateTime(dr["SaleDate"]);
                    sale.SaleNo = Convert.ToString(dr["SaleNo"]);
                    sale.CustomerName = Convert.ToString(dr["CustomerName"]);
                    sale.SalesmanNo = Convert.ToString(dr["SalesmanNo"]);
                    if(sale.SalesmanNo != "")
                    {
                        sale.SalesmanId = Convert.ToInt32(dr["SalesmanId"]);
                    }
                    else
                    {
                        sale.SalesmanId = 0;
                    }
                    
                    sale.Total = Convert.ToDecimal(dr["Total"]);
                    sale.TaxAmount = Convert.ToDecimal(dr["TaxAmount"]);
                    sale.GrandTotal = Convert.ToDecimal(dr["GrandTotal"]);
                    sale.OrderStatusId = Convert.ToInt32(dr["OrderStatusId"]);

                    saleList.Add(sale);
                }

                con.Close();                

                return saleList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SaleEntities GetSaleDetails(int SaleId)
        {
            SaleEntities sale = new SaleEntities();
            sale.saleDetails = new List<SaleDetailEnitities>();

            try
            {

                SqlConnection con = conn.OpenDbConnection();
                SqlCommand cmd = new SqlCommand("GetSaleDetail", con);
                cmd.Parameters.Add("@SaleId", SqlDbType.Int).Value = SaleId;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    sale.SaleId = Convert.ToInt32(dr["SaleId"]);
                    sale.SaleDate = Convert.ToDateTime(dr["SaleDate"]);
                    sale.Date = Convert.ToString(dr["SaleDate"]);
                    sale.SaleNo = Convert.ToString(dr["SaleNo"]);                    
                    sale.CustomerName = Convert.ToString(dr["CustomerName"]);
                    sale.Contact = Convert.ToString(dr["Contact"]);                    
                    sale.SalesmanNo = Convert.ToString(dr["SalesmanNo"]);
                    if (sale.SalesmanNo != "")
                    {
                        sale.SalesmanId = Convert.ToInt32(dr["SalesmanId"]);
                    }
                    else
                    {
                        sale.SalesmanId = 0;
                    }
                    sale.Total = Convert.ToDecimal(dr["Total"]);
                    sale.TaxAmount = Convert.ToDecimal(dr["TaxAmount"]);
                    sale.GrandTotal = Convert.ToDecimal(dr["GrandTotal"]);
                    sale.UserId = Convert.ToInt32(dr["UserId"]);
                    sale.ReceiveAmount = Convert.ToDecimal(dr["ReceiveAmount"]);
                    sale.PendingAmount = Convert.ToDecimal(dr["PendingAmount"]);
                    sale.Change = Convert.ToDecimal(dr["Change"]);
                    sale.Cash = Convert.ToDecimal(dr["Cash"]);
                    sale.PaymentOption = Convert.ToString(dr["PaymentOption"]);
                }


                cmd = new SqlCommand("GetSaleItemDetail", con);
                cmd.Parameters.Add("@SaleId", SqlDbType.Int).Value = SaleId;
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                string baseadder = ConfigurationManager.AppSettings["WebUIUrl"];

                while (dr.Read())
                {
                    SaleDetailEnitities enitities = new SaleDetailEnitities();

                    enitities.SaleDetId = Convert.ToInt32(dr["SaleDetId"]);
                    enitities.SaleId = Convert.ToInt32(dr["SaleId"]);
                    enitities.SaleNo = Convert.ToString(dr["SaleNo"]);                   
                    enitities.ProductId = Convert.ToInt32(dr["ProductId"]);
                    enitities.ProductName = Convert.ToString(dr["ProductName"]);
                    enitities.Unit = Convert.ToString(dr["Unit"]);
                    enitities.SizeId = Convert.ToInt32(dr["SizeId"]);

                    var prd = db.Product_AttributeTB.Where(x => x.ProductId == enitities.ProductId && x.PrSizeId == enitities.SizeId).FirstOrDefault();
                    enitities.ImagePath = baseadder + prd.ImagePath;

                    enitities.SaleQty = Convert.ToString(dr["SaleQty"]);
                    enitities.SaleRate = Convert.ToDecimal(dr["SaleRate"]);
                    enitities.TaxId = Convert.ToInt32(dr["TaxId"]);                    
                    enitities.TaxName = Convert.ToString(dr["TaxName"]);
                    enitities.TaxAmount = Convert.ToDecimal(dr["TaxAmount"]);
                    enitities.Total = Convert.ToDecimal(dr["Total"]);
                    enitities.GrandTotal = Convert.ToDecimal(dr["GrandAmount"]);
                    sale.saleDetails.Add(enitities);
                }

                con.Close();

                return sale;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public long? AddorUpdateSale(SaleEntities sale)
        {
            try
            {
                SaleOrderTB ut = new SaleOrderTB();

                if (sale.SaleId == 0)
                {
                    var prd = db.SaleOrderTBs.OrderByDescending(d => d.SaleId).FirstOrDefault();
                    string uniqueId = "";

                    if (prd != null)
                    {
                        uniqueId =Convert.ToString(prd.SaleId);
                        int newNumber = Convert.ToInt32(uniqueId) + 1;
                        uniqueId = "SO-" + newNumber.ToString("000000");
                    }
                    else
                    {
                        uniqueId = "000000";
                        int newNumber = Convert.ToInt32(uniqueId) + 1;
                        uniqueId = "SO-" + newNumber.ToString("000000");
                    }
                    
                    ut.SaleNo = uniqueId;
                    ut.SaleDate = sale.SaleDate;                    
                    ut.CustomerName = sale.CustomerName;
                    ut.Contact = sale.Contact;
                    ut.SalesmanId = sale.SalesmanId;
                    ut.SalesmanNo = sale.SalesmanNo;
                    ut.Total = sale.Total;
                    ut.TaxAmount = sale.TaxAmount;
                    ut.Discount = sale.Discount;
                    ut.GrandTotal = sale.GrandTotal;
                    ut.ReceiveAmount = sale.ReceiveAmount;
                    ut.PendingAmount = sale.PendingAmount;
                    ut.Cash = sale.Cash;
                    ut.Change = sale.Change;
                    ut.PaymentOption = sale.PaymentOption;
                    ut.UserId = sale.UserId;

                    var status = db.OrderStatusTBs.Where(d => d.Name == "New").FirstOrDefault();
                    ut.OrderStatusId = status.Id;

                    db.SaleOrderTBs.Add(ut);
                    db.SaveChanges();

                    foreach (var item in sale.saleDetails)
                    {
                        SaleOrderDetailsTB tB = new SaleOrderDetailsTB();
                        tB.SaleId = ut.SaleId;
                        tB.SaleNo = ut.SaleNo;
                        tB.CategoryId = db.ProductTBs.Where(d => d.Id == item.ProductId).Select(c => c.CategoryId).FirstOrDefault();
                        tB.SubcategoryId = db.ProductTBs.Where(d => d.Id == item.ProductId).Select(c => c.SubCategoryId).FirstOrDefault();
                        tB.BrandId = db.ProductTBs.Where(d => d.Id == item.ProductId).Select(c => c.BrandId).FirstOrDefault();
                        tB.ProductId = item.ProductId;
                        tB.SizeId = item.SizeId;
                        tB.Unit = item.Unit;
                        tB.SaleQty = item.SaleQty;
                        tB.SaleRate = item.SaleRate;
                        tB.TaxId = item.TaxId;
                        tB.TaxAmount = item.TaxAmount;
                        tB.Total = item.Total;
                        tB.Discount = item.Discount;
                        tB.GrandAmount = item.GrandTotal;
                        db.SaleOrderDetailsTBs.Add(tB);
                        db.SaveChanges();
                       
                        var attribute = db.Product_AttributeTB.Where(d => d.ProductId == item.ProductId && d.PrSizeId == item.SizeId).FirstOrDefault();
                        if (attribute != null)
                        {
                            int stk = Convert.ToInt32(attribute.StockQty) - Convert.ToInt32(item.SaleQty);
                            attribute.StockQty = stk;

                            db.Entry(attribute).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        StockEntryTB STK = new StockEntryTB();
                        STK.ProductId = item.ProductId;
                        STK.SizeId = item.SizeId;
                        STK.EntryDate = DateTime.Now;
                        STK.Reference = "SO-" + sale.CustomerName;
                        STK.ReferenceNo = ut.SaleNo;
                        STK.SaleQty = item.SaleQty;
                        STK.PurchaseQty = "0";
                        db.StockEntryTBs.Add(STK);
                        db.SaveChanges();

                        //update in batch mgmt table

                        var batchdt = db.BatchMgmtTBs.OrderBy(d => d.GrnId).ToList();

                        int stkdt = Convert.ToInt32(item.SaleQty);

                        BatchMgmtTB batch = new BatchMgmtTB();

                        foreach (var mgmt in batchdt)
                        { 
                            if(mgmt.ProductId == item.ProductId && mgmt.SizeId == item.SizeId)
                            {
                                var btch = db.BatchMgmtTBs.Where(d => d.Id == mgmt.Id).FirstOrDefault();

                                if (Convert.ToInt32(btch.Stock) > 0)
                                {
                                    if(Convert.ToInt32(item.SaleQty) <= Convert.ToInt32(btch.Stock))
                                    {   
                                        btch.SaleId = ut.SaleId;
                                        btch.SaleNo = ut.SaleNo;
                                        btch.SaleQty = item.SaleQty;
                                        int stk = Convert.ToInt32(btch.Stock) - Convert.ToInt32(item.SaleQty);
                                        btch.Stock = Convert.ToString(stk);
                                        db.Entry(btch).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    if(Convert.ToInt32(item.SaleQty) >= Convert.ToInt32(btch.Stock))
                                    {

                                    }

                                    break;
                                }
                            }
                        }
                    }

                    //save in transaction table
                    TransactionsTB transactions = new TransactionsTB();
                    transactions.Date = sale.SaleDate;
                    transactions.Type = "SO";
                    transactions.Reference = ut.SaleNo;
                    transactions.ReferenceId = ut.SaleId;
                    transactions.Customer = sale.CustomerName;
                    transactions.Amount = sale.GrandTotal;
                    db.TransactionsTBs.Add(transactions);
                    db.SaveChanges();

                    
                }
                else
                {
                    ut.SaleId = sale.SaleId;
                    ut.SaleNo = sale.SaleNo;
                    ut.SaleDate = sale.SaleDate;
                    ut.CustomerName = sale.CustomerName;
                    ut.Contact = sale.Contact;
                    ut.SalesmanId = sale.SalesmanId;
                    ut.SalesmanNo = sale.SalesmanNo;
                    ut.Total = sale.Total;
                    ut.TaxAmount = sale.TaxAmount;
                    ut.GrandTotal = sale.GrandTotal;
                    ut.UserId = sale.UserId;
                    ut.OrderStatusId = sale.OrderStatusId;
                    db.Entry(ut).State = EntityState.Modified;
                    db.SaveChanges();

                    var Check = db.SaleOrderDetailsTBs.Where(d => d.SaleId == ut.SaleId).ToList();
                    foreach (var item in Check)
                    {
                        db.SaleOrderDetailsTBs.Remove(item);
                        db.SaveChanges();
                    }

                    foreach (var item in sale.saleDetails)
                    {
                        SaleOrderDetailsTB tB = new SaleOrderDetailsTB();
                        tB.SaleId = ut.SaleId;
                        tB.SaleNo = ut.SaleNo;
                        tB.CategoryId = item.CategoryId;
                        tB.SubcategoryId = item.SubcategoryId;
                        tB.BrandId = item.BrandId;
                        tB.ProductId = item.ProductId;
                        tB.SizeId = item.SizeId;
                        tB.Unit = item.Unit;
                        tB.SaleQty = item.SaleQty;
                        tB.SaleRate = item.SaleRate;
                        tB.TaxId = item.TaxId;
                        tB.TaxAmount = item.TaxAmount;
                        tB.Total = item.Total;
                        tB.GrandAmount = item.GrandTotal;
                        db.SaleOrderDetailsTBs.Add(tB);
                        db.SaveChanges();
                    }
                }
                return ut.SaleId;

            }
            catch (Exception ex)
            {
                return 0;
                throw ex;
            }
        }

        public bool DeleteSaleDetails(int SaleId)
        {
            try
            {
                SaleOrderTB tB = new SaleOrderTB();

              //  var Checkitems = db.SaleOrderDetailsTBs.Where(d => d.SaleId == SaleId).ToList();

                

                var CheckSale = db.SaleOrderTBs.Where(d => d.SaleId == SaleId).FirstOrDefault();
                if (CheckSale.SaleId > 0)
                {
                    CheckSale.IsActive = false;
                    db.Entry(CheckSale).State = EntityState.Modified;
                    db.SaveChanges();
                    //foreach (var item in Checkitems)
                    //{
                    //    db.SaleOrderDetailsTBs.Remove(item);
                    //    db.SaveChanges();
                    //}

                    //db.SaleOrderTBs.Remove(CheckSale);
                    //db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }      

        public SaleRateEntities GetSaleRate(string Barcode)
        {
            SaleRateEntities saleRate = new SaleRateEntities();
            try
            {
                string baseadder = ConfigurationManager.AppSettings["WebUIUrl"];

                var attr = db.Product_AttributeTB.Where(d => d.Barcode == Barcode).FirstOrDefault();

                var lst = db.BatchMgmtTBs.OrderBy(d=>d.GrnId).ToList();

                var lstnew = db.BatchMgmtTBs.OrderBy(d => d.GrnId).Where(d => d.ProductId == attr.ProductId && d.SizeId == attr.PrSizeId && d.Stock != "0").ToList();

                if(lstnew.Count() == 0)
                {
                    var dt = lstnew.FirstOrDefault();

                    var grndt = db.GRNDetailsTBs.Where(d => d.GrnId == dt.GrnId && d.ProductId == attr.ProductId && d.SizeId == attr.PrSizeId).FirstOrDefault();

                    saleRate.SaleRate = Convert.ToDecimal(grndt.SaleRate);
                    saleRate.ProductId = Convert.ToInt32(attr.ProductId);
                    saleRate.ProductName = db.ProductTBs.Where(x => x.Id == attr.ProductId).Select(v => v.Name).FirstOrDefault();

                    var prd = db.Product_AttributeTB.Where(x => x.ProductId == attr.ProductId && x.PrSizeId == attr.PrSizeId).FirstOrDefault();
                    saleRate.ImagePath = baseadder + prd.ImagePath;
                    saleRate.TaxId = Convert.ToInt32(grndt.TaxId);
                    saleRate.ProductName = saleRate.ProductName + "(" + prd.Barcode + ")";
                    saleRate.Stock = Convert.ToString(prd.StockQty);

                    var taxdt = db.TaxTBs.Where(t => t.TaxId == grndt.TaxId).FirstOrDefault();
                    saleRate.TaxName = taxdt.Name + "-" + Convert.ToString(taxdt.Perc) + "%";
                    saleRate.TaxValue = Convert.ToDecimal(taxdt.Perc);
                    saleRate.SizeId = Convert.ToInt32(attr.PrSizeId);
                }
                else
                {
                    if (lst.Count > 0)
                    {
                        foreach (var item in lst)
                        {
                            if (item.Stock != null)
                            {
                                int stk = Convert.ToInt32(item.Stock);

                                if (stk > 0)
                                {
                                    var grndt = db.GRNDetailsTBs.Where(d => d.GrnId == item.GrnId && d.ProductId == attr.ProductId && d.SizeId == attr.PrSizeId).FirstOrDefault();
                                    if (grndt != null)
                                    {
                                        saleRate.SaleRate = Convert.ToDecimal(grndt.SaleRate);
                                        saleRate.ProductId = Convert.ToInt32(attr.ProductId);
                                        saleRate.ProductName = db.ProductTBs.Where(x => x.Id == attr.ProductId).Select(v => v.Name).FirstOrDefault();

                                        var prd = db.Product_AttributeTB.Where(x => x.ProductId == attr.ProductId && x.PrSizeId == attr.PrSizeId).FirstOrDefault();
                                        saleRate.ImagePath = baseadder + prd.ImagePath;
                                        saleRate.TaxId = Convert.ToInt32(grndt.TaxId);
                                        saleRate.ProductName = saleRate.ProductName + "(" + prd.Barcode + ")";
                                        saleRate.Stock = Convert.ToString(prd.StockQty);

                                        var taxdt = db.TaxTBs.Where(t => t.TaxId == grndt.TaxId).FirstOrDefault();
                                        saleRate.TaxName = taxdt.Name + "-" + Convert.ToString(taxdt.Perc) + "%";
                                        saleRate.TaxValue = Convert.ToDecimal(taxdt.Perc);
                                        saleRate.SizeId = Convert.ToInt32(attr.PrSizeId);

                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                return saleRate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateOrderStatus(int SaleId, int OrderstatusId)
        {
            try
            {
                SaleOrderTB tB = new SaleOrderTB();

                var Checkitems = db.SaleOrderTBs.Where(d => d.SaleId == SaleId).FirstOrDefault();

                if(Checkitems != null)
                {
                    Checkitems.OrderStatusId = OrderstatusId;
                    db.Entry(Checkitems).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<AutocompleteSearchEntities> GetProductSearchData()
        {
            List<AutocompleteSearchEntities> PrdList = new List<AutocompleteSearchEntities>();

            try
            {
                var list = db.Product_AttributeTB.OrderBy(d => d.Id).ToList();

                foreach (var item in list)
                {
                    AutocompleteSearchEntities prd = new AutocompleteSearchEntities();
                    prd.Id = item.Id;
                    prd.Name = item.Barcode;
                    PrdList.Add(prd);
                }

                return PrdList;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}