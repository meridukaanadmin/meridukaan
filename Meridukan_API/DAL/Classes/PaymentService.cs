﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Payment;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class PaymentService : IPaymentService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public bool AddPayment(PaymentEntities payment)
        {
            try
            {
                PaymentTB tB = new PaymentTB();
                tB.CustomerId = payment.CustomerId;
                tB.Contact = payment.Contact;
                tB.OrderId = payment.OrderId;
                tB.PaymentAmount = payment.PaymentAmount;
                tB.PaymentDate = payment.PaymentDate;
                db.PaymentTBs.Add(tB);
                db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public PaymentEntities GetPaymentByOrderId(int OrderId)
        {
            PaymentEntities Payment = new PaymentEntities();
            try
            {
                var item = db.PaymentTBs.Where(d => d.OrderId == OrderId).FirstOrDefault();
                Payment.Id = item.Id;
                Payment.CustomerId = Convert.ToInt32(item.CustomerId);
                Payment.Contact = item.Contact;
                Payment.PaymentAmount = Convert.ToDecimal(item.PaymentAmount);
                Payment.PaymentDate = Convert.ToDateTime(item.PaymentDate);
                Payment.OrderId = Convert.ToInt32(item.OrderId);

                return Payment;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PaymentEntities> GetPayments(string Contact)
        {
            List<PaymentEntities> PaymentList = new List<PaymentEntities>();
            try
            {
                var list = db.PaymentTBs.Where(d=>d.Contact == Contact).ToList();

                foreach (var item in list)
                {
                    PaymentEntities brand = new PaymentEntities();
                    brand.Id = item.Id;
                    brand.CustomerId = Convert.ToInt32(item.CustomerId);
                    brand.Contact = item.Contact;
                    brand.PaymentAmount = Convert.ToDecimal(item.PaymentAmount);
                    brand.PaymentDate = Convert.ToDateTime(item.PaymentDate);
                    brand.OrderId = Convert.ToInt32(item.OrderId);
                    PaymentList.Add(brand);
                }

                return PaymentList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}