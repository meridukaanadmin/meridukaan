﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Customer;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class CustomerService : ICustomerService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<CustomerEntities> GetCustomerList()
        {
            List<CustomerEntities> customerList = new List<CustomerEntities>();

            try
            {
                var list = db.CustomerTBs.OrderBy(d => d.Name).ToList();

                foreach (var item in list)
                {
                    CustomerEntities customer = new CustomerEntities();
                    customer.Id = item.Id;
                    customer.CustomerId = item.CustomerId;
                    customer.Name = item.Name;
                    customer.MobileNo = item.MobileNo;
                    customer.Address = item.Address;
                    customer.PinCode = item.PinCode;
                    customer.EmailId = item.EmailId;
                    customerList.Add(customer);
                }

                return customerList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CustomerEntities GetCustomerDetails(int Id)
        {
            CustomerEntities customer = new CustomerEntities();

            try
            {
                var data = db.CustomerTBs.OrderBy(d => d.Id == Id).FirstOrDefault();

                customer.Id = data.Id;
                customer.CustomerId = data.CustomerId;
                customer.Name = data.Name;
                customer.MobileNo = data.MobileNo;
                customer.Address = data.Address;
                customer.PinCode = data.PinCode;
                customer.EmailId = data.EmailId;

                return customer;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateCustomerDetails(CustomerEntities customer)
        {
            try
            {
                CustomerTB tB = new CustomerTB();

                var checkcust = db.CustomerTBs.Where(d => d.MobileNo == customer.MobileNo).FirstOrDefault();

                if (checkcust != null)
                {
                    checkcust.Name = customer.Name;
                    checkcust.Address = customer.Address;
                    checkcust.PinCode = customer.PinCode;
                    checkcust.EmailId = customer.EmailId;
                    db.Entry(checkcust).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    Random generator = new Random();
                    string rand = generator.Next(0, 1000000).ToString("D6");
                    tB.CustomerId = rand;
                    tB.Name = customer.Name;
                    tB.MobileNo = customer.MobileNo;
                    tB.Address = customer.Address;
                    tB.PinCode = customer.PinCode;
                    tB.EmailId = customer.EmailId;
                    db.CustomerTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteCustomerDetails(string Contact)
        {
            try
            {
                CustomerTB tB = new CustomerTB();

                var Check = db.CustomerTBs.Where(d => d.MobileNo == Contact).FirstOrDefault();
                if (Check.Id > 0)
                {
                    Check.IsActive = false;
                    db.Entry(Check).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<CustomerOrderEntities> GetCustomerOrdersList(string Contact)
        {
            List<CustomerOrderEntities> orderList = new List<CustomerOrderEntities>();
            
            try
            {
                var orders = db.SaleOrderTBs.Where(d => d.Contact == Contact && d.IsActive == true).ToList();

                foreach (var item in orders)
                {
                    CustomerOrderEntities customerOrder = new CustomerOrderEntities();

                    customerOrder.SaleId = item.SaleId;
                    customerOrder.SaleDate = Convert.ToDateTime(item.SaleDate);
                    customerOrder.SaleNo = item.SaleNo;
                    customerOrder.CustomerName = item.CustomerName;
                    customerOrder.Contact = item.Contact;
                    customerOrder.SalesmanNo = item.SalesmanNo;
                    customerOrder.SalesmanId = Convert.ToInt32(item.SalesmanId);
                    customerOrder.Total = Convert.ToDecimal(item.Total);
                    customerOrder.TaxAmount = Convert.ToDecimal(item.TaxAmount);
                    customerOrder.GrandTotal = Convert.ToDecimal(item.GrandTotal);

                    customerOrder.orderDetails = new List<CustomerOrderDetailEnitities>();
                    string baseadder = ConfigurationManager.AppSettings["WebUIUrl"];

                    var saledetails = db.SaleOrderDetailsTBs.Where(d => d.SaleId == item.SaleId).ToList();

                    foreach (var detail in saledetails)
                    {
                        CustomerOrderDetailEnitities customerOrderDetail = new CustomerOrderDetailEnitities();
                        customerOrderDetail.SaleDetId = detail.SaleDetId;
                        customerOrderDetail.ProductId = Convert.ToInt32(detail.ProductId);
                        customerOrderDetail.SizeId = Convert.ToInt32(detail.SizeId);

                        var prd = db.ProductTBs.Where(x => x.Id == detail.ProductId).FirstOrDefault();
                        customerOrderDetail.ProductName = prd.Name;

                        var size = db.SizeTBs.Where(x => x.Id == detail.SizeId).FirstOrDefault();
                        customerOrderDetail.Size = size.Name;

                        var prdattr = db.Product_AttributeTB.Where(x => x.ProductId == detail.ProductId && x.PrSizeId == detail.SizeId).FirstOrDefault();
                        customerOrderDetail.ImagePath = baseadder + prdattr.ImagePath;

                        customerOrderDetail.SaleQty = detail.SaleQty;
                        customerOrderDetail.SaleRate = Convert.ToDecimal(detail.SaleRate);
                        customerOrderDetail.TaxAmount = Convert.ToDecimal(detail.TaxAmount);
                        customerOrderDetail.Total = Convert.ToDecimal(detail.Total);
                        customerOrderDetail.GrandTotal = Convert.ToDecimal(detail.GrandAmount);
                        customerOrderDetail.Discount = Convert.ToDecimal(detail.Discount);

                        customerOrder.orderDetails.Add(customerOrderDetail);
                    }

                    orderList.Add(customerOrder);
                }

                return orderList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CustomerAddressEntities> GetCustomerAddress(string Contact)
        {
            List<CustomerAddressEntities> customerList = new List<CustomerAddressEntities>();

            try
            {
                var list = db.CustomerAddressTBs.Where(d=>d.Contact == Contact).OrderBy(d => d.Id).ToList();

                foreach (var item in list)
                {
                    CustomerAddressEntities customer = new CustomerAddressEntities();
                    customer.Id = item.Id;
                    customer.Contact = item.Contact;
                    customer.Address = item.Address;
                    customer.PinCode = item.PinCode;
                    customer.IsDefault = item.IsDefault;                    
                    customerList.Add(customer);
                }

                return customerList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateCustomerAddressDetails(CustomerAddressEntities customer)
        {
            try
            {
                CustomerAddressTB tB = new CustomerAddressTB();

                if(customer.Id > 0)
                {
                    var checkaddress = db.CustomerAddressTBs.Where(d => d.Id == customer.Id).FirstOrDefault();
                    checkaddress.Contact = customer.Contact;
                    checkaddress.Address = customer.Address;
                    checkaddress.PinCode = customer.PinCode;
                    db.Entry(checkaddress).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    var checkcust = db.CustomerAddressTBs.Where(d => d.Contact == customer.Contact).ToList();

                    tB.Contact = customer.Contact;
                    tB.Address = customer.Address;
                    tB.PinCode = customer.PinCode;

                    if (checkcust.Count() > 0)
                    {
                        tB.IsDefault = false;
                    }
                    else
                    {                       
                        tB.IsDefault = true;                       
                    }

                    db.CustomerAddressTBs.Add(tB);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<CustomerReportEntities> GetCustomerReportList()
        {
            List<CustomerReportEntities> customerList = new List<CustomerReportEntities>();

            try
            {
                var list = db.CustomerTBs.OrderBy(d => d.Name).ToList();

                foreach (var item in list)
                {
                    CustomerReportEntities customer = new CustomerReportEntities();
                    customer.CustomerId = Convert.ToInt32(item.CustomerId);
                    customer.Name = item.Name;
                    customer.Contact = item.MobileNo;
                    customer.EmailId = item.EmailId;
                    customer.Address = item.Address;

                    var sale = db.SaleOrderTBs.Where(d => d.Contact == item.MobileNo).ToList();
                    customer.Totalorders = sale.Count();
                    decimal? grnd = sale.Sum(d => d.GrandTotal);
                    customer.Totalspend = Convert.ToDecimal(grnd);
                    customerList.Add(customer);
                }

                return customerList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}