﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities;
using Meridukan_API.Entities.Unit;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class UnitDAL : IUnitDAL
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<UnitEntities> GetUnit()
        {
            List<UnitEntities> unitList = new List<UnitEntities>();

            try
            {
                var list = db.UnitTBs.OrderBy(d => d.Name).ToList();

                foreach (var item in list)
                {
                    UnitEntities unit = new UnitEntities();
                    unit.Id = (long)item.Id;
                    unit.CreatedOn = Convert.ToDateTime(item.CreatedOn);
                    unit.Name = item.Name;
                    unitList.Add(unit);
                }

                return unitList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public WebAPIResponse AddOrUpdateUnit(AddorUpdateUnit unit)
        {
            WebAPIResponse Response = new WebAPIResponse();

            try
            {
                UnitTB tB = new UnitTB();
                var CheckUnit = db.UnitTBs.Where(d => d.Name == unit.Name).FirstOrDefault();
                if (CheckUnit == null)
                {

                    if (unit.Id > 0)
                    {
                        tB.Id = (int)unit.Id;
                        tB.Name = unit.Name;
                        tB.UpdatedOn = DateTime.Now;
                        db.Entry(CheckUnit).State = EntityState.Modified;
                        db.SaveChanges();
                        Response.Message = "Success";
                        Response.Description = "Unit Updated successfully";
                    }
                    else
                    {
                        tB.Id = (int)unit.Id;
                        tB.Name = unit.Name;
                        tB.CreatedOn = DateTime.Now;
                        db.UnitTBs.Add(tB);
                        db.SaveChanges();
                        Response.Message = "Success";
                        Response.Description = "Unit Created successfully";
                    }
                }
                else
                {
                    Response.Message = "failure";
                    Response.Description = "Unit already exist";
                }
            }
            catch (Exception ex)
            {
                Response.Message = "failure";
                Response.Description = ex.Message;
            }

            return Response;

        }
    }
}