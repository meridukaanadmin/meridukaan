﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Offer;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class OfferService : IOfferService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<OfferEnitities> GetOfferList()
        {
            List<OfferEnitities> enitities = new List<OfferEnitities>();
            try
            {
                var list = db.OfferTBs.OrderByDescending(d=>d.EnteredDate).ToList();

                foreach (var item in list)
                {
                    OfferEnitities offer = new OfferEnitities();

                    offer.Id = item.Id;
                    offer.OfferName = item.OfferName;
                    offer.EnteredDate = Convert.ToDateTime(item.EnteredDate);
                    offer.ExpiryDate = Convert.ToDateTime(item.Expirydate);
                    offer.OfferImagePath = item.OfferImagePath;
                    enitities.Add(offer);
                }

            }
            catch(Exception ex)
            {

            }
            return enitities;
        }

        public OfferEnitities GetOfferDetails(int Id)
        {
            OfferEnitities offer = new OfferEnitities();
            offer.offerDetails = new List<OfferDetailEnitities>();

            try
            {
                var data = db.OfferTBs.Where(d=>d.Id == Id).FirstOrDefault();

                offer.Id = data.Id;
                offer.OfferName = data.OfferName;
                offer.EnteredDate = Convert.ToDateTime(data.EnteredDate);
                offer.ExpiryDate = Convert.ToDateTime(data.Expirydate);
                offer.OfferImagePath = data.OfferImagePath;

                var details = db.OfferDetailsTBs.Where(d => d.OfferId == data.Id).ToList();

                foreach (var item in details)
                {
                    OfferDetailEnitities offerDetail = new OfferDetailEnitities();
                    offerDetail.BrandId = item.BrandId;
                    var brand = db.BrandTBs.Where(d => d.Id == item.BrandId).FirstOrDefault();
                    offerDetail.Brand = brand.Name;
                    var category = db.CategoryTBs.Where(d => d.Id == item.CategoryId).FirstOrDefault();
                    offerDetail.Category = category.Name;
                    offerDetail.CategoryId = item.CategoryId;
                    offerDetail.DiscountInPerc = Convert.ToInt32(item.DiscountInPerc);
                    offerDetail.DiscountInAmount = Convert.ToInt32(item.DiscountInAmount);
                    offer.offerDetails.Add(offerDetail);
                }
            }
            catch (Exception ex)
            {

            }
            return offer;
        }

        public bool AddorUpdateOffer(OfferEnitities offer)
        {
            try
            {
                OfferTB tB = new OfferTB();

                if (offer.Id > 0)
                {
                    var check = db.OfferTBs.Where(d => d.Id == offer.Id).FirstOrDefault();

                    check.OfferName = offer.OfferName;
                    check.OfferImagePath = offer.OfferImagePath;
                    check.EnteredDate = offer.EnteredDate;
                    check.Expirydate = offer.ExpiryDate;
                    db.Entry(check).State = EntityState.Modified;
                    db.SaveChanges();

                    var Checkdt = db.OfferDetailsTBs.Where(d => d.OfferId == offer.Id).ToList();
                    foreach (var item in Checkdt)
                    {
                        db.OfferDetailsTBs.Remove(item);
                        db.SaveChanges();
                    }

                    OfferDetailsTB offerDetails = new OfferDetailsTB();
                    foreach (var i in offer.offerDetails)
                    {
                        offerDetails.OfferId = check.Id;
                        offerDetails.BrandId = i.BrandId;
                        offerDetails.CategoryId = i.CategoryId;
                        offerDetails.DiscountInAmount = i.DiscountInAmount;
                        offerDetails.DiscountInPerc = i.DiscountInPerc;
                        db.OfferDetailsTBs.Add(offerDetails);
                        db.SaveChanges();
                    }
                }
                else
                {
                    tB.Id = offer.Id;
                    tB.OfferName = offer.OfferName;
                    tB.OfferImagePath = offer.OfferImagePath;
                    tB.EnteredDate = offer.EnteredDate;
                    tB.Expirydate = offer.ExpiryDate;
                    db.OfferTBs.Add(tB);
                    db.SaveChanges();

                    OfferDetailsTB offerDetails = new OfferDetailsTB();
                    foreach (var i in offer.offerDetails)
                    {
                        offerDetails.OfferId = tB.Id;
                        offerDetails.BrandId = i.BrandId;
                        offerDetails.CategoryId = i.CategoryId;
                        offerDetails.DiscountInAmount = i.DiscountInAmount;
                        offerDetails.DiscountInPerc = i.DiscountInPerc;
                        db.OfferDetailsTBs.Add(offerDetails);
                        db.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteOffer(int OfferId)
        {
            try
            {
                OfferTB tB = new OfferTB();

                if (OfferId > 0)
                {
                    var Checkdt = db.OfferDetailsTBs.Where(d => d.OfferId == OfferId).ToList();
                    foreach (var item in Checkdt)
                    {
                        db.OfferDetailsTBs.Remove(item);
                    }

                    var check = db.OfferTBs.Where(d => d.Id == OfferId).FirstOrDefault();
                    if(check != null)
                    {
                        db.OfferTBs.Remove(check);                       
                    }
                    
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }    
    }
}