﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Product;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class ProductService : IProductService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        DbConnection conn = null;
        public ProductService()
        {
            conn = new DbConnection();
        }

        #region ProductType

        public List<ProductTypeEntities> GetProductTypeList()
        {
            List<ProductTypeEntities> ProductTypeList = new List<ProductTypeEntities>();
            string baseadder = ConfigurationManager.AppSettings["WebUIUrl"];
            try
            {
                var list = db.ProductTypeTBs.OrderBy(d => d.Name).ToList();

                foreach (var item in list)
                {
                    ProductTypeEntities ProductType = new ProductTypeEntities();
                    ProductType.Id = item.Id;
                    ProductType.Name = item.Name;
                    ProductTypeList.Add(ProductType);
                }

                return ProductTypeList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProductTypeEntities GetProductTypeDetails(int ProductTypeId)
        {
            try
            {
                ProductTypeEntities ProductType = new ProductTypeEntities();

                var data = db.ProductTypeTBs.Where(d => d.Id == ProductTypeId).FirstOrDefault();

                ProductType.Id = data.Id;
                ProductType.Name = data.Name;

                return ProductType;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateProductTypeDetails(ProductTypeEntities ProductType)
        {

            try
            {
                ProductTypeTB tB = new ProductTypeTB();

                if (ProductType.Id > 0)
                {

                    tB.Id = ProductType.Id;
                    tB.Name = ProductType.Name;

                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    tB.Id = ProductType.Id;
                    tB.Name = ProductType.Name;
                    db.ProductTypeTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteProductTypeDetails(int Id)
        {
            try
            {
                ProductTypeTB tB = new ProductTypeTB();
                var CheckProductType = db.ProductTypeTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (CheckProductType.Id > 0)
                {
                    db.ProductTypeTBs.Remove(CheckProductType);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        #endregion

        #region Product_Attribute
        public List<ProductAttributeEntities> GetProductAttributeList()
        {
            List<ProductAttributeEntities> ProductAttributeList = new List<ProductAttributeEntities>();
            string baseadder = ConfigurationManager.AppSettings["WebUIUrl"];
            try
            {
                var list = db.Product_AttributeTB.OrderBy(d => d.Id).ToList();

                foreach (var item in list)
                {
                    ProductAttributeEntities ProductAttribute = new ProductAttributeEntities();
                    ProductAttribute.Id = item.Id;
                    ProductAttribute.ProductId = (int)item.ProductId;
                    //ProductAttribute.ColorId = (int)item.ColorId;
                    //ProductAttribute.Size = (int)item.Size;
                    //ProductAttribute.WeightageId = (int)item.WeightageId;
                    ProductAttribute.Tags = item.Tags;
                    // ProductAttribute.RegularPrice = Convert.ToDecimal(item.RegularPrice);
                    ProductAttribute.SalePrice = Convert.ToDecimal(item.SalePrice);
                    ProductAttribute.DiscountPerc = Convert.ToDecimal(item.DiscountPerc);
                    ProductAttribute.CreatedOn = Convert.ToDateTime(item.CreatedOn);
                    ProductAttribute.CreatedBy = (int)item.CreatedBy;
                    ProductAttribute.MaximumOrderQty = (int)item.MaximumOrderQty;
                    ProductAttribute.MinimumStockQty = (int)item.MinimumStockQty;
                    //ProductAttribute.NewPrice = Convert.ToDecimal(item.NewPrice);
                    //ProductAttribute.OldPrice = Convert.ToDecimal(item.OldPrice);

                    ProductAttributeList.Add(ProductAttribute);
                }

                return ProductAttributeList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProductAttributeEntities GetProductAttributeDetails(int ProductAttributeId)
        {
            try
            {
                ProductAttributeEntities ProductAttribute = new ProductAttributeEntities();

                var data = db.Product_AttributeTB.Where(d => d.Id == ProductAttributeId).FirstOrDefault();

                ProductAttribute.Id = data.Id;
                ProductAttribute.ProductId = (int)data.ProductId;

                ProductAttribute.Tags = data.Tags;
                ProductAttribute.SalePrice = Convert.ToDecimal(data.SalePrice);
                ProductAttribute.DiscountPerc = Convert.ToDecimal(data.DiscountPerc);
                ProductAttribute.CreatedOn = Convert.ToDateTime(data.CreatedOn);
                ProductAttribute.CreatedBy = (int)data.CreatedBy;
                ProductAttribute.MaximumOrderQty = (int)data.MaximumOrderQty;
                ProductAttribute.MinimumStockQty = (int)data.MinimumStockQty;

                return ProductAttribute;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateProductAttributeDetails(ProductAttributeEntities ProductAttribute)
        {

            try
            {
                Product_AttributeTB tB = new Product_AttributeTB();

                if (ProductAttribute.Id > 0)
                {

                    tB.Id = ProductAttribute.Id;
                    tB.ProductId = (int)ProductAttribute.ProductId;

                    tB.Tags = ProductAttribute.Tags;

                    tB.SalePrice = Convert.ToDecimal(ProductAttribute.SalePrice);
                    tB.DiscountPerc = Convert.ToDecimal(ProductAttribute.DiscountPerc);
                    tB.UpdatedOn = Convert.ToDateTime(ProductAttribute.UpdatedOn);
                    tB.UpdatedBy = (int)ProductAttribute.UpdatedBy;
                    tB.MaximumOrderQty = (int)ProductAttribute.MaximumOrderQty;
                    tB.MinimumStockQty = (int)ProductAttribute.MinimumStockQty;

                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {

                    tB.Id = ProductAttribute.Id;
                    tB.ProductId = (int)ProductAttribute.ProductId;

                    tB.Tags = ProductAttribute.Tags;
                    tB.SalePrice = Convert.ToDecimal(ProductAttribute.SalePrice);
                    tB.DiscountPerc = Convert.ToDecimal(ProductAttribute.DiscountPerc);
                    tB.UpdatedOn = Convert.ToDateTime(ProductAttribute.UpdatedOn);
                    tB.UpdatedBy = (int)ProductAttribute.UpdatedBy;
                    tB.MaximumOrderQty = (int)ProductAttribute.MaximumOrderQty;
                    tB.MinimumStockQty = (int)ProductAttribute.MinimumStockQty;

                    db.Product_AttributeTB.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteProductAttributeDetails(int Id)
        {
            try
            {
                Product_AttributeTB tB = new Product_AttributeTB();
                var CheckProductAttribute = db.Product_AttributeTB.Where(d => d.Id == Id).FirstOrDefault();
                if (CheckProductAttribute.Id > 0)
                {
                    db.Product_AttributeTB.Remove(CheckProductAttribute);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
        #endregion

        #region Product
        public object GetProductList()
        {
            List<ProductEntities> ProductList = new List<ProductEntities>();
            string baseadder = ConfigurationManager.AppSettings["WebUIUrl"];
            try
            {
                var list1 = db.ProductTBs.Where(d=>d.IsActive == true).ToList();
                var list2 = db.Product_AttributeTB.ToList();
                

                var list = (from i in list1
                            join j in list2 on i.Id equals j.ProductId
                            join k in db.CategoryTBs on i.CategoryId equals k.Id
                            join l in db.SubcategoryTBs on i.SubCategoryId equals l.Id
                            join m in db.BrandTBs on i.BrandId equals m.Id
                            select new
                            {
                                Id = i.Id,
                                Name=i.Name,
                                i.ShortDescription,
                                i.Description,
                                i.Tags,
                                i.UserId,
                                i.Barcode,
                                j.SKU,
                                j.MRP,
                                j.DiscountPerc,
                                j.DiscountPrice,
                                i.CategoryId,
                                Category=k.Name,
                                i.SubCategoryId,
                                SubCategory = l.Name,
                                i.BrandId,
                                Brand = m.Name,
                                j.StockQty,
                                j.MinimumStockQty,
                                j.MaximumOrderQty,
                                j.IsAllowOrder,
                                j.IsPublished

                            }).ToList();
                 
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AddProductModel GetProductDetails(int ProductId)
        {
           
            AddProductModel product = new AddProductModel();
            product.ProductAtributes = new List<ProductAttributes>();

            string baseadder = ConfigurationManager.AppSettings["WebUIUrl"];
            try
            {
                var list1 = db.ProductTBs.Where(c=>c.Id == ProductId).FirstOrDefault();
                var list2 = db.Product_AttributeTB.Where(d=>d.ProductId == ProductId).ToList();
                var list3 = db.Product_Attribute_Weight.Where(d => d.ProductId == ProductId).FirstOrDefault();

                product.Id = list1.Id;
                product.ProductName = list1.Name;
                product.ShortDescription = list1.ShortDescription;
                product.ProductDescription = list1.Description;
                product.Tags = list1.Tags;
                product.UserId= Convert.ToInt32(list1.UserId);
               // product.Barcode= list1.Barcode;
                product.CategoryId = Convert.ToInt32(list1.CategoryId);
                product.SubCategoryId = Convert.ToInt32(list1.SubCategoryId);
                product.BrandId = Convert.ToInt32(list1.BrandId);

                if(list3 != null)
                {
                    product.Weight = (float)list3.Weight;
                    product.Length = (float)list3.Length;
                    product.Height = (float)list3.Height;
                    product.Width = (float)list3.Width;
                }
                else
                {
                    product.Weight = 0;
                    product.Length = 0;
                    product.Height = 0;
                    product.Width = 0;
                }

               

                foreach (var c in list2)
                {
                    ProductAttributes attribute = new ProductAttributes();
                    attribute.Id=c.Id;
                    attribute.PrAttribute_Id = Convert.ToInt32(c.ProductId);
                    attribute.SKU = c.SKU;
                    attribute.Tags = c.Tags;
                    attribute.MRP = Convert.ToDecimal(c.MRP);
                    attribute.DiscountPerc = Convert.ToDecimal(c.DiscountPerc);
                    attribute.DiscountPrice = Convert.ToDecimal(c.DiscountPrice);

                    attribute.SalePrice= Convert.ToDecimal(c.SalePrice);
                    attribute.SaleStart = Convert.ToDateTime(c.SaleStart);
                    attribute.SaleEnd = Convert.ToDateTime(c.SaleStart);
                    attribute.StockQty = Convert.ToInt32(c.StockQty);
                    attribute.MaximumOrderQty = Convert.ToInt32(c.MaximumOrderQty);
                    attribute.MinimumStockQty = Convert.ToInt32(c.MinimumStockQty); 
                    attribute.PrColorId = Convert.ToInt32(c.PrColorId);
                    attribute.PrColor = db.ColorTBs.Where(d=>d.Id == c.PrColorId).FirstOrDefault().Name;
                    attribute.PrSizeId = Convert.ToInt32(c.PrSizeId);
                    attribute.PrSize = db.SizeTBs.Where(d => d.Id == c.PrSizeId).FirstOrDefault().Name;

                    product.ProductAtributes.Add(attribute);
                }



                //var list = (from i in list1 
                //            select new
                //            {
                //                Id = i.Id,
                //                ProductName = i.Name,
                //                ShortDescription=i.ShortDescription,
                //                ProductDescription=i.Description,
                //                Tags=i.Tags,
                //                i.UserId,
                //                i.Barcode,
                //                ProductAtributes = db.Product_AttributeTB.Where(d => d.ProductId == ProductId).Select(c=> new {

                //                    c.Id,
                //                    PrAttribute_Id=c.ProductId,
                //                    c.SKU,
                //                    c.Weight, 
                //                    c.Length,
                //                    c.Height,
                //                    c.Width,

                //                    c.Tags,
                //                    c.MRP,
                //                    c.DiscountPerc,
                //                    c.DiscountPrice,

                //                    c.SalePrice,
                //                    c.SaleStart,
                //                    c.SaleEnd,
                //                    c.StockQty, 
                //                    c.MaximumOrderQty,
                //                    c.MinimumStockQty,

                //                    c.CategoryId,
                //                    c.SubCategoryId,
                //                    c.BrandId,

                //                    //c.PrColorId,
                //                    //c.PrSizeId


                //                })
                                     
                                    
                //            }).ToList();

                return product;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductEntities> GetProducts(int CategoryId)
        {
            List<ProductEntities> products = new List<ProductEntities>();
            string baseadder = ConfigurationManager.AppSettings["WebUIUrl"];

            try
            {
                var prdlist = db.ProductTBs.Where(c => c.CategoryId == CategoryId).ToList();

                foreach (var item in prdlist)
                {
                    ProductEntities product = new ProductEntities();

                    product.Id = item.Id;
                    product.ProductName = item.Name;
                    product.ShortDescription = item.ShortDescription;
                    product.ProductDescription = item.Description;
                    product.Tags = item.Tags;
                    product.UserId = Convert.ToInt32(item.UserId);                    
                    product.CategoryId = Convert.ToInt32(item.CategoryId);
                    product.SubCategoryId = Convert.ToInt32(item.SubCategoryId);
                    product.BrandId = Convert.ToInt32(item.BrandId);

                    var prdattrlist = db.Product_AttributeTB.Where(d => d.ProductId == item.Id).ToList();
                    product.ProductAttributes = new List<ProductAttributes>();

                    foreach (var attr in prdattrlist)
                    {
                        ProductAttributes attribute = new ProductAttributes();
                        attribute.Id = attr.Id;
                        attribute.PrAttribute_Id = Convert.ToInt32(attr.ProductId);
                        attribute.SKU = attr.SKU;
                        attribute.Tags = attr.Tags;
                        attribute.MRP = Convert.ToDecimal(attr.MRP);
                        attribute.DiscountPerc = Convert.ToDecimal(attr.DiscountPerc);
                        attribute.DiscountPrice = Convert.ToDecimal(attr.DiscountPrice);
                        attribute.ImagePath = baseadder + attr.ImagePath;
                        attribute.SalePrice = Convert.ToDecimal(attr.SalePrice);
                        attribute.SaleStart = Convert.ToDateTime(attr.SaleStart);
                        attribute.SaleEnd = Convert.ToDateTime(attr.SaleStart);
                        attribute.StockQty = Convert.ToInt32(attr.StockQty);
                        attribute.MaximumOrderQty = Convert.ToInt32(attr.MaximumOrderQty);
                        attribute.MinimumStockQty = Convert.ToInt32(attr.MinimumStockQty);
                        attribute.PrColorId = Convert.ToInt32(attr.PrColorId);
                        attribute.PrColor = db.ColorTBs.Where(d => d.Id == attr.PrColorId).FirstOrDefault().Name;
                        attribute.PrSizeId = Convert.ToInt32(attr.PrSizeId);
                        attribute.PrSize = db.SizeTBs.Where(d => d.Id == attr.PrSizeId).FirstOrDefault().Name;

                        product.ProductAttributes.Add(attribute);
                    }

                    products.Add(product);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return products;
        }

        public bool AddorUpdateProductDetails(AddProductModel Product)
        {
            try
            {
                ProductTB tB = new ProductTB();

                if (Product.Id > 0)
                {
                    tB.Id = Product.Id;
                    tB.Name = Product.ProductName;
                    tB.UserId = (int)Product.UserId;                   
                    tB.Description = Product.ProductDescription;
                    tB.ShortDescription = Product.ShortDescription;
                    tB.CategoryId = Product.CategoryId;
                    tB.SubCategoryId = Product.SubCategoryId;
                    tB.BrandId = Product.BrandId;

                    db.Entry(tB).State = EntityState.Modified;

                    Product_Attribute_Weight wt = new Product_Attribute_Weight();

                    var CheckWeight = db.Product_Attribute_Weight.Where(d => d.ProductId == Product.Id).FirstOrDefault();
                    if (CheckWeight != null)
                    {
                        db.Product_Attribute_Weight.Remove(CheckWeight);
                        db.SaveChanges();
                    }
                    
                    // wt.Id = CheckWeight.Id;
                    wt.ProductId = Product.Id;
                    wt.Weight = Product.Weight;
                    wt.Length = Product.Length;
                    wt.Height = Product.Height;
                    wt.Width = Product.Width;
                    db.Product_Attribute_Weight.Add(wt);
                    db.SaveChanges();

                    Product_AttributeTB ProTB = new Product_AttributeTB();

                    var CheckAttr = db.Product_AttributeTB.Where(d => d.ProductId == Product.Id).FirstOrDefault();
                    if (CheckAttr != null)
                    {
                        db.Product_AttributeTB.Remove(CheckAttr);
                        db.SaveChanges();
                    }

                    foreach (var i in Product.ProductAtributes)
                    {
                       // ProTB.Id = i.Id;
                        ProTB.ProductId = tB.Id;
                        ProTB.SKU = i.SKU;                       
                        ProTB.Tags = i.Tags;
                        ProTB.MRP = i.MRP;
                        ProTB.DiscountPerc = i.DiscountPerc;
                        ProTB.DiscountPrice = i.DiscountPrice;
                        ProTB.SalePrice = i.SalePrice;
                        ProTB.SaleStart = i.SaleStart;
                        ProTB.SaleEnd = i.SaleEnd;
                        ProTB.MaximumOrderQty = i.MaximumOrderQty;
                        ProTB.MinimumStockQty = i.MinimumStockQty;
                        ProTB.StockQty = i.StockQty;
                        ProTB.IsAllowOrder = i.IsAllowOrder;
                        ProTB.IsPublished = i.IsPublished;
                        ProTB.IsAllowNegativeStock = i.IsAllowNegativeStock;
                        ProTB.PrSizeId = i.PrSizeId;
                        ProTB.PrColorId = i.PrColorId;
                        ProTB.ImageName = i.ImageName;
                        ProTB.ImagePath = i.ImagePath;


                        string uniqueId = Convert.ToString(ProTB.ProductId);
                        int newNumber = Convert.ToInt32(uniqueId) + 1;
                        uniqueId = newNumber.ToString("000000");

                        ProTB.Barcode = uniqueId;

                        db.Product_AttributeTB.Add(ProTB);
                        db.SaveChanges();
                    }
                }
                else
                {

                    tB.Id = Product.Id;
                    tB.Name = Product.ProductName;
                    tB.UserId = (int)Product.UserId;
                   // tB.Barcode = Product.Barcode;
                    tB.Tags= Product.Barcode;
                    tB.Description = Product.ProductDescription;
                    tB.ShortDescription = Product.ShortDescription;
                    tB.CategoryId = Product.CategoryId;
                    tB.SubCategoryId = Product.SubCategoryId;
                    tB.BrandId = Product.BrandId;

                    db.ProductTBs.Add(tB);

                    Product_Attribute_Weight wt = new Product_Attribute_Weight();
                    // wt.Id = CheckWeight.Id;
                    wt.ProductId = Product.Id;
                    wt.Weight = Product.Weight;
                    wt.Length = Product.Length;
                    wt.Height = Product.Height;
                    wt.Width = Product.Width;
                    db.Product_Attribute_Weight.Add(wt);
                    db.SaveChanges();

                    Product_AttributeTB ProTB = new Product_AttributeTB();
                    foreach (var i in Product.ProductAtributes)
                    {
                        ProTB.ProductId = tB.Id;
                        ProTB.SKU = i.SKU;                       
                        ProTB.Tags = i.Tags;
                        ProTB.MRP = i.MRP;
                        ProTB.DiscountPerc = i.DiscountPerc;
                        ProTB.DiscountPrice = i.DiscountPrice;
                        ProTB.SalePrice = i.SalePrice;
                        ProTB.SaleStart = i.SaleStart;
                        ProTB.SaleEnd = i.SaleEnd;
                        ProTB.MaximumOrderQty = i.MaximumOrderQty;
                        ProTB.MinimumStockQty = i.MinimumStockQty;
                        ProTB.StockQty = i.StockQty;
                        ProTB.IsAllowOrder = i.IsAllowOrder;
                        ProTB.IsPublished = i.IsPublished;
                        ProTB.PrColorId = i.PrColorId;
                        ProTB.PrSizeId = i.PrSizeId;
                        ProTB.PrColorId = i.PrColorId;
                        ProTB.ImageName = i.ImageName;

                        string uniqueId = Convert.ToString(ProTB.ProductId);
                        int newNumber = Convert.ToInt32(uniqueId) + 1;
                        uniqueId = newNumber.ToString("000000");

                        ProTB.Barcode = uniqueId;

                        db.Product_AttributeTB.Add(ProTB);
                    }
                     
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteProductDetails(int Id)
        {
            try
            {
                ProductTB tB = new ProductTB();
                var CheckProduct = db.ProductTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (CheckProduct.Id > 0)
                {
                    db.ProductTBs.Remove(CheckProduct);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
        #endregion

        public bool DeleteProduct(int Id)
        {
            try
            {
                ProductTB tB = new ProductTB();
                var CheckProduct = db.ProductTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (CheckProduct.Id > 0)
                {
                    CheckProduct.IsActive = false;
                    db.Entry(CheckProduct).State = EntityState.Modified;                    
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<ProductGRNListEntities> GetProductGRNList(int ProductId)
        {
            List<ProductGRNListEntities> grnList = new List<ProductGRNListEntities>();

            try
            {

                SqlConnection con = conn.OpenDbConnection();
                SqlCommand cmd = new SqlCommand("ProductGRNList", con);
                cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = ProductId;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    ProductGRNListEntities grn = new ProductGRNListEntities();
                    grn.GrnId = Convert.ToInt32(dr["GrnId"]);                    
                    grn.GrnNo = Convert.ToString(dr["GrnNo"]);
                    grn.ProductId = Convert.ToInt32(dr["ProductId"]);
                    grn.Size = Convert.ToString(dr["Size"]);
                    grn.SizeId = Convert.ToInt32(dr["SizeId"]);
                    grn.ReceiveQty = Convert.ToString(dr["ReceiveQty"]);
                    grn.SaleQty = Convert.ToString(dr["SaleQty"]);
                    grn.Stock = Convert.ToString(dr["Stock"]);

                    grnList.Add(grn);
                }

                con.Close();

                return grnList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SaleProductAttributes> GetSaleProductList()
        {
            List<SaleProductAttributes> saleProductsList = new List<SaleProductAttributes>();
            
            try
            {
                var list = from d in db.ProductTBs
                           join p in db.Product_AttributeTB on d.Id equals p.ProductId
                           select new
                           {
                              AttributeId = p.Id,
                              ProductId =  d.Id,
                              ProductName = d.Name,
                              SizeId = p.PrSizeId,
                              Size = db.SizeTBs.Where(x=>x.Id == p.PrSizeId).Select(c=>c.Name).FirstOrDefault(),
                              Barcode = p.Barcode
                           };

                foreach (var item in list)
                {
                    SaleProductAttributes sale = new SaleProductAttributes();
                    sale.PrAttributeId = item.AttributeId;
                    sale.ProductId = item.ProductId;                    
                    sale.SizeId = Convert.ToInt32(item.SizeId);
                    sale.SizeName = item.Size;
                    sale.ProductName = item.ProductName + "-"+ item.Size;
                    sale.Barcode = item.Barcode;
                    saleProductsList.Add(sale);
                }

                return saleProductsList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       
    }
}