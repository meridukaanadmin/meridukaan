﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Transport;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class TransportService : ITransportService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<TransportEntities> GetTransportDetailList()
        {
            List<TransportEntities> transportList = new List<TransportEntities>();
            
            try
            {
                var list = db.TransportTBs.OrderBy(d => d.Id).ToList();

                foreach (var item in list)
                {
                    TransportEntities transport = new TransportEntities();
                    transport.Id = item.Id;
                    transport.Partner = item.Partner;
                    transport.LR_No = item.LR_No;
                    transport.Description = item.Description;
                    transportList.Add(transport);
                }

                return transportList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TransportEntities GetTransportDetails(int Id)
        {
            try
            {
                TransportEntities transport = new TransportEntities();

                var data = db.TransportTBs.Where(d => d.Id == Id).FirstOrDefault();

                transport.Id = data.Id;
                transport.Partner = data.Partner;
                transport.LR_No = data.LR_No;
                transport.Description = data.Description;
                return transport;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateTransportDetails(TransportEntities transport)
        {
            try
            {
                TransportTB tB = new TransportTB();

                if (transport.Id > 0)
                {
                    tB.Id = transport.Id;
                    tB.Partner = transport.Partner;
                    tB.LR_No = transport.LR_No;
                    tB.Description = transport.Description;
                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    tB.Partner = transport.Partner;
                    tB.LR_No = transport.LR_No;
                    tB.Description = transport.Description;
                    db.TransportTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteTransportDetails(int Id)
        {
            try
            {
                TransportTB tB = new TransportTB();
                var Checkbrand = db.TransportTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (Checkbrand.Id > 0)
                {
                    db.TransportTBs.Remove(Checkbrand);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
    }
}