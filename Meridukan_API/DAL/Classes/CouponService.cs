﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Coupon;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class CouponService : ICouponService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public bool AddorUpdateCouponDetails(CouponEntities coupon)
        {
            try
            {
                CouponTB tB = new CouponTB();

                if (coupon.Id > 0)
                {
                    var check = db.CouponTBs.Where(d => d.Id == coupon.Id).FirstOrDefault();
                    check.CouponCode = coupon.CouponCode;
                    check.DiscountTypeId = coupon.DiscountTypeId;
                    check.CouponAmt = coupon.CouponAmt;
                    check.IsfreeShipping = coupon.IsfreeShipping;
                    check.ExpiryDate = coupon.ExpiryDate;
                    check.MinimumSpend = coupon.MinimumSpend;
                    check.MaximumSpend = coupon.MaximumSpend;
                    db.Entry(check).State = EntityState.Modified;
                    db.SaveChanges();

                    var Incprd = db.CouponIncludeProductsTBs.Where(d => d.CouponId == coupon.Id).ToList();
                    foreach (var item in Incprd)
                    {
                        db.CouponIncludeProductsTBs.Remove(item);
                        db.SaveChanges();
                    }
                    CouponIncludeProductsTB incprd = new CouponIncludeProductsTB();
                    if(coupon.couponIncludeProductList != null)
                    {
                        foreach (var i in coupon.couponIncludeProductList)
                        {
                            var prd = db.Product_AttributeTB.Where(d => d.Id == i.PrdAttributeId).FirstOrDefault();
                            incprd.CouponId = check.Id;
                            incprd.PrdAttributeId = i.PrdAttributeId;
                            incprd.ProductId = prd.Id;
                            db.CouponIncludeProductsTBs.Add(incprd);
                            db.SaveChanges();
                        }
                    }
                    

                    var Excprd = db.CouponExcludeProductsTBs.Where(d => d.CouponId == coupon.Id).ToList();
                    foreach (var item in Excprd)
                    {
                        db.CouponExcludeProductsTBs.Remove(item);
                        db.SaveChanges();
                    }
                    CouponExcludeProductsTB excprd = new CouponExcludeProductsTB();
                    if(coupon.couponExcludeProductList != null)
                    {
                        foreach (var i in coupon.couponExcludeProductList)
                        {
                            var prd = db.Product_AttributeTB.Where(d => d.Id == i.PrdAttributeId).FirstOrDefault();
                            excprd.CouponId = check.Id;
                            excprd.PrdAttributeId = i.PrdAttributeId;
                            excprd.ProductId = prd.Id;
                            db.CouponExcludeProductsTBs.Add(excprd);
                            db.SaveChanges();
                        }
                    }
                    

                    var Inccat = db.CouponIncludeCategoryTBs.Where(d => d.CouponId == coupon.Id).ToList();
                    foreach (var item in Inccat)
                    {
                        db.CouponIncludeCategoryTBs.Remove(item);
                        db.SaveChanges();
                    }
                    CouponIncludeCategoryTB inccat = new CouponIncludeCategoryTB();
                    if(coupon.couponIncludeCategoryList != null)
                    {
                        foreach (var i in coupon.couponIncludeCategoryList)
                        {
                            inccat.CouponId = check.Id;
                            inccat.CategoryId = i.CategoryId;
                            db.CouponIncludeCategoryTBs.Add(inccat);
                            db.SaveChanges();
                        }
                    }
                    

                    var Exccat = db.CouponExcludeCategoryTBs.Where(d => d.CouponId == coupon.Id).ToList();
                    foreach (var item in Exccat)
                    {
                        db.CouponExcludeCategoryTBs.Remove(item);
                        db.SaveChanges();
                    }
                    CouponExcludeCategoryTB exccat = new CouponExcludeCategoryTB();
                    if(coupon.couponExcludeCategoryList != null)
                    {
                        foreach (var i in coupon.couponExcludeCategoryList)
                        {
                            exccat.CouponId = check.Id;
                            exccat.CategoryId = i.CategoryId;
                            db.CouponExcludeCategoryTBs.Add(exccat);
                            db.SaveChanges();
                        }
                    }                    
                }
                else
                {                    
                    tB.CouponCode = coupon.CouponCode;
                    tB.DiscountTypeId = coupon.DiscountTypeId;
                    tB.CouponAmt = coupon.CouponAmt;
                    tB.IsfreeShipping = coupon.IsfreeShipping;
                    tB.ExpiryDate = coupon.ExpiryDate;
                    tB.MinimumSpend = coupon.MinimumSpend;
                    tB.MaximumSpend = coupon.MaximumSpend;
                    db.CouponTBs.Add(tB);
                    db.SaveChanges();

                    CouponIncludeProductsTB incprd = new CouponIncludeProductsTB();
                    if(coupon.couponIncludeProductList != null)
                    {
                        foreach (var i in coupon.couponIncludeProductList)
                        {
                            var prd = db.Product_AttributeTB.Where(d => d.Id == i.PrdAttributeId).FirstOrDefault();
                            incprd.CouponId = tB.Id;
                            incprd.PrdAttributeId = i.PrdAttributeId;
                            incprd.ProductId = prd.Id;
                            db.CouponIncludeProductsTBs.Add(incprd);
                            db.SaveChanges();
                        }
                    }                    

                    CouponExcludeProductsTB excprd = new CouponExcludeProductsTB();
                    if(coupon.couponExcludeProductList != null)
                    {
                        foreach (var i in coupon.couponExcludeProductList)
                        {
                            var prd = db.Product_AttributeTB.Where(d => d.Id == i.PrdAttributeId).FirstOrDefault();
                            excprd.CouponId = tB.Id;
                            excprd.PrdAttributeId = i.PrdAttributeId;
                            excprd.ProductId = prd.Id;
                            db.CouponExcludeProductsTBs.Add(excprd);
                            db.SaveChanges();
                        }
                    }                    

                    CouponIncludeCategoryTB inccat = new CouponIncludeCategoryTB();
                    if(coupon.couponIncludeCategoryList != null)
                    {
                        foreach (var i in coupon.couponIncludeCategoryList)
                        {
                            inccat.CouponId = tB.Id;
                            inccat.CategoryId = i.CategoryId;
                            db.CouponIncludeCategoryTBs.Add(inccat);
                            db.SaveChanges();
                        }
                    }                    

                    CouponExcludeCategoryTB exccat = new CouponExcludeCategoryTB();
                    if(coupon.couponExcludeCategoryList != null)
                    {
                        foreach (var i in coupon.couponExcludeCategoryList)
                        {
                            exccat.CouponId = tB.Id;
                            exccat.CategoryId = i.CategoryId;
                            db.CouponExcludeCategoryTBs.Add(exccat);
                            db.SaveChanges();
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteCouponDetails(int Id)
        {
            try
            {
                CouponTB tB = new CouponTB();

                var check = db.CouponTBs.Where(d => d.Id == Id).FirstOrDefault();
                if(check != null)
                {
                    var Incprd = db.CouponIncludeProductsTBs.Where(d => d.CouponId == check.Id).ToList();
                    foreach (var item in Incprd)
                    {
                        db.CouponIncludeProductsTBs.Remove(item);
                       // db.SaveChanges();
                    }                   

                    var Excprd = db.CouponExcludeProductsTBs.Where(d => d.CouponId == check.Id).ToList();
                    foreach (var item in Excprd)
                    {
                        db.CouponExcludeProductsTBs.Remove(item);
                       // db.SaveChanges();
                    }
                    

                    var Inccat = db.CouponIncludeCategoryTBs.Where(d => d.CouponId == check.Id).ToList();
                    foreach (var item in Inccat)
                    {
                        db.CouponIncludeCategoryTBs.Remove(item);
                       // db.SaveChanges();
                    }
                    

                    var Exccat = db.CouponExcludeCategoryTBs.Where(d => d.CouponId == check.Id).ToList();
                    foreach (var item in Exccat)
                    {
                        db.CouponExcludeCategoryTBs.Remove(item);
                        //db.SaveChanges();
                    }

                    db.CouponTBs.Remove(check);
                    db.SaveChanges();

                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public CouponEntities GetCouponDetails(int Id)
        {
            try
            {
                CouponEntities coupon = new CouponEntities();

                var data = db.CouponTBs.Where(d => d.Id == Id).FirstOrDefault();

                coupon.Id = data.Id;
                coupon.CouponCode = data.CouponCode;
                coupon.CouponAmt = Convert.ToDecimal(data.CouponAmt);
                coupon.DiscountTypeId = Convert.ToInt32(data.DiscountTypeId);
                var discounttype = db.DiscountTypeTBs.Where(d => d.Id == coupon.DiscountTypeId).FirstOrDefault();
                coupon.DiscountType = discounttype.DiscountType;
                coupon.ExpiryDate = Convert.ToDateTime(data.ExpiryDate);
                coupon.IsfreeShipping = Convert.ToBoolean(data.IsfreeShipping);
                coupon.MinimumSpend = Convert.ToDecimal(data.MinimumSpend);
                coupon.MaximumSpend = Convert.ToDecimal(data.MaximumSpend);

                coupon.couponIncludeProductList = new List<CouponProductsEnitites>();
                
                var incprd = db.CouponIncludeProductsTBs.Where(d => d.CouponId == data.Id).ToList();

                foreach (var item in incprd)
                {
                    CouponProductsEnitites inprd = new CouponProductsEnitites();
                    inprd.Id = item.Id;
                    inprd.CouponId = item.Id;
                    inprd.ProductId = Convert.ToInt32(item.ProductId);
                    inprd.PrdAttributeId = Convert.ToInt32(item.PrdAttributeId);
                    coupon.couponIncludeProductList.Add(inprd);
                }

                coupon.couponExcludeProductList = new List<CouponProductsEnitites>();
                var excprd = db.CouponExcludeProductsTBs.Where(d => d.CouponId == data.Id).ToList();

                foreach (var item in excprd)
                {
                    CouponProductsEnitites inprd = new CouponProductsEnitites();
                    inprd.Id = item.Id;
                    inprd.CouponId = item.Id;
                    inprd.ProductId = Convert.ToInt32(item.ProductId);
                    inprd.PrdAttributeId = Convert.ToInt32(item.PrdAttributeId);
                    coupon.couponIncludeProductList.Add(inprd);
                }

                coupon.couponIncludeCategoryList = new List<CouponCategoryEntities>();
                var inccat = db.CouponIncludeCategoryTBs.Where(d => d.CouponId == data.Id).ToList();

                foreach (var item in inccat)
                {
                    CouponCategoryEntities inprd = new CouponCategoryEntities();
                    inprd.Id = item.Id;
                    inprd.CouponId = item.Id;
                    inprd.CategoryId = Convert.ToInt32(item.CategoryId);                    
                    coupon.couponIncludeCategoryList.Add(inprd);
                }

                coupon.couponExcludeCategoryList = new List<CouponCategoryEntities>();
                var exccat = db.CouponExcludeCategoryTBs.Where(d => d.CouponId == data.Id).ToList();

                foreach (var item in exccat)
                {
                    CouponCategoryEntities inprd = new CouponCategoryEntities();
                    inprd.Id = item.Id;
                    inprd.CouponId = item.Id;
                    inprd.CategoryId = Convert.ToInt32(item.CategoryId);
                    coupon.couponIncludeCategoryList.Add(inprd);
                }

                return coupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CouponEntities> GetCouponList()
        {            
            List<CouponEntities> List = new List<CouponEntities>();
            try
            {
                var list = db.CouponTBs.OrderByDescending(d => d.Id).ToList();

                foreach (var item in list)
                {
                    CouponEntities coupon = new CouponEntities();
                    coupon.Id = item.Id;
                    coupon.CouponCode = item.CouponCode;
                    coupon.CouponAmt = Convert.ToDecimal(item.CouponAmt);
                    coupon.DiscountTypeId = Convert.ToInt32(item.DiscountTypeId);
                    var discounttype = db.DiscountTypeTBs.Where(d => d.Id == coupon.DiscountTypeId).FirstOrDefault();
                    coupon.DiscountType = discounttype.DiscountType;
                    coupon.ExpiryDate = Convert.ToDateTime(item.ExpiryDate);
                    coupon.IsfreeShipping = Convert.ToBoolean(item.IsfreeShipping);
                    coupon.MinimumSpend = Convert.ToDecimal(item.MinimumSpend);
                    coupon.MaximumSpend = Convert.ToDecimal(item.MaximumSpend);
                    List.Add(coupon);
                }

                return List;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}