﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Supplier;
using Meridukan_API.Models;

namespace Meridukan_API.DAL.Classes
{
    public class SupplierService : ISupplierService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        public List<SupplierEntities> GetSupplierList()
        {
            List<SupplierEntities> supplierList = new List<SupplierEntities>();
            try
            {
                var list = db.SupplierTBs.OrderBy(d => d.FirstName).ToList();

                foreach (var item in list)
                {
                    SupplierEntities supplier = new SupplierEntities();
                    supplier.Id = item.Id;
                    supplier.Name = item.FirstName + ' ' + item.MiddleName + ' ' + item.LastName;
                    supplier.FirstName = item.FirstName;
                    supplier.MiddleName = item.MiddleName;
                    supplier.LastName = item.LastName;
                    supplier.Email = item.EmailId;
                    supplier.Mobile = item.Phone;
                    supplier.Address = item.Address;
                    supplier.GST = item.GST;
                    supplierList.Add(supplier);
                }

                return supplierList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SupplierEntities GetSupplierDetails(int SupplierId)
        {
            try
            {
                SupplierEntities supplier = new SupplierEntities();

                var item = db.SupplierTBs.Where(d => d.Id == SupplierId).FirstOrDefault();

                supplier.Id = item.Id;
                supplier.Name = item.FirstName + ' ' + item.LastName;
                supplier.FirstName = item.FirstName;
                supplier.MiddleName = item.MiddleName;
                supplier.LastName = item.LastName;
                supplier.Email = item.EmailId;
                supplier.Mobile = item.Phone;
                supplier.Address = item.Address;
                supplier.GST = item.GST;

                return supplier;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdateSupplierDetails(SupplierEntities supplier)
        {
            try
            {
                SupplierTB tB = new SupplierTB();

                if (supplier.Id > 0)
                {
                    tB.Id = supplier.Id;
                    tB.FirstName =  supplier.FirstName;
                    tB.MiddleName = supplier.MiddleName;
                    tB.LastName = supplier.LastName;
                    tB.EmailId = supplier.Email;
                    tB.Phone = supplier.Mobile;
                    tB.Address = supplier.Address;
                    tB.GST = supplier.GST;
                    db.Entry(tB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                   // tB.Id = supplier.Id;
                    tB.FirstName = supplier.FirstName;
                    tB.MiddleName = supplier.MiddleName;
                    tB.LastName = supplier.LastName;
                    tB.EmailId = supplier.Email;
                    tB.Phone = supplier.Mobile;
                    tB.Address = supplier.Address;
                    tB.GST = supplier.GST;
                    db.SupplierTBs.Add(tB);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeleteSupplierDetails(int Id)
        {
            try
            {
                SupplierTB tB = new SupplierTB();
                var CheckSupplier = db.SupplierTBs.Where(d => d.Id == Id).FirstOrDefault();
                if (CheckSupplier.Id > 0)
                {
                    db.SupplierTBs.Remove(CheckSupplier);
                    db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
    }
}