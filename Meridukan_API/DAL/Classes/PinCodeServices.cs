﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.PinCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Meridukan_API.DAL.Classes
{
    public class PinCodeServices : IPinCodeServices
    {
        public List<PinCodeEntities> GetPinCodeDetail(string PinCode)
        {
            List<PinCodeEntities> pinCodes = new List<PinCodeEntities>();

            try
            {
                if (PinCode != null)
                {
                    WebClient client = new WebClient();
                    client.Headers["Content-type"] = "application/json";
                    client.Encoding = Encoding.UTF8;
                    string json = client.DownloadString("https://api.postalpincode.in/pincode/" + PinCode);

                    List<PinCodeJsonEntity>
                    pinCodeJsonEntities = (new JavaScriptSerializer()).Deserialize<List<PinCodeJsonEntity>>(json);

                    string statename = null; string cityname = null;string countryname = null;                   

                    if (pinCodeJsonEntities.FirstOrDefault().PostOffice != null)
                    {
                        foreach (var item in pinCodeJsonEntities)
                        {
                            foreach (var items in item.PostOffice)
                            {
                                PinCodeEntities pinCode = new PinCodeEntities();
                                pinCode.PinCode = PinCode;
                                pinCode.State = items.State;
                                pinCode.City = items.District;
                                pinCode.Country = items.Country;
                                pinCode.Block = items.Block;
                                pinCode.Name = items.Name;
                                pinCodes.Add(pinCode);
                            }
                        }
                    }
                }

                return pinCodes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}