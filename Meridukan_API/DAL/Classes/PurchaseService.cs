﻿using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Purchase;
using Meridukan_API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Classes
{
    public class PurchaseService : IPurchaseService
    {
        MeriDukaanDBEntities db = new MeriDukaanDBEntities();

        DbConnection conn = null;
        public PurchaseService()
        {
            conn = new DbConnection();
        }

        public List<PurchaseEnitities> GetPurchaseList()
        {
            List<PurchaseEnitities> purchaseList = new List<PurchaseEnitities>();
            
            try
            {

                SqlConnection con = conn.OpenDbConnection();
                SqlCommand cmd = new SqlCommand("GetPurchaseList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    PurchaseEnitities purchase = new PurchaseEnitities();
                    purchase.PoId = Convert.ToInt32(dr["PoId"]);
                    purchase.PoDate = Convert.ToDateTime(dr["PoDate"]);                    
                    purchase.PoNo = Convert.ToString(dr["PoNo"]);
                    purchase.SupplierName = Convert.ToString(dr["SupplierName"]);
                    purchase.Total = Convert.ToDecimal(dr["Total"]);
                    purchase.TaxAmount = Convert.ToDecimal(dr["TaxAmount"]);
                    purchase.GrandTotal = Convert.ToDecimal(dr["GrandTotal"]);
                    
                    purchaseList.Add(purchase);
                }

                con.Close();

                //var list = (from d in db.PurchaseTBs
                //            join s in db.SupplierTBs on d.SupplierId equals s.Id
                //            select new
                //            {
                //                d.PoId,
                //                d.PoDate,
                //                d.PoNo,
                //                d.SupplierId,
                //                Name = s.FirstName + ' ' + s.MiddleName + ' ' + s.LastName,
                //                d.Total,
                //                d.TaxAmount,
                //                d.GrandTotal,
                //                d.PendAmount,
                //                d.RecAmount
                //            }).OrderByDescending(d => d.PoDate).ToList();

                //foreach (var item in list)
                //{
                //    PurchaseEnitities purchase = new PurchaseEnitities();
                //    purchase.PoId = item.PoId;
                //    purchase.PoDate = Convert.ToDateTime(item.PoDate);
                //    purchase.PoNo = item.PoNo;
                //    purchase.SupplierId = item.SupplierId;
                //    purchase.SupplierName = item.Name;
                //    purchase.Total = Convert.ToDecimal(item.Total);
                //    purchase.TaxAmount = Convert.ToDecimal(item.TaxAmount);
                //    purchase.GrandTotal = Convert.ToDecimal(item.GrandTotal);
                //    purchase.PendAmount = Convert.ToDecimal(item.PendAmount);
                //    purchase.RecAmount = Convert.ToDecimal(item.RecAmount);
                //    purchaseList.Add(purchase);
                //}

                return purchaseList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PurchaseEnitities GetPurchaseDetails(int PoId)
        {
            PurchaseEnitities purchase = new PurchaseEnitities();
            purchase.purchaseDetails = new List<PurchaseDetailEnitities>();

            try
            {

                SqlConnection con = conn.OpenDbConnection();
                SqlCommand cmd = new SqlCommand("GetPurchaseDetail", con);
                cmd.Parameters.Add("@PoId", SqlDbType.Int).Value = PoId;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {  
                    purchase.PoId = Convert.ToInt32(dr["PoId"]);
                    purchase.PoDate = Convert.ToDateTime(dr["PoDate"]);
                    purchase.Date = Convert.ToString(dr["PoDate"]);
                    purchase.PoNo = Convert.ToString(dr["PoNo"]);
                    purchase.SupplierId = Convert.ToInt32(dr["SupplierId"]);
                    purchase.SupplierName = Convert.ToString(dr["SupplierName"]);
                    purchase.SupplierInvNo = Convert.ToString(dr["SupplierInvNo"]);
                    purchase.SupplierInvAmt = Convert.ToDecimal(dr["SupplierInvAmt"]);
                    purchase.SupplierInvDate = Convert.ToDateTime(dr["SupplierInvDate"]);
                    purchase.SupInvDate = Convert.ToString(dr["SupplierInvDate"]);
                    purchase.City = Convert.ToString(dr["City"]);
                    purchase.TransportDetailId = Convert.ToInt32(dr["TransportId"]);
                    purchase.LR_No = Convert.ToString(dr["LR_No"]);
                    purchase.LR_Date = Convert.ToDateTime(dr["LR_Date"]);
                    purchase.LRDate = Convert.ToString(dr["LR_Date"]);
                    purchase.No_of_Packages = Convert.ToString(dr["No_of_Packages"]);
                    purchase.Total = Convert.ToDecimal(dr["Total"]);
                    purchase.TaxAmount = Convert.ToDecimal(dr["TaxAmount"]);
                    purchase.GrandTotal = Convert.ToDecimal(dr["GrandTotal"]);
                    purchase.UserId = Convert.ToInt32(dr["UserId"]);                    
                }


                cmd = new SqlCommand("GetPurchaseItemDetail", con);
                cmd.Parameters.Add("@PoId", SqlDbType.Int).Value = PoId;
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    PurchaseDetailEnitities enitities = new PurchaseDetailEnitities();

                    enitities.PoDetId = Convert.ToInt32(dr["PoDetId"]);
                    enitities.PoId = Convert.ToInt32(dr["PoId"]);
                    enitities.PoNo = Convert.ToString(dr["PoNo"]);
                    enitities.CategoryId = Convert.ToInt32(dr["CategoryId"]);
                    enitities.CategoryName = Convert.ToString(dr["CategoryName"]);
                    enitities.SubcategoryId = Convert.ToInt32(dr["SubcategoryId"]);
                    enitities.SubCategoryName = Convert.ToString(dr["SubCategoryName"]);
                    enitities.BrandId = Convert.ToInt32(dr["BrandId"]);
                    enitities.BrandName = Convert.ToString(dr["BrandName"]);
                    enitities.ProductId = Convert.ToInt32(dr["ProductId"]);
                    enitities.ProductName = Convert.ToString(dr["ProductName"]);
                    enitities.Unit = Convert.ToString(dr["Unit"]);

                    if(dr["SizeId"] != null)
                    {
                        enitities.SizeId = Convert.ToInt32(dr["SizeId"]);
                        enitities.Size = Convert.ToString(dr["Size"]);
                    }
                    else
                    {
                        enitities.SizeId = 0;
                        enitities.Size = "";
                    }

                    if (dr["ColorId"] != null)
                    {
                        enitities.ColorId = Convert.ToInt32(dr["ColorId"]);
                        enitities.Color = Convert.ToString(dr["Color"]);
                    }
                    else
                    {
                        enitities.ColorId = 0;
                        enitities.Color = "";
                    }
                    
                    enitities.Qty = Convert.ToString(dr["Qty"]);
                    enitities.Rate = Convert.ToDecimal(dr["PurchaseRate"]);
                    enitities.TaxId = Convert.ToInt32(dr["TaxId"]);                    
                    enitities.TaxName = Convert.ToString(dr["TaxName"]);
                    enitities.TaxAmount = Convert.ToDecimal(dr["TaxAmount"]);
                    enitities.Total = Convert.ToDecimal(dr["Total"]);
                    enitities.GrandTotal = Convert.ToDecimal(dr["GrandAmount"]);
                    purchase.purchaseDetails.Add(enitities);
                }


                con.Close();

                

                return purchase;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PurchaseEnitities GetPurchaseDetailsForGRN(int PoId)
        {
            PurchaseEnitities purchase = new PurchaseEnitities();
            purchase.purchaseDetails = new List<PurchaseDetailEnitities>();

            try
            {

                SqlConnection con = conn.OpenDbConnection();
                SqlCommand cmd = new SqlCommand("GetPurchaseDetail", con);
                cmd.Parameters.Add("@PoId", SqlDbType.Int).Value = PoId;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    purchase.PoId = Convert.ToInt32(dr["PoId"]);
                    purchase.PoDate = Convert.ToDateTime(dr["PoDate"]);
                    purchase.Date = Convert.ToString(dr["PoDate"]);
                    purchase.PoNo = Convert.ToString(dr["PoNo"]);
                    purchase.SupplierId = Convert.ToInt32(dr["SupplierId"]);
                    purchase.SupplierName = Convert.ToString(dr["SupplierName"]);
                    purchase.SupplierInvNo = Convert.ToString(dr["SupplierInvNo"]);
                    purchase.SupplierInvAmt = Convert.ToDecimal(dr["SupplierInvAmt"]);
                    purchase.SupplierInvDate = Convert.ToDateTime(dr["SupplierInvDate"]);
                    purchase.SupInvDate = Convert.ToString(dr["SupplierInvDate"]);
                    purchase.City = Convert.ToString(dr["City"]);
                    purchase.TransportDetailId = Convert.ToInt32(dr["TransportId"]);
                    purchase.LR_No = Convert.ToString(dr["LR_No"]);
                    purchase.LR_Date = Convert.ToDateTime(dr["LR_Date"]);
                    purchase.LRDate = Convert.ToString(dr["LR_Date"]);
                    purchase.No_of_Packages = Convert.ToString(dr["No_of_Packages"]);
                    purchase.Total = Convert.ToDecimal(dr["Total"]);
                    purchase.TaxAmount = Convert.ToDecimal(dr["TaxAmount"]);
                    purchase.GrandTotal = Convert.ToDecimal(dr["GrandTotal"]);
                    purchase.UserId = Convert.ToInt32(dr["UserId"]);
                }


                cmd = new SqlCommand("GetPurchaseItemDetail", con);
                cmd.Parameters.Add("@PoId", SqlDbType.Int).Value = PoId;
                cmd.CommandType = CommandType.StoredProcedure;
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    PurchaseDetailEnitities enitities = new PurchaseDetailEnitities();

                    enitities.PoDetId = Convert.ToInt32(dr["PoDetId"]);
                    enitities.PoId = Convert.ToInt32(dr["PoId"]);
                    enitities.PoNo = Convert.ToString(dr["PoNo"]);
                    enitities.CategoryId = Convert.ToInt32(dr["CategoryId"]);
                    enitities.CategoryName = Convert.ToString(dr["CategoryName"]);
                    enitities.SubcategoryId = Convert.ToInt32(dr["SubcategoryId"]);
                    enitities.SubCategoryName = Convert.ToString(dr["SubCategoryName"]);
                    enitities.BrandId = Convert.ToInt32(dr["BrandId"]);
                    enitities.BrandName = Convert.ToString(dr["BrandName"]);
                    enitities.ProductId = Convert.ToInt32(dr["ProductId"]);
                    enitities.ProductName = Convert.ToString(dr["ProductName"]);
                    enitities.Unit = Convert.ToString(dr["Unit"]);

                    if (dr["SizeId"] != null)
                    {
                        enitities.SizeId = Convert.ToInt32(dr["SizeId"]);
                        enitities.Size = Convert.ToString(dr["Size"]);
                    }
                    else
                    {
                        enitities.SizeId = 0;
                        enitities.Size = "";
                    }

                    if (dr["ColorId"] != null)
                    {
                        enitities.ColorId = Convert.ToInt32(dr["ColorId"]);
                        enitities.Color = Convert.ToString(dr["Color"]);
                    }
                    else
                    {
                        enitities.ColorId = 0;
                        enitities.Color = "";
                    }

                    enitities.PurchaseQty = Convert.ToString(dr["Qty"]);
                    enitities.PurchaseRate = Convert.ToDecimal(dr["PurchaseRate"]);
                    enitities.TaxId = Convert.ToInt32(dr["TaxId"]);
                    enitities.TaxName = Convert.ToString(dr["TaxName"]);
                    enitities.TaxAmount = Convert.ToDecimal(dr["TaxAmount"]);
                    enitities.Total = Convert.ToDecimal(dr["Total"]);
                    enitities.GrandTotal = Convert.ToDecimal(dr["GrandAmount"]);
                    purchase.purchaseDetails.Add(enitities);
                }


                con.Close();



                return purchase;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddorUpdatePO(PurchaseEnitities purchase)
        {
            try
            {
                PurchaseTB ut = new PurchaseTB();

                if (purchase.PoId == 0)
                {

                    var prd = db.PurchaseTBs.OrderByDescending(d => d.PoId).FirstOrDefault();
                    string uniqueId = "";

                    if (prd != null)
                    {
                        uniqueId = Convert.ToString(prd.PoId);
                        int newNumber = Convert.ToInt32(uniqueId) + 1;
                        uniqueId = "PO-" + newNumber.ToString("000000");
                    }
                    else
                    {
                        uniqueId = "000000";
                        int newNumber = Convert.ToInt32(uniqueId) + 1;
                        uniqueId = "PO-" + newNumber.ToString("000000");
                    }

                   // ut.PoId = purchase.PoId;
                    ut.PoNo = uniqueId;
                    ut.PoDate = purchase.PoDate;
                    ut.SupplierId = purchase.SupplierId;
                    ut.SupplierInvNo = purchase.SupplierInvNo;
                    ut.SupplierInvAmt = purchase.SupplierInvAmt;
                    ut.SupplierInvDate = purchase.SupplierInvDate;
                    ut.TransportId = purchase.TransportDetailId;
                    ut.LR_No = purchase.LR_No;
                    ut.LR_Date = purchase.LR_Date;
                    ut.City = purchase.City;
                    ut.No_of_Packages = purchase.No_of_Packages;
                    ut.EnterBy = purchase.UserId;
                    ut.EnterDate = DateTime.Now;
                    ut.Total = purchase.Total;
                    ut.TaxAmount = purchase.TaxAmount;
                    ut.GrandTotal = purchase.GrandTotal;
                    ut.UserId = purchase.UserId;

                    db.PurchaseTBs.Add(ut);
                    db.SaveChanges();

                    foreach (var item in purchase.purchaseDetails)
                    {
                        PurchaseDetailsTB tB = new PurchaseDetailsTB();
                        tB.PoId = ut.PoId;
                        tB.PoNo = ut.PoNo;
                        tB.CategoryId = item.CategoryId;
                        tB.SubcategoryId = item.SubcategoryId;
                        tB.BrandId = item.BrandId;
                        tB.ProductId = item.ProductId;
                        tB.SizeId = item.SizeId;
                        tB.ColorId = item.ColorId;
                        tB.Unit = item.Unit;
                        tB.Qty = item.Qty;
                        tB.PurchaseRate = item.Rate;
                        tB.TaxId = item.TaxId;
                        tB.TaxAmount = item.TaxAmount;
                        tB.Total = item.Total;
                        tB.GrandAmount = item.GrandTotal;
                        db.PurchaseDetailsTBs.Add(tB);
                        db.SaveChanges();
                    }
                }
                else
                { 

                    ut.PoId = purchase.PoId;
                    ut.PoNo = purchase.PoNo;
                    ut.PoDate = purchase.PoDate;
                    ut.SupplierId = purchase.SupplierId;
                    ut.SupplierInvNo = purchase.SupplierInvNo;
                    ut.SupplierInvAmt = purchase.SupplierInvAmt;
                    ut.SupplierInvDate = purchase.SupplierInvDate;
                    ut.City = purchase.City;
                    ut.TransportId = purchase.TransportDetailId;
                    ut.LR_No = purchase.LR_No;
                    ut.LR_Date = purchase.LR_Date;
                    ut.No_of_Packages = purchase.No_of_Packages;
                    ut.Total = purchase.Total;
                    ut.TaxAmount = purchase.TaxAmount;
                    ut.GrandTotal = purchase.GrandTotal;
                    ut.UserId = purchase.UserId;

                    db.Entry(ut).State = EntityState.Modified;
                    db.SaveChanges();

                    var Check = db.PurchaseDetailsTBs.Where(d => d.PoId == ut.PoId).ToList();
                    foreach (var item in Check)
                    {
                        db.PurchaseDetailsTBs.Remove(item);
                        db.SaveChanges();
                    }

                    foreach (var item in purchase.purchaseDetails)
                    {
                        PurchaseDetailsTB tB = new PurchaseDetailsTB();
                        tB.PoId = ut.PoId;
                        tB.PoNo = ut.PoNo;
                        tB.CategoryId = item.CategoryId;
                        tB.SubcategoryId = item.SubcategoryId;
                        tB.BrandId = item.BrandId;
                        tB.ProductId = item.ProductId;
                        tB.SizeId = item.SizeId;
                        tB.ColorId = item.ColorId;
                        tB.Unit = item.Unit;
                        tB.Qty = item.Qty;
                        tB.PurchaseRate = item.Rate;
                        tB.TaxId = item.TaxId;
                        tB.TaxAmount = item.TaxAmount;
                        tB.Total = item.Total;
                        tB.GrandAmount = item.GrandTotal;
                        db.PurchaseDetailsTBs.Add(tB);
                        db.SaveChanges();
                    }
                }
                return true;

            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool DeletePoDetails(int PoId)
        {

            try
            {
                PurchaseTB tB = new PurchaseTB();

               // var Checkpoitems = db.PurchaseDetailsTBs.Where(d => d.PoId == PoId).ToList();

                var CheckPo = db.PurchaseTBs.Where(d => d.PoId == PoId).FirstOrDefault();
                if (CheckPo.PoId > 0)
                {
                    CheckPo.IsActive = false;
                    db.Entry(CheckPo).State = EntityState.Modified;
                    db.SaveChanges();

                    //foreach (var item in Checkpoitems)
                    //{
                    //    db.PurchaseDetailsTBs.Remove(item);
                    //    db.SaveChanges();
                    //}

                    //db.PurchaseTBs.Remove(CheckPo);
                    //db.SaveChanges();
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }       
    }
}