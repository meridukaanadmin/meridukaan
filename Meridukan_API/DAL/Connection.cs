﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL
{
    public class DbConnection
    {
        public readonly string connectionString = string.Empty;

        public DbConnection()
        {
            connectionString = ConfigurationManager.ConnectionStrings["MeriDukaanDBConn"].ToString();
        }

        public SqlConnection OpenDbConnection()
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            return con;
        }

    }
}