﻿using Meridukan_API.Entities.Coupon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
    public interface IDiscountTypeService
    {
        List<DiscountType> GetDiscountTypeList();

        DiscountType GetDiscountTypeDetails(int Id);

        bool AddorUpdateDiscountType (DiscountType discount);

        bool DeleteDiscountTypeDetails(int Id);
    }
}
