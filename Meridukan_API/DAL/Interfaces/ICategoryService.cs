﻿using Meridukan_API.Entities.Category;
using System.Collections.Generic;

namespace Meridukan_API.DAL.Interfaces
{
    public interface ICategoryService
    {
        List<CategoryEntities> GetCategory();

        CategoryEntities GetCategoryDetails(int Id);

        bool AddorUpdateCategoryDetails(CategoryEntities brand);

        bool DeleteCategoryDetails(long Id);
    }
}
