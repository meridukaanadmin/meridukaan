﻿using Meridukan_API.Entities.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
    public interface IRoleService
    {
        List<RoleEntities> GetRoleList();

        RoleEntities GetRoleDetails(int Id);

        bool AddorUpdateRoleDetails(RoleEntities role);

        bool DeleteRoleDetails(int Id);
    }
}
