﻿using Meridukan_API.Entities.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
    public interface ITaxService
    {
        List<TaxEntities> GetTaxList();

        TaxEntities GetTaxDetails(int Id);

        bool AddorUpdateTaxDetails(TaxEntities Tax);

        bool DeleteTaxDetails(int Id);

    }
}
