﻿using Meridukan_API.Entities.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
   public interface IPaymentService
    {
        List<PaymentEntities> GetPayments(string Contact);

        PaymentEntities GetPaymentByOrderId(int OrderId);

        bool AddPayment(PaymentEntities payment);
    }
}
