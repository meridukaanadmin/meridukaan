﻿using Meridukan_API.Entities.OrderStatus;
using System.Collections.Generic;

namespace Meridukan_API.DAL.Interfaces
{
    public interface IOrderStatusService
    {
        List<OrderStatusEntities> getOrderStatusList();
    }
}
