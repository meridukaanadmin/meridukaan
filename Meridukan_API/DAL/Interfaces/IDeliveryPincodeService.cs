﻿using Meridukan_API.Entities.DeliveryPinCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
    public interface IDeliveryPincodeService
    {
        List<DeliveryPinCodeEntities> GetDeliveryPinCodeEntities();

        DeliveryPinCodeEntities GetDeliveryPinCodeDetails(int Id);

        bool AddorUpdateDeliveryPinCodeDetails(DeliveryPinCodeEntities delivery);

        bool DeleteDeliveryPinCodeDetails(int Id);
    }
}
