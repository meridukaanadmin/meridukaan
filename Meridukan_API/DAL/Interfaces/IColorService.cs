﻿using Meridukan_API.Entities.Color;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
    public interface IColorService
    {
        List<ColorEntities> GetColorList();

        ColorEntities GetColorDetails(int Id);

        bool AddorUpdateColorDetails(ColorEntities color);

        bool DeleteColorDetails(int Id);
    }
}
