﻿using Meridukan_API.Entities.Purchase;
using System.Collections.Generic;

namespace Meridukan_API.DAL.Interfaces
{
    public interface IPurchaseService
    {
        List<PurchaseEnitities> GetPurchaseList();

        PurchaseEnitities GetPurchaseDetails(int PoId);

        PurchaseEnitities GetPurchaseDetailsForGRN(int PoId);

        bool AddorUpdatePO(PurchaseEnitities purchase);

        bool DeletePoDetails(int PoId);
    }
}
