﻿using Meridukan_API.Entities.PinCode;
using System.Collections.Generic;

namespace Meridukan_API.DAL.Interfaces
{
    public interface IPinCodeServices
    {
        List<PinCodeEntities> GetPinCodeDetail(string PinCode);
    }
}
