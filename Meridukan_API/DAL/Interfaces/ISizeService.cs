﻿using Meridukan_API.Entities.Size;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
    public interface ISizeService
    {
        List<SizeEntities> GetSizeList();

        SizeEntities GetSizeDetails(int Id);

        bool AddorUpdateSize(SizeEntities size);

        bool DeleteSizeDetails(int Id);
    }
}
