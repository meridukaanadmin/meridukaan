﻿using Meridukan_API.Entities.Dashboard;

namespace Meridukan_API.DAL.Interfaces
{
    public interface IDashboardService
    {
        DashboardEntities GetDashboardData();
    }
}
