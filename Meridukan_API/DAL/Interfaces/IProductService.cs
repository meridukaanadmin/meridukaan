﻿using Meridukan_API.Entities.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
    public interface IProductService
    {
        //Product Type
        List<ProductTypeEntities> GetProductTypeList();
        ProductTypeEntities GetProductTypeDetails(int Id); 
        bool AddorUpdateProductTypeDetails(ProductTypeEntities ProductType); 
        bool DeleteProductTypeDetails(int Id);        

        // Product Attribute
        List<ProductAttributeEntities> GetProductAttributeList();
        ProductAttributeEntities GetProductAttributeDetails(int Id);
        bool AddorUpdateProductAttributeDetails(ProductAttributeEntities ProductAttribute);
        bool DeleteProductAttributeDetails(int Id);

        // Product Master
        object GetProductList();
        AddProductModel GetProductDetails(int Id);
        bool AddorUpdateProductDetails(AddProductModel Product);
        bool DeleteProductDetails(int Id);

        //GRN List
        List<ProductGRNListEntities> GetProductGRNList(int ProductId);

        //Product List in sale page
        List<SaleProductAttributes> GetSaleProductList();

        //inactive products
        bool DeleteProduct(int Id);

        //for Front End Product list
        List<ProductEntities> GetProducts(int CategoryId);
    }
}
