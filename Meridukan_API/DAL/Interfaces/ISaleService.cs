﻿using Meridukan_API.Entities.Sale;
using Meridukan_API.Entities.Size;
using System.Collections.Generic;

namespace Meridukan_API.DAL.Interfaces
{
    public interface ISaleService
    {
        List<SaleEntities> GetSaleList();

        SaleEntities GetSaleDetails(int SaleId);

        SaleRateEntities GetSaleRate(string Barcode);

        long? AddorUpdateSale(SaleEntities sale);

        bool DeleteSaleDetails(int SaleId);

        bool UpdateOrderStatus(int SaleId, int OrderstatusId);

        List<AutocompleteSearchEntities> GetProductSearchData();
    }
}
