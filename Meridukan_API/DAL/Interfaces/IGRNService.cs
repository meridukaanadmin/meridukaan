﻿using Meridukan_API.Entities.GRN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
    public interface IGRNService
    {
        object GetGRNList();

        GRNEntities GetGRNDetails(int GrnId);

        GRNEntities GetGRNDetailsNew(int GrnId);

        bool AddorUpdateGRN(GRNEntities purchase);

        bool DeleteGRNDetails(int GRNId);
    }
}
