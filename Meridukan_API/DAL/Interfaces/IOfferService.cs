﻿using Meridukan_API.Entities.Offer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
    public interface IOfferService
    {
        List<OfferEnitities> GetOfferList();

        OfferEnitities GetOfferDetails(int Id);

        bool AddorUpdateOffer(OfferEnitities offer);

        bool DeleteOffer(int OfferId);
    }
}
