﻿using Meridukan_API.Entities.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
    public interface ICartService
    {
        bool SaveCartAtSuspend(CartEntities cart);

        bool DeleteCartByCustomer(string Contact);

        List<CustomerCartDetailsEntities> GetCartDetailsByCustomer(string contact);

        bool SaveCustomerCart(CartEntities cart);

        bool DeleteCustomerCartItems(DeleteCustomerCartItemsEntities cart);
    }
}
