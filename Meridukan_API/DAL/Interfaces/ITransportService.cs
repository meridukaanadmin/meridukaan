﻿using Meridukan_API.Entities.Transport;
using System.Collections.Generic;

namespace Meridukan_API.DAL.Interfaces
{
    public interface ITransportService
    {
        List<TransportEntities> GetTransportDetailList();

        TransportEntities GetTransportDetails(int Id);

        bool AddorUpdateTransportDetails(TransportEntities transport);

        bool DeleteTransportDetails(int Id);
    }
}
