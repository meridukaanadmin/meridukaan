﻿using Meridukan_API.Entities.Customer;
using System.Collections.Generic;

namespace Meridukan_API.DAL.Interfaces
{
    public interface ICustomerService
    {
        List<CustomerEntities> GetCustomerList();

        CustomerEntities GetCustomerDetails(int Id);

        bool AddorUpdateCustomerDetails(CustomerEntities customer);

        bool DeleteCustomerDetails(string Contact);

        List<CustomerOrderEntities> GetCustomerOrdersList(string Contact);

        //address part
        List<CustomerAddressEntities> GetCustomerAddress(string Contact);

        bool AddorUpdateCustomerAddressDetails(CustomerAddressEntities customer);

        //report part
        List<CustomerReportEntities> GetCustomerReportList();
    }
}
