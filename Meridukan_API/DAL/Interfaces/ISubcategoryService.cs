﻿using Meridukan_API.Entities.Subcategory;
using System.Collections.Generic;

namespace Meridukan_API.DAL.Interfaces
{
    public interface ISubcategoryService
    {
        List<SubcategoryEntities> GetSubcategoryList();

        List<SubcategoryEntities> GetSubcategoryByCategoryId(int CategoryId);

        SubcategoryEntities GetSubcategoryDetails(int Id);

        bool AddorUpdateSubcategoyDetails(SubcategoryEntities brand);

        bool DeleteSubcategoryDetails(int Id);
    }
}
