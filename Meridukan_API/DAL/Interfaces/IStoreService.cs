﻿using Meridukan_API.Entities.Store;
using System.Collections.Generic;

namespace Meridukan_API.DAL.Interfaces
{
    public interface IStoreService
    {
        List<StoreEntities> GetStores();

        StoreEntities GetStoreDetails(int Id);

        bool AddorUpdateStoreDetails(StoreEntities store);

        bool DeleteStoreDetails(int Id);
    }
}
