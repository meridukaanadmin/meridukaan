﻿using Meridukan_API.Entities.Brand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
    public interface IBrandService
    {
        List<BrandEntities> GetBrandList();

        BrandEntities GetBrandDetails(int Id);

        bool AddorUpdateBrandDetails(BrandEntities brand);

        bool DeleteBrandDetails(int Id);

    }
}
