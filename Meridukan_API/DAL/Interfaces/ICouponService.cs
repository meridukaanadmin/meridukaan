﻿using Meridukan_API.Entities.Coupon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
    public interface ICouponService
    {
        List<CouponEntities> GetCouponList();

        CouponEntities GetCouponDetails(int Id);

        bool AddorUpdateCouponDetails(CouponEntities coupon);

        bool DeleteCouponDetails(int Id);
    }
}
