﻿using Meridukan_API.Entities.SubstitudeCategory;
using System.Collections.Generic;

namespace Meridukan_API.DAL.Interfaces
{
    public interface ISubstitudeCategoryService
    {
        List<SubstitudeCategoryEnitites> GetSubstitudecategoryList();

        SubstitudeCategoryEnitites GetSubstitudecategoryDetails(int Id);

        bool AddorUpdateSubstitudecategoyDetails(SubstitudeCategoryEnitites brand);

        bool DeleteSubstitudecategoryDetails(int Id);
    }
}
