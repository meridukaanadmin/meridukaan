﻿using Meridukan_API.Entities.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.DAL.Interfaces
{
    public interface IUserLoginService
    {
        List<UserLoginEntities> GetUserList();

        UserLoginEntities GetLogin(LoginEntity Login);

        bool addorUpdateUser(UserLoginEntities User);    

        UserLoginEntities GetUserDetails(int UserId);

        bool DeleteUserDetails(int UserId);
    }
}