﻿using Meridukan_API.Entities.Salesman;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
   public interface ISalesmanService
    {
        List<SalesmanEnitities> GetSalesmanList();

        SalesmanEnitities GetSalesmanDetails(int Id);

        bool AddorUpdateSalesmanDetails(SalesmanEnitities salesman);

        bool DeleteSalesmanDetails(int Id);
    }
}
