﻿using Meridukan_API.Entities;
using Meridukan_API.Entities.Unit;
using System.Collections.Generic;

namespace Meridukan_API.DAL.Interfaces
{
    public interface IUnitDAL
    {
        List<UnitEntities> GetUnit();

        WebAPIResponse AddOrUpdateUnit(AddorUpdateUnit unit);
    }
}
