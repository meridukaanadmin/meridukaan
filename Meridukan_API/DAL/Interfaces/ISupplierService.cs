﻿using Meridukan_API.Entities.Supplier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
   public interface ISupplierService
    {
        List<SupplierEntities> GetSupplierList();

        SupplierEntities GetSupplierDetails(int Id);

        bool AddorUpdateSupplierDetails(SupplierEntities supplier);

        bool DeleteSupplierDetails(int Id);
    }
}
