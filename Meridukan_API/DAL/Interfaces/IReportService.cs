﻿using Meridukan_API.Entities.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meridukan_API.DAL.Interfaces
{
    interface IReportService
    {
        List<TransactionReportEntities> GetTransactions();

        List<OrderReportEntities> OrderReportData(DateTime fromdate, DateTime todate);
    }
}
