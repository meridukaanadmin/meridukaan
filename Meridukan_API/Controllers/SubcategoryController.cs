﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Subcategory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class SubcategoryController : ApiController
    {
        private readonly ISubcategoryService subcategoryService;
        public SubcategoryController()
        {
            subcategoryService = new SubcategoryService();
        }

        [HttpGet]
        [Route("api/Subcategory/GetSubcategoryList")]
        public HttpResponseMessage GetSubcategory()
        {
            try
            {
                var Entities = subcategoryService.GetSubcategoryList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Subcategory/GetSubcategoryByCategoryId")]
        public HttpResponseMessage GetSubcategoryByCategoryId(int CategoryId)
        {
            try
            {
                var Entities = subcategoryService.GetSubcategoryByCategoryId(CategoryId);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Subcategory/GetSubcategoryDetails")]
        public HttpResponseMessage GetSubcategory(int subcategoryId)
        {
            try
            {
                var Entities = subcategoryService.GetSubcategoryDetails(subcategoryId);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Subcategory/AddorUpdateSubcategory")]
        public HttpResponseMessage AddorUpdateSubcategory([FromBody]SubcategoryEntities subcategoryEntity)
        {
            bool result = false;
            try
            {
                result = subcategoryService.AddorUpdateSubcategoyDetails(subcategoryEntity);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Subcategory/DeleteSubcategory")]
        public HttpResponseMessage DeleteSubcategory([FromBody]DeleteSubcategory subcategory)
        {
            bool result = false;
            try
            {
                result = subcategoryService.DeleteSubcategoryDetails(subcategory.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
