﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class DashboardController : ApiController
    {
        private readonly IDashboardService dashboardService;
        public DashboardController()
        {
            dashboardService = new DashboardService();
        }

        [HttpGet]
        [Route("api/Dashboard/GetDashboardDetails")]
        public HttpResponseMessage GetDashboardData()
        {
            try
            {
                var Entities = dashboardService.GetDashboardData();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
