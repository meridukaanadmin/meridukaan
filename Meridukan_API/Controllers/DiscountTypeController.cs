﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Coupon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class DiscountTypeController : ApiController
    {
        private readonly IDiscountTypeService typeService;
        public DiscountTypeController()
        {
            typeService = new DiscountTypeService();
        }

        [HttpGet]
        [Route("api/DiscountType/GetDiscountTypeList")]
        public HttpResponseMessage GetDiscountTypeList()
        {
            try
            {
                var BrandEntities = typeService.GetDiscountTypeList();
                if (BrandEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, BrandEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, BrandEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/DiscountType/GetDiscountTypeDetails")]
        public HttpResponseMessage GetDiscountTypeDetails(int Id)
        {
            try
            {
                var Entities = typeService.GetDiscountTypeDetails(Id);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/DiscountType/AddorUpdateDiscountType")]
        public HttpResponseMessage AddorUpdateDiscountType([FromBody]DiscountType discount)
        {
            bool result = false;
            try
            {
                result = typeService.AddorUpdateDiscountType(discount);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/DiscountType/DeleteDiscountTypeDetails")]
        public HttpResponseMessage DeleteDiscountTypeDetails([FromBody]DeleteDiscountType delete)
        {
            bool result = false;
            try
            {
                result = typeService.DeleteDiscountTypeDetails(delete.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
