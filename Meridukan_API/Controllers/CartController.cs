﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class CartController : ApiController
    {
        private readonly ICartService cartService;
        public CartController()
        {
            cartService = new CartService();
        }

        [HttpGet]
        [Route("api/Cart/GetCardDetailsByCustomer")]
        public HttpResponseMessage AddorUpdateBrand(string Contact)
        {

            var Entities = cartService.GetCartDetailsByCustomer(Contact);
            if (Entities != null)
                return Request.CreateResponse(HttpStatusCode.OK, Entities);
            else
                return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
        }

        [HttpPost]
        [Route("api/Cart/SaveCartAtSuspend")]
        public HttpResponseMessage AddorUpdateBrand([FromBody]CartEntities cartEntities)
        {
            bool result = false;
            try
            {
                result = cartService.SaveCartAtSuspend(cartEntities);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Cart/DeleteCartByCustomer")]
        public HttpResponseMessage DeleteCartByCustomer([FromBody]DeleteCartDataByCustomerEntities deleteCart)
        {
            bool result = false;
            try
            {
                result = cartService.DeleteCartByCustomer(deleteCart.Contact);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Cart/SaveCustomerCart")]
        public HttpResponseMessage SaveCustomerCart([FromBody]CartEntities cartEntities)
        {
            bool result = false;
            try
            {
                result = cartService.SaveCartAtSuspend(cartEntities);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Cart/DeleteCustomerCartItems")]
        public HttpResponseMessage DeleteCustomerCartItems([FromBody]DeleteCustomerCartItemsEntities deleteCart)
        {
            bool result = false;
            try
            {
                result = cartService.DeleteCustomerCartItems(deleteCart);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
