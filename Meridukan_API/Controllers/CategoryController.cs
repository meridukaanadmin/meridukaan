﻿
using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities;
using Meridukan_API.Entities.Category;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class CategoryController : ApiController
    {
        private readonly ICategoryService categoryService;
        public CategoryController()
        {
            categoryService = new CategoryService();
        }

        [HttpGet]
        [Route("api/Category/GetCategoryList")]
        public HttpResponseMessage GetCategory()
        {
            try
            {
                var categoryEntities = categoryService.GetCategory();
                if (categoryEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, categoryEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, categoryEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Category/GetCategoryDetails")]
        public HttpResponseMessage GetCategory(int CategoryId)
        {
            try
            {
                var CategoryEntities = categoryService.GetCategoryDetails(CategoryId);
                if (CategoryEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, CategoryEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, CategoryEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Category/AddorUpdateCategory")]
        public HttpResponseMessage AddorUpdateCategory([FromBody]CategoryEntities categoryEntity)
        {
            bool result = false;
            try
            {
                result = categoryService.AddorUpdateCategoryDetails(categoryEntity);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Category/DeleteCategory")]
        public HttpResponseMessage DeleteCategory([FromBody]CategoryEntities categoryEntity)
        {
            bool result = false;
            try
            {
                result = categoryService.DeleteCategoryDetails(categoryEntity.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

    }
}
