﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Salesman;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class SalesmanController : ApiController
    {
        private readonly ISalesmanService salesmanService;
        public SalesmanController()
        {
            salesmanService = new SalesmanService();
        }

        [HttpGet]
        [Route("api/Salesman/GetSalesmanList")]
        public HttpResponseMessage GetSalesmanList()
        {
            try
            {
                var Entities = salesmanService.GetSalesmanList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Salesman/GetSalesmanDetails")]
        public HttpResponseMessage GetSalesmanDetails(int Id)
        {
            try
            {
                var Entities = salesmanService.GetSalesmanDetails(Id);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Salesman/AddorUpdateSalesman")]
        public HttpResponseMessage AddorUpdateSalesman([FromBody]SalesmanEnitities salesman)
        {
            bool result = false;
            try
            {
                result = salesmanService.AddorUpdateSalesmanDetails(salesman);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Salesman/DeleteSalesman")]
        public HttpResponseMessage DeleteBrandDetails([FromBody]DeleteSalesman delete)
        {
            bool result = false;
            try
            {
                result = salesmanService.DeleteSalesmanDetails(delete.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

    }
}
