﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class UserLoginController : ApiController
    {

        private readonly IUserLoginService UserLoginServices;
        public UserLoginController()
        {
            UserLoginServices = new UserLoginServices();
        }
        [HttpPost]
        [Route("api/Login/GetLogin")]
        public HttpResponseMessage GetLogin(LoginEntity Login)
        {
            try
            {
                var LoginEntities = UserLoginServices.GetLogin(Login);
                if (LoginEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, LoginEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, LoginEntities);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
        [HttpPost]
        [Route("api/User/AddorUpdateUser")]
        public HttpResponseMessage AddorUpdateUser(UserLoginEntities User)
        {
            try
            {
                bool result = false;
                result = UserLoginServices.addorUpdateUser(User);
                return Request.CreateResponse(HttpStatusCode.OK, result);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/User/GetUserList")]
        public HttpResponseMessage GetUserList()
        {
            try
            {
                var Entities = UserLoginServices.GetUserList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/User/GetUserDetails")]
        public HttpResponseMessage GetUserDetails(int UserId)
        {
            try
            {
                var BrandEntities = UserLoginServices.GetUserDetails(UserId);
                if (BrandEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, BrandEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, BrandEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/User/DeleteUser")]
        public HttpResponseMessage DeleteUser([FromBody]DeleteUser user)
        {
            bool result = false;
            try
            {
                result = UserLoginServices.DeleteUserDetails(user.UserId);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
