﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class PinCodeController : ApiController
    {
        private readonly IPinCodeServices pinCodeService;
        public PinCodeController()
        {
            pinCodeService = new PinCodeServices();
        }

        [HttpGet]
        [Route("api/PinCode/GetPinCodeDetail")]
        public HttpResponseMessage GetPinCodeDetail(string PinCode)
        {
            try
            {
                var BrandEntities = pinCodeService.GetPinCodeDetail(PinCode);
                if (BrandEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, BrandEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, BrandEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
