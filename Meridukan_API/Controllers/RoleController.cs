﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class RoleController : ApiController
    {
        private readonly IRoleService roleService;
        public RoleController()
        {
            roleService = new RoleService();
        }

        [HttpGet]
        [Route("api/Role/GetRoleList")]
        public HttpResponseMessage GetRole()
        {
            try
            {
                var Entities = roleService.GetRoleList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Role/GetRoleDetails")]
        public HttpResponseMessage GetRole(int RoleId)
        {
            try
            {
                var Entities = roleService.GetRoleDetails(RoleId);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Role/AddorUpdateRole")]
        public HttpResponseMessage AddorUpdateRole([FromBody]RoleEntities role)
        {
            bool result = false;
            try
            {
                result = roleService.AddorUpdateRoleDetails(role);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Role/DeleteRole")]
        public HttpResponseMessage DeleteRoleDetails([FromBody]DeleteRole role)
        {
            bool result = false;
            try
            {
                result = roleService.DeleteRoleDetails(role.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
