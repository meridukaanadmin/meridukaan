﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class OrderStatusController : ApiController
    {
        private readonly IOrderStatusService orderStatusService;
        public OrderStatusController()
        {
            orderStatusService = new OrderStatusService();
        }

        [HttpGet]
        [Route("api/OrderStatus/GetOrderStatusList")]
        public HttpResponseMessage GetBrand()
        {
            try
            {
                var Entities = orderStatusService.getOrderStatusList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
