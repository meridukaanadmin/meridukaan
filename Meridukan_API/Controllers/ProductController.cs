﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class ProductController : ApiController
    {
        private readonly IProductService ProductService;
        public ProductController()
        {
            ProductService = new ProductService();
        }

        #region ProductType
        [HttpGet]
        [Route("api/Product/GetProductTypeList")]
        public HttpResponseMessage GetProductType()
        {
            try
            {
                var ProductTypeEntities = ProductService.GetProductTypeList();
                if (ProductTypeEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, ProductTypeEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, ProductTypeEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Product/GetProductTypeDetails")]
        public HttpResponseMessage GetProductType(int ProductTypeId)
        {
            try
            {
                var ProductTypeEntities = ProductService.GetProductTypeDetails(ProductTypeId);
                if (ProductTypeEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, ProductTypeEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, ProductTypeEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Product/AddorUpdateProductType")]
        public HttpResponseMessage AddorUpdateProductType([FromBody]ProductTypeEntities ProductTypeEntity)
        {
            bool result = false;
            try
            {
                result = ProductService.AddorUpdateProductTypeDetails(ProductTypeEntity);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Product/DeleteProductType")]
        public HttpResponseMessage DeleteProductTypeDetails([FromBody]ProductTypeEntities ProductTypeEntity)
        {
            bool result = false;
            try
            {
                result = ProductService.DeleteProductTypeDetails(ProductTypeEntity.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
        #endregion
          
        #region ProductAttribute
        [HttpGet]
        [Route("api/Product/GetProductAttributeList")]
        public HttpResponseMessage GetProductAttribute()
        {
            try
            {
                var ProductAttributeEntities = ProductService.GetProductAttributeList();
                if (ProductAttributeEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, ProductAttributeEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, ProductAttributeEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Product/GetProductAttributeDetails")]
        public HttpResponseMessage GetProductAttribute(int ProductAttributeId)
        {
            try
            {
                var ProductAttributeEntities = ProductService.GetProductAttributeDetails(ProductAttributeId);
                if (ProductAttributeEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, ProductAttributeEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, ProductAttributeEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Product/AddorUpdateProductAttribute")]
        public HttpResponseMessage AddorUpdateProductAttribute([FromBody]ProductAttributeEntities ProductAttributeEntity)
        {
            bool result = false;
            try
            {
                result = ProductService.AddorUpdateProductAttributeDetails(ProductAttributeEntity);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Product/DeleteProductAttribute")]
        public HttpResponseMessage DeleteProductAttributeDetails([FromBody]ProductAttributeEntities ProductAttributeEntity)
        {
            bool result = false;
            try
            {
                result = ProductService.DeleteProductAttributeDetails(ProductAttributeEntity.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
        #endregion
         
        #region Product
        [HttpGet]
        [Route("api/Product/GetProductList")]
        public HttpResponseMessage GetProduct()
        {
            try
            {
                var ProductEntities = ProductService.GetProductList();
                if (ProductEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, ProductEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, ProductEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Product/GetProductDetails")]
        public HttpResponseMessage GetProduct(int ProductId)
        {
            try
            {
                var ProductEntities = ProductService.GetProductDetails(ProductId);
                if (ProductEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, ProductEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, ProductEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Product/AddorUpdateProduct")]
        public HttpResponseMessage AddorUpdateProduct([FromBody]AddProductModel ProductEntity)
        {
            bool result = false;
            try
            {
                result = ProductService.AddorUpdateProductDetails(ProductEntity);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Product/DeleteProduct")]
        public HttpResponseMessage DeleteProductDetails([FromBody]ProductEntities ProductEntity)
        {
            bool result = false;
            try
            {
                result = ProductService.DeleteProductDetails(ProductEntity.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
        #endregion

        [HttpPost]
        [Route("api/Product/DeleteProductData")]
        public HttpResponseMessage DeleteProduct([FromBody]DeleteProductEntities ProductEntity)
        {
            bool result = false;
            try
            {
                result = ProductService.DeleteProduct(ProductEntity.ProductId);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpGet]
        [Route("api/Product/GetProductGRNList")]
        public HttpResponseMessage GetProductGRNList(int ProductId)
        {
            try
            {
                var Entities = ProductService.GetProductGRNList(ProductId);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Product/GetSaleProductList")]
        public HttpResponseMessage GetSaleProductList()
        {
            try
            {
                var ProductTypeEntities = ProductService.GetSaleProductList();
                if (ProductTypeEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, ProductTypeEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, ProductTypeEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Product/GetGroceryProductList")]
        public HttpResponseMessage GetProducts(int CategoryId)
        {
            try
            {
                var ProductEntities = ProductService.GetProducts(CategoryId);
                if (ProductEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, ProductEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, ProductEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }


    }
}
