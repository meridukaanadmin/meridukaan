﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class PaymentController : ApiController
    {
        private readonly IPaymentService paymentService;
        public PaymentController()
        {
            paymentService = new PaymentService();
        }

        [HttpGet]
        [Route("api/Payment/GetPaymentList")]
        public HttpResponseMessage GetPaymentList(string Contact)
        {
            try
            {
                var Entities = paymentService.GetPayments(Contact);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Payment/GetPaymentByOrderId")]
        public HttpResponseMessage GetPaymentByOrderId(int OrderId)
        {
            try
            {
                var Entities = paymentService.GetPaymentByOrderId(OrderId);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Payment/AddPayment")]
        public HttpResponseMessage AddPayment([FromBody]PaymentEntities payment)
        {
            bool result = false;
            try
            {
                result = paymentService.AddPayment(payment);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
