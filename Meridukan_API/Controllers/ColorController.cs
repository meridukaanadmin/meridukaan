﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Color;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class ColorController : ApiController
    {
        private readonly IColorService colorService;
        public ColorController()
        {
            colorService = new ColorService();
        }

        [HttpGet]
        [Route("api/Color/GetColorList")]
        public HttpResponseMessage GetSize()
        {
            try
            {
                var Entities = colorService.GetColorList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Color/GetColorDetails")]
        public HttpResponseMessage GetColor(int Id)
        {
            try
            {
                var Entities = colorService.GetColorDetails(Id);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Color/AddorUpdateColor")]
        public HttpResponseMessage AddorUpdateColor([FromBody]ColorEntities color)
        {
            bool result = false;
            try
            {
                result = colorService.AddorUpdateColorDetails(color);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Color/DeleteColor")]
        public HttpResponseMessage DeleteColorDetails([FromBody]DeleteColor color)
        {
            bool result = false;
            try
            {
                result = colorService.DeleteColorDetails(color.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
