﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.DeliveryPinCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class DeliveryPostalCodeController : ApiController
    {
        private readonly IDeliveryPincodeService deliveryService;
        public DeliveryPostalCodeController()
        {
            deliveryService = new DeliveryPincodeService();
        }

        [HttpGet]
        [Route("api/DeliveryPinCode/GetDeliveryPinCodeList")]
        public HttpResponseMessage GetDeliveryPinCodeEntities()
        {
            try
            {
                var BrandEntities = deliveryService.GetDeliveryPinCodeEntities();
                if (BrandEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, BrandEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, BrandEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/DeliveryPinCode/GetDeliveryPinCodeDetails")]
        public HttpResponseMessage GetDeliveryPinCodeDetails(int Id)
        {
            try
            {
                var BrandEntities = deliveryService.GetDeliveryPinCodeDetails(Id);
                if (BrandEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, BrandEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, BrandEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/DeliveryPinCode/AddorUpdateDeliveryPinCodeDetails")]
        public HttpResponseMessage AddorUpdateDeliveryPinCodeDetails([FromBody]DeliveryPinCodeEntities Entity)
        {
            bool result = false;
            try
            {
                result = deliveryService.AddorUpdateDeliveryPinCodeDetails(Entity);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/DeliveryPinCode/DeleteDeliveryPinCodeDetails")]
        public HttpResponseMessage DeleteDeliveryPinCodeDetails([FromBody]DeleteDeliveryPinCode Entity)
        {
            bool result = false;
            try
            {
                result = deliveryService.DeleteDeliveryPinCodeDetails(Entity.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

    }
}
