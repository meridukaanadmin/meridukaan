﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class ReportController : ApiController
    {
        private readonly IReportService reportService;
        public ReportController()
        {
            reportService = new ReportService();
        }

        [HttpGet]
        [Route("api/Report/GeLedgerReportList")]
        public HttpResponseMessage GeLedgerReportList()
        {
            try
            {
                var Entities = reportService.GetTransactions();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
