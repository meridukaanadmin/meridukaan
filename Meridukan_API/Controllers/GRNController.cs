﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.GRN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class GRNController : ApiController
    {
        private readonly IGRNService GRNService;
        public GRNController()
        {
            GRNService = new GRNService();
        }


        [HttpGet]
        [Route("api/GRN/GetGRNList")]
        public HttpResponseMessage GetGRNList()
        {
            try
            {
                var Entities = GRNService.GetGRNList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/GRN/AddorUpdateGRN")]
        public HttpResponseMessage AddorUpdateGRN(GRNEntities grn)
        {
            try
            {
                bool result = false;
                result = GRNService.AddorUpdateGRN(grn);
                return Request.CreateResponse(HttpStatusCode.OK, result);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/GRN/GetGRNDetails")]
        public HttpResponseMessage GetGRNDetails(int GrnId)
        {
            try
            {
                var Entities = GRNService.GetGRNDetailsNew(GrnId);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/GRN/DeleteGRN")]
        public HttpResponseMessage DeleteGRN([FromBody]DeleteGRN deleteGRN)
        {
            bool result = false;
            try
            {
                result = GRNService.DeleteGRNDetails(deleteGRN.GrnId);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

    }
}
