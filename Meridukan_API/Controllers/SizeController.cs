﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Size;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class SizeController : ApiController
    {
        private readonly ISizeService sizeService;
        public SizeController()
        {
            sizeService = new SizeService();
        }

        [HttpGet]
        [Route("api/Size/GetSizeList")]
        public HttpResponseMessage GetSize()
        {
            try
            {
                var Entities = sizeService.GetSizeList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Size/GetSizeDetails")]
        public HttpResponseMessage GetSize(int Id)
        {
            try
            {
                var Entities = sizeService.GetSizeDetails(Id);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Size/AddorUpdateSize")]
        public HttpResponseMessage AddorUpdateSize([FromBody]SizeEntities sizentities)
        {
            bool result = false;
            try
            {
                result = sizeService.AddorUpdateSize(sizentities);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Size/DeleteSize")]
        public HttpResponseMessage DeleteSizeDetails([FromBody]DeleteSize size)
        {
            bool result = false;
            try
            {
                result = sizeService.DeleteSizeDetails(size.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
