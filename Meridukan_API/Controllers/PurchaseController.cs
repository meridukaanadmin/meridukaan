﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Purchase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class PurchaseController : ApiController
    {
        private readonly IPurchaseService purchaseService;
        public PurchaseController()
        {
            purchaseService = new PurchaseService();
        }

        [HttpGet]
        [Route("api/Purchase/GetPurchaseList")]
        public HttpResponseMessage GetPurchaseList()
        {
            try
            {
                var Entities = purchaseService.GetPurchaseList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Purchase/AddorUpdatePO")]
        public HttpResponseMessage AddorUpdatePO(PurchaseEnitities purchase)
        {
            try
            {
                bool result = false;
                result = purchaseService.AddorUpdatePO(purchase);
                return Request.CreateResponse(HttpStatusCode.OK, result);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Purchase/GetPurchaseDetails")]
        public HttpResponseMessage GetPurchaseDetails(int PoId)
        {
            try
            {
                var Entities = purchaseService.GetPurchaseDetails(PoId);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Purchase/GetPurchaseDetailsForGRN")]
        public HttpResponseMessage GetPurchaseDetailsForGRN(int PoId)
        {
            try
            {
                var Entities = purchaseService.GetPurchaseDetailsForGRN(PoId);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Purchase/DeletePO")]
        public HttpResponseMessage DeleteUser([FromBody]DeletePO deletePO)
        {
            bool result = false;
            try
            {
                result = purchaseService.DeletePoDetails(deletePO.PoId);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
