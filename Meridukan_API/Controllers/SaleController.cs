﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Sale;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class SaleController : ApiController
    {
        private readonly ISaleService saleService;
        public SaleController()
        {
            saleService = new SaleService();
        }

        [HttpGet]
        [Route("api/Sale/GetSaleList")]
        public HttpResponseMessage GetSaleList()
        {
            try
            {
                var Entities = saleService.GetSaleList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Sale/AddorUpdateSale")]
        public HttpResponseMessage AddorUpdatePO(SaleEntities sale)
        {
            long? result = 0;
            try
            {                
                result = saleService.AddorUpdateSale(sale);
                return Request.CreateResponse(HttpStatusCode.OK, result);

            }
            catch (Exception ex)
            {
                result = 0;
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Sale/GetSaleDetails")]
        public HttpResponseMessage GetSaleDetails(int SaleId)
        {
            try
            {
                var Entities = saleService.GetSaleDetails(SaleId);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Sale/DeleteSale")]
        public HttpResponseMessage DeleteUser([FromBody]DeleteSO deleteSO)
        {
            bool result = false;
            try
            {
                result = saleService.DeleteSaleDetails(deleteSO.SaleId);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
        

        [HttpGet]
        [Route("api/Sale/GetSaleRateByBarcode")]
        public HttpResponseMessage GetSaleRate(string Barcode)
        {
            try
            {
                var Entities = saleService.GetSaleRate(Barcode);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Sale/UpdateOrderStatus")]
        public HttpResponseMessage UpdateOrderStatus([FromBody]OrderStatusEntities order)
        {
            bool result = false;
            try
            {
                result = saleService.UpdateOrderStatus(order.SaleId,order.OrderStatusId);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpGet]
        [Route("api/Sale/GetProductSearchData")]
        public HttpResponseMessage GetProductSearchList()
        {
            try
            {
                var Entities = saleService.GetProductSearchData();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
