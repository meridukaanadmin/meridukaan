﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.SubstitudeCategory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class SubstitudeCategoryController : ApiController
    {
        private readonly ISubstitudeCategoryService substitudeCategoryService;
        public SubstitudeCategoryController()
        {
            substitudeCategoryService = new SubstitudeCategoryService();
        }

        [HttpGet]
        [Route("api/SubstitudeCategory/GetSubstitudecategoryList")]
        public HttpResponseMessage GetSubstitudecategory()
        {
            try
            {
                var Entities = substitudeCategoryService.GetSubstitudecategoryList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/SubstitudeCategory/GetSubstitudecategoryDetails")]
        public HttpResponseMessage GetSubstitudecategory(int substitudecategoryId)
        {
            try
            {
                var Entities = substitudeCategoryService.GetSubstitudecategoryDetails(substitudecategoryId);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/SubstitudeCategory/AddorUpdateSubstitudecategory")]
        public HttpResponseMessage AddorUpdateSubcategory([FromBody]SubstitudeCategoryEnitites subcategoryEntity)
        {
            bool result = false;
            try
            {
                result = substitudeCategoryService.AddorUpdateSubstitudecategoyDetails(subcategoryEntity);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/SubstitudeCategory/DeleteSubstitudecategory")]
        public HttpResponseMessage DeleteSubcategory([FromBody]DeleteSubstitudecategory subcategory)
        {
            bool result = false;
            try
            {
                result = substitudeCategoryService.DeleteSubstitudecategoryDetails(subcategory.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

    }
}
