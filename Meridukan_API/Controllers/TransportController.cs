﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class TransportController : ApiController
    {
        private readonly ITransportService transportService;
        public TransportController()
        {
            transportService = new TransportService();
        }

        [HttpGet]
        [Route("api/Transport/GetTransportList")]
        public HttpResponseMessage GetTransport()
        {
            try
            {
                var Entities = transportService.GetTransportDetailList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Transport/GetTransportDetails")]
        public HttpResponseMessage GetTransportDetails(int Id)
        {
            try
            {
                var Entities = transportService.GetTransportDetails(Id);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Transport/AddorUpdateTransportDetails")]
        public HttpResponseMessage AddorUpdateTransportDetails([FromBody]TransportEntities Entity)
        {
            bool result = false;
            try
            {
                result = transportService.AddorUpdateTransportDetails(Entity);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Transport/DeleteTransportDetails")]
        public HttpResponseMessage DeleteTransportDetails([FromBody]DeleteTransport Entity)
        {
            bool result = false;
            try
            {
                result = transportService.DeleteTransportDetails(Entity.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
