﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Brand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class BrandController : ApiController
    {
        private readonly IBrandService brandService;
        public BrandController()
        {
            brandService = new BrandService();
        }

        [HttpGet]
        [Route("api/Brand/GetBrandList")]
        public HttpResponseMessage GetBrand()
        {
            try
            {
                var BrandEntities = brandService.GetBrandList();
                if (BrandEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, BrandEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, BrandEntities);
            }
            
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Brand/GetBrandDetails")]
        public HttpResponseMessage GetBrand(int BrandId)
        {
            try
            {
                var BrandEntities = brandService.GetBrandDetails(BrandId);
                if (BrandEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, BrandEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, BrandEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Brand/AddorUpdateBrand")]
        public HttpResponseMessage AddorUpdateBrand([FromBody]BrandEntities brandEntity)
        {
            bool result = false;
            try
            {
                result = brandService.AddorUpdateBrandDetails(brandEntity);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Brand/DeleteBrand")]
        public HttpResponseMessage DeleteBrandDetails([FromBody]BrandEntities brandEntity)
        {
            bool result = false;
            try
            { 
                result = brandService.DeleteBrandDetails(brandEntity.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

    }
}
