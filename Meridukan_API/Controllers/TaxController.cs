﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.ClassesTax;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class TaxController : ApiController
    {
        private readonly ITaxService TaxService;
        public TaxController()
        {
            TaxService = new TaxService();
        }

        [HttpGet]
        [Route("api/Tax/GetTaxList")]
        public HttpResponseMessage GetTax()
        {
            try
            {
                var  Entities = TaxService.GetTaxList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
         
        [HttpGet]
        [Route("api/Tax/GetTaxDetails")]
        public HttpResponseMessage GetTax(int TaxId)
        {
            try
            {
                var TaxEntities = TaxService.GetTaxDetails(TaxId);
                if (TaxEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, TaxEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, TaxEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Tax/AddorUpdateTax")]
        public HttpResponseMessage AddorUpdateTax([FromBody]TaxEntities TaxEntity)
        {
            bool result = false;
            try
            {
                result = TaxService.AddorUpdateTaxDetails(TaxEntity);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Tax/DeleteTax")]
        public HttpResponseMessage DeleteTaxDetails([FromBody]TaxEntities TaxEntity)
        {
            bool result = false;
            try
            {
                result = TaxService.DeleteTaxDetails(TaxEntity.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

    }
}
