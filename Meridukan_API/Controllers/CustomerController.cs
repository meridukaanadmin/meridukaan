﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Customer;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class CustomerController : ApiController
    {
        private readonly ICustomerService customerService;
        public CustomerController()
        {
            customerService = new CustomerService();
        }

        [HttpGet]
        [Route("api/Customer/GetCustomerList")]
        public HttpResponseMessage GetCustomer()
        {
            try
            {
                var Entities = customerService.GetCustomerList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Customer/GetCustomerDetails")]
        public HttpResponseMessage GetCustomer(int Id)
        {
            try
            {
                var Entities = customerService.GetCustomerDetails(Id);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Customer/AddorUpdateCustomer")]
        public HttpResponseMessage AddorUpdateCustomer([FromBody]CustomerEntities customer)
        {
            bool result = false;
            try
            {
                result = customerService.AddorUpdateCustomerDetails(customer);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Customer/DeleteCustomer")]
        public HttpResponseMessage DeleteCustomerDetails([FromBody]DeleteCustomer customer)
        {
            bool result = false;
            try
            {
                result = customerService.DeleteCustomerDetails(customer.Contact);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpGet]
        [Route("api/Customer/GetCustomerOrdersList")]
        public HttpResponseMessage GetCustomerOrdersList(string Contact)
        {
            try
            {
                var Entities = customerService.GetCustomerOrdersList(Contact);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Customer/GetCustomerReportList")]
        public HttpResponseMessage GetCustomerReportList()
        {
            try
            {
                var Entities = customerService.GetCustomerReportList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
