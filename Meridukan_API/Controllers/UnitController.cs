﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.Entities;
using Meridukan_API.Entities.Unit;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class UnitController : ApiController
    {
        UnitDAL unitDAL = new UnitDAL();

        [HttpGet]
        public WebAPIResponse GetUnitList()
        {
            WebAPIResponse Response = new WebAPIResponse();

            try
            {
                List<UnitEntities> Data = unitDAL.GetUnit();

                Response.Message = "Success";
                Response.Description = "Data Fetched successfully";
                Response.Data = Data;
            }
            catch
            {
                Response.Message = "failure";
                Response.Description = "Error";
                Response.Data = null;
            }
           

            return Response;
        }

        #region "Add or Update Unit" 
        [HttpPost]
        public object AddOrUpdateUnit(AddorUpdateUnit objData)
        {
            WebAPIResponse Response = new WebAPIResponse();
            try
            {

                var resposne = unitDAL.AddOrUpdateUnit(objData);
                return resposne;


            }
            catch (Exception ex)
            {

                Response.Message = "failure";
                Response.Description = "";
                Response.Error = ex.Message;
            }

            return Response;
        }
        #endregion
    }
}
