﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Coupon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class CouponController : ApiController
    {
        private readonly ICouponService couponService;
        public CouponController()
        {
            couponService = new CouponService();
        }

        [HttpGet]
        [Route("api/Coupon/GetCouponList")]
        public HttpResponseMessage GetCouponList()
        {
            try
            {
                var BrandEntities = couponService.GetCouponList();
                if (BrandEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, BrandEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, BrandEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Coupon/GetCouponDetails")]
        public HttpResponseMessage GetCouponDetails(int Id)
        {
            try
            {
                var BrandEntities = couponService.GetCouponDetails(Id);
                if (BrandEntities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, BrandEntities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, BrandEntities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Coupon/AddorUpdateCouponDetails")]
        public HttpResponseMessage AddorUpdateCouponDetails([FromBody]CouponEntities coupon)
        {
            bool result = false;
            try
            {
                result = couponService.AddorUpdateCouponDetails(coupon);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Coupon/DeleteCouponDetails")]
        public HttpResponseMessage DeleteCouponDetails([FromBody]DeleteCoupon delete)
        {
            bool result = false;
            try
            {
                result = couponService.DeleteCouponDetails(delete.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
