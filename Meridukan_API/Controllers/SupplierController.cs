﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Supplier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class SupplierController : ApiController
    {
        private readonly ISupplierService supplierService;
        public SupplierController()
        {
            supplierService = new SupplierService();
        }

        [HttpGet]
        [Route("api/Supplier/GetSupplierList")]
        public HttpResponseMessage GetSupplier()
        {
            try
            {
                var Entities = supplierService.GetSupplierList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Supplier/GetSupplierDetails")]
        public HttpResponseMessage GetSupplier(int SupplierId)
        {
            try
            {
                var Entities = supplierService.GetSupplierDetails(SupplierId);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Supplier/AddorUpdateSupplier")]
        public HttpResponseMessage AddorUpdateSupplier([FromBody]SupplierEntities supplierEntity)
        {
            bool result = false;
            try
            {
                result = supplierService.AddorUpdateSupplierDetails(supplierEntity);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Supplier/DeleteSupplier")]
        public HttpResponseMessage DeleteSupplierDetails([FromBody]DeleteSupplier Entity)
        {
            bool result = false;
            try
            {
                result = supplierService.DeleteSupplierDetails(Entity.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
