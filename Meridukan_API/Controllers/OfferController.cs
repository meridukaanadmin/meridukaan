﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Offer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class OfferController : ApiController
    {
        private readonly IOfferService offerService;
        public OfferController()
        {
            offerService = new OfferService();
        }

        [HttpGet]
        [Route("api/Offer/GetOfferList")]
        public HttpResponseMessage GetBrand()
        {
            try
            {
                var Entities = offerService.GetOfferList();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Offer/GetOfferDetails")]
        public HttpResponseMessage GetOfferDetails(int Id)
        {
            try
            {
                var Entities = offerService.GetOfferDetails(Id);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Offer/AddorUpdateOffer")]
        public HttpResponseMessage AddorUpdateOffer([FromBody]OfferEnitities offer)
        {
            bool result = false;
            try
            {
                result = offerService.AddorUpdateOffer(offer);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Offer/DeleteOffer")]
        public HttpResponseMessage DeleteOffer([FromBody]DeleteOfferEnitities delete)
        {
            bool result = false;
            try
            {
                result = offerService.DeleteOffer(delete.OfferId);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
