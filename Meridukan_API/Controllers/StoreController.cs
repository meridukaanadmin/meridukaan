﻿using Meridukan_API.DAL.Classes;
using Meridukan_API.DAL.Interfaces;
using Meridukan_API.Entities.Store;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Meridukan_API.Controllers
{
    public class StoreController : ApiController
    {
        private readonly IStoreService storeService;
        public StoreController()
        {
            storeService = new StoreService();
        }

        [HttpGet]
        [Route("api/Store/GetStoreList")]
        public HttpResponseMessage GetStores()
        {
            try
            {
                var Entities = storeService.GetStores();
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpGet]
        [Route("api/Store/GetStoreDetails")]
        public HttpResponseMessage GetStore(int StoreId)
        {
            try
            {
                var Entities = storeService.GetStoreDetails(StoreId);
                if (Entities != null)
                    return Request.CreateResponse(HttpStatusCode.OK, Entities);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, Entities);
            }

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("api/Store/AddorUpdateStore")]
        public HttpResponseMessage AddorUpdateStore([FromBody]StoreEntities storeEntity)
        {
            bool result = false;
            try
            {
                result = storeService.AddorUpdateStoreDetails(storeEntity);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }

        [HttpPost]
        [Route("api/Store/DeleteStore")]
        public HttpResponseMessage DeleteStoreDetails([FromBody]DeleteStore deleteStore)
        {
            bool result = false;
            try
            {
                result = storeService.DeleteStoreDetails(deleteStore.Id);

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                result = false;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ToString());
            }
        }
    }
}
