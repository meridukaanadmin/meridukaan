//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Meridukan_API.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ItemTransactionsTB
    {
        public int Id { get; set; }
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> GrnId { get; set; }
        public string GrnNo { get; set; }
        public Nullable<System.DateTime> GrnDate { get; set; }
        public Nullable<System.DateTime> ExpiryDate { get; set; }
        public string Size { get; set; }
        public string PurchaseQty { get; set; }
        public Nullable<decimal> PurchaseRate { get; set; }
        public Nullable<decimal> MRP { get; set; }
        public string SaleQty { get; set; }
        public Nullable<decimal> SaleRate { get; set; }
        public Nullable<int> TaxInclusion { get; set; }
    }
}
