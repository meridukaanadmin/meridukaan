//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Meridukan_API.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CustomerTB
    {
        public CustomerTB()
        {
            this.OrderTBs = new HashSet<OrderTB>();
            this.ShippingTBs = new HashSet<ShippingTB>();
        }
    
        public int Id { get; set; }
        public string CustomerId { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string Address { get; set; }
        public string PinCode { get; set; }
        public string EmailId { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual ICollection<OrderTB> OrderTBs { get; set; }
        public virtual ICollection<ShippingTB> ShippingTBs { get; set; }
    }
}
