﻿using System;

namespace Meridukan_API.Models
{
    public class EntityNotFoundException : Exception
    {
        /// <summary>
        /// Name of the Entity which is not found when accessed by Id
        /// </summary>
        public string Entity { get; set; }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public EntityNotFoundException() { }

        /// <summary>
        /// Constructor with custom error message
        ///  </summary>
        public EntityNotFoundException(string message) : base(message)
        {

        }

        /// <summary>
        /// Exception thrown when Entity referred by Id is returned as null
        /// </summary>
        /// <param name="entity">Name of the Entity, e.g. User, UserRole, Client etc.</param>
        /// <param name="message">Error message which needs to be thrown.</param>
        /// <param name="innerException">Inner exception which is wrapped in this exception.</param>
        public EntityNotFoundException(string entity, string message, Exception inner) : base(message, inner)
        {
            this.Entity = entity;
        }
    }
}