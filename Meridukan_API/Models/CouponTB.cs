//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Meridukan_API.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CouponTB
    {
        public int Id { get; set; }
        public string CouponCode { get; set; }
        public Nullable<int> DiscountTypeId { get; set; }
        public Nullable<decimal> CouponAmt { get; set; }
        public Nullable<bool> IsfreeShipping { get; set; }
        public Nullable<System.DateTime> ExpiryDate { get; set; }
        public Nullable<decimal> MinimumSpend { get; set; }
        public Nullable<decimal> MaximumSpend { get; set; }
    }
}
