﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.DeliveryPinCode
{
    public class DeliveryPinCodeEntities
    {
        public int Id { get; set; }
        public string PostalCode { get; set; }
        public string Region { get; set; }
        public string AreaName { get; set; }
    }

    public class DeleteDeliveryPinCode
    {
        public int Id { get; set; }
    }
}