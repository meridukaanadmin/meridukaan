﻿using System;
using System.Collections.Generic;

namespace Meridukan_API.Entities.Cart
{
    public class CartEntities
    {
        public int CartId { get; set; }
        public string CartNo { get; set; }
        public DateTime date { get; set; }
        public string Contact { get; set; }
        public string CustomerName { get; set; }
        public int CustomerId { get; set; }
        public decimal Total { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal GrandTotal { get; set; }
        public List<CartDetailsEntities> cartDetails { get; set; }
    }

    public class CartDetailsEntities
    {
        public int CartDetId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int SizeId { get; set; }        
        public decimal Rate { get; set; }
        public string Qty { get; set; }
        public decimal Total { get; set; }
        public int TaxId { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal GrandTotal { get; set; }
    }

    public class CustomerCartDetailsEntities
    {
        public string Contact { get; set; }
        public string Name { get; set; }
        public int CartDetId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ImagePath { get; set; }
        public int SizeId { get; set; }
        public string SizeName { get; set; }
        public decimal Rate { get; set; }
        public string Qty { get; set; }
        public decimal Total { get; set; }
        public int TaxId { get; set; }
        public string TaxName { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal FinalTotal { get; set; }
        public decimal FinalTaxAmount { get; set; }
        public decimal FinalGrandTotal { get; set; }
    }

    public class DeleteCartDataByCustomerEntities
    {
        public string Contact { get; set; }
    }

    public class DeleteCustomerCartItemsEntities
    {
        public int CartDetId { get; set; }
        public int CartId { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public int SizeId { get; set; }
    }
}