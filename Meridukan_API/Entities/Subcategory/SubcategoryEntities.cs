﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.Subcategory
{
    public class SubcategoryEntities
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }

    public class DeleteSubcategory
    {
        public int Id { get; set; }
    }
}