﻿using System;
using System.Collections.Generic;

namespace Meridukan_API.Entities.Customer
{
    public class CustomerEntities
    {
        public int Id { get; set; }
        public string CustomerId { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string Address { get; set; }
        public string PinCode { get; set; }
        public string EmailId { get; set; }
    }

    public class DeleteCustomer
    {
        public string Contact { get; set; }
    }

    public class CustomerOrderEntities
    {
        public int SaleId { get; set; }
        public DateTime SaleDate { get; set; }
        public string Date { get; set; }
        public string SaleNo { get; set; }
        public int SalesmanId { get; set; }
        public string SalesmanNo { get; set; }
        public string CustomerName { get; set; }
        public string Contact { get; set; }
        public decimal Total { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal Discount { get; set; }
        public decimal PendAmount { get; set; }
        public decimal ReceiveAmount { get; set; }
        public decimal PendingAmount { get; set; }
        public decimal Change { get; set; }
        public decimal Cash { get; set; }
        public string PaymentOption { get; set; }
        public int UserId { get; set; }
        public int OrderStatusId { get; set; }
        public List<CustomerOrderDetailEnitities> orderDetails { get; set; }
    }

    public class CustomerOrderDetailEnitities
    {
        public int SaleDetId { get; set; }
        public int SaleId { get; set; }
        public string SaleNo { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int SubcategoryId { get; set; }
        public string SubCategoryName { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ImagePath { get; set; }
        public int SizeId { get; set; }
        public string Size { get; set; }
        public string Unit { get; set; }
        public string SaleQty { get; set; }
        public decimal SaleRate { get; set; }
        public int TaxId { get; set; }
        public string TaxName { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Total { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal Discount { get; set; }
    }

    public class CustomerAddressEntities
    {
        public int Id { get; set; }
        public string Contact { get; set; }
        public string PinCode { get; set; }
        public string Address { get; set; }
        public bool? IsDefault { get; set; }
    }

    public class CustomerReportEntities
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string EmailId { get; set; }
        public string Address { get; set; }
        public int Totalorders { get; set; }
        public decimal Totalspend { get; set; }
    }
}