﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities
{
    public class Common
    {
    }

    public class WebAPIResponse
    {
        public string Message { get; set; }
        public string Description { get; set; }
        public string Error { get; set; }
        public object Data { get; set; }
        public WebAPIResponse()
        {
            Message = "failure";
            Description = "";
            Error = "";
            Data = new { };
        }
    }
}