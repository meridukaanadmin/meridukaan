﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.OrderStatus
{
    public class OrderStatusEntities
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}