﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.Color
{
    public class ColorEntities
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class DeleteColor
    {
        public int Id { get; set; }        
    }
}