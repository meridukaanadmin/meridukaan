﻿namespace Meridukan_API.Entities.Store
{
    public class StoreEntities
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
    }

    public class DeleteStore
    {
        public int Id { get; set; }
    }
}