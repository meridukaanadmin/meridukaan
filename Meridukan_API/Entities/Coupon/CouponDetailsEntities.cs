﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.Coupon
{
    public class CouponDetailsEntities
    {
    }

    public class CouponIncludeProductEntities
    {
        public int Id { get; set; }
        public int CouponId { get; set; }
        public int ProductId { get; set; }
        public int PrdAttributeId { get; set; }
    }

    public class CouponExcludeProductEntities
    {
        public int Id { get; set; }
        public int CouponId { get; set; }
        public int ProductId { get; set; }
        public int PrdAttributeId { get; set; }
    }

    public class CouponIncludeCategoryEntities
    {
        public int Id { get; set; }
        public int CouponId { get; set; }
        public int CategoryId { get; set; }
    }

    public class CouponExcludeCategoryEntities
    {
        public int Id { get; set; }
        public int CouponId { get; set; }
        public int CategoryId { get; set; }
    }
}