﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.Coupon
{
    public class CouponEntities
    {
        public int Id { get; set; }
        public string CouponCode { get; set; }
        public int DiscountTypeId { get; set; }
        public string DiscountType { get; set; }
        public decimal CouponAmt { get; set; }
        public bool IsfreeShipping { get; set; }
        public DateTime ExpiryDate { get; set; }
        public decimal MinimumSpend { get; set; }
        public decimal MaximumSpend { get; set; }
        public List<CouponProductsEnitites> couponIncludeProductList { get; set; }
        public List<CouponProductsEnitites> couponExcludeProductList { get; set; }
        public List<CouponCategoryEntities> couponIncludeCategoryList { get; set; }
        public List<CouponCategoryEntities> couponExcludeCategoryList { get; set; }
    }

    public class CouponProductsEnitites
    {
        public int Id { get; set; }
        public int CouponId { get; set; }
        public int ProductId { get; set; }
        public int PrdAttributeId { get; set; }
    }

    public class CouponCategoryEntities
    {
        public int Id { get; set; }
        public int CouponId { get; set; }
        public int CategoryId { get; set; }
    }

    public class DeleteCoupon
    {
        public int Id { get; set; }
    }
}