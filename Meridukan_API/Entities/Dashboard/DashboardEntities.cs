﻿namespace Meridukan_API.Entities.Dashboard
{
    public class DashboardEntities
    {
        public int TotalStores { get; set; }
        public int TotalSuppliers { get; set; }
        public int TotalCustomers { get; set; }
        public int TotalProducts { get; set; }
        public decimal TotalPurchase { get; set; }
        public decimal TotalSale { get; set; }
    }
}