﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.Brand
{
    public class BrandEntities
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
    }

    public class AddorUpdateBrand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
    }

    public class DeleteBrand
    {
        public int Id { get; set; }
    }
}