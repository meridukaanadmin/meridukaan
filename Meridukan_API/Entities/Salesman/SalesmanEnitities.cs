﻿namespace Meridukan_API.Entities.Salesman
{
    public class SalesmanEnitities
    {
        public int Id { get; set; }
        public string SalesmanNo { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
    }

    public class DeleteSalesman
    {
        public int Id { get; set; }
    }
}