﻿using System;

namespace Meridukan_API.Entities.Category
{
    public class CategoryEntities
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
    }
}