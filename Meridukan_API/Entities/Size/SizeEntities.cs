﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.Size
{
    public class SizeEntities
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class DeleteSize
    {
        public int Id { get; set; }
    }
}