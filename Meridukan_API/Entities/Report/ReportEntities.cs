﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.Report
{
    public class ReportEntities
    {

    }

    public class TransactionReportEntities
    {
        public int TransId { get; set; }
        public DateTime date { get; set; }
        public string Type { get; set; }
        public string Reference { get; set; }
        public int ReferenceId { get; set; }
        public string Supplier { get; set; }
        public string Customer { get; set; }
        public decimal Amount { get; set; }
    }

    public class OrderReportEntities
    {        
        public DateTime date { get; set; }
        public decimal GrossSale { get; set; }
        public decimal GrossPurchase { get; set; }
        public int OrderPlaced { get; set; }
        public int ItemsPurchased { get; set; }
    }
}