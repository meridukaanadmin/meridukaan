﻿namespace Meridukan_API.Entities.Role
{
    public class RoleEntities
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class DeleteRole
    {
        public int Id { get; set; }        
    }
}