﻿using System;
using System.Collections.Generic;

namespace Meridukan_API.Entities.Sale
{
    public class SaleEntities
    {
        public int SaleId { get; set; }
        public DateTime SaleDate { get; set; }
        public string Date { get; set; }
        public string SaleNo { get; set; }
        public int SalesmanId { get; set; }
        public string SalesmanNo { get; set; }
        public string CustomerName { get; set; }
        public string Contact { get; set; }
        public decimal Total { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal Discount { get; set; }
        public decimal PendAmount { get; set; }
        public decimal ReceiveAmount { get; set; }
        public decimal PendingAmount { get; set; }
        public decimal Change { get; set; }
        public decimal Cash { get; set; }
        public string PaymentOption { get; set; }
        public int UserId { get; set; }
        public int OrderStatusId { get; set; }
        public List<SaleDetailEnitities> saleDetails { get; set; }
    }

    public class SaleDetailEnitities
    {
        public int SaleDetId { get; set; }
        public int SaleId { get; set; }
        public string SaleNo { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int SubcategoryId { get; set; }
        public string SubCategoryName { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ImagePath { get; set; }
        public int SizeId { get; set; }
        public string Size { get; set; }
        public string Unit { get; set; }
        public string SaleQty { get; set; }
        public decimal SaleRate { get; set; }       
        public int TaxId { get; set; }
        public string TaxName { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Total { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal Discount { get; set; }
    }

    public class DeleteSO
    {
        public int SaleId { get; set; }
    }

    public class SaleRateEntities
    {
        public string ProductName { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public int ProductId { get; set; }
        public int SizeId { get; set; }
        public int TaxId { get; set; }
        public string TaxName { get; set; }
        public decimal TaxValue { get; set; }
        public decimal SaleRate { get; set; }
        public string Stock { get; set; }
    }

    public class OrderStatusEntities
    {   
        public int SaleId { get; set; }
        public int OrderStatusId { get; set; }
    }

    public class AutocompleteSearchEntities
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}