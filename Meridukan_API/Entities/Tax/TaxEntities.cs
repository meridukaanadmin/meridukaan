﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.Tax
{
    public class TaxEntities
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Perc { get; set; }
    }
     
    public class AddorUpdateTax
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Perc { get; set; }
    }

    public class DeleteTax
    {
        public int Id { get; set; }
    }
}