﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.GRN
{
    public class GRNEntities
    {
        public int UserId { get; set; }
        public int GrnId { get; set; }
        public string GrnNo { get; set; }
        public DateTime GrnDate { get; set; }
        public int PoId { get; set; }
        public DateTime PoDate { get; set; }
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string SupplierInvNo { get; set; }
        public decimal SupplierInvAmt { get; set; }
        public DateTime SupplierInvDate { get; set; }
        public string City { get; set; }
        public string PoNo { get; set; }
        public int TransportId { get; set; }
        public string LR_No { get; set; }
        public DateTime LR_Date { get; set; }
        public string No_of_Packages { get; set; }
        public decimal Total { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal PendAmount { get; set; }
        public decimal RecAmount { get; set; }
        public decimal Discount { get; set; }
        public List<GRNDetailsEntities> GRNDetails { get; set; }

    }
    public class GRNDetailsEntities
    {  
        public int GrnDetId { get; set; }
        public int GrnId { get; set; }
        public string GrnNo { get; set; }
      
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int SubcategoryId { get; set; }
        public string SubCategoryName { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int SizeId { get; set; }
        public string Size { get; set; }
        public int ColorId { get; set; }
        public string Color { get; set; }
        public string Unit { get; set; }        
        public int TaxInclusive { get; set; }
        public string Qty { get; set; }
        public decimal Rate { get; set; }
        public string PurchaseQty { get; set; }
        public decimal PurchaseRate { get; set; }
        public string RecieveQty { get; set; }
        public decimal MRP { get; set; }
        public decimal Discount { get; set; }
        public string Expiry { get; set; }
        public decimal SaleRate { get; set; }
        public int TaxId { get; set; }
        public string TaxName { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal Total { get; set; }
        public decimal GrandAmount { get; set; }
    }
    public class DeleteGRN
    {
        public int GrnId { get; set; }
    }
}