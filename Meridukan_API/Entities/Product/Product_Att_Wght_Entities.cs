﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.Product
{
    public class Product_Att_Wght_Entities
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public decimal Weight { get; set; }
        public int UnitId { get; set; }
    }
}