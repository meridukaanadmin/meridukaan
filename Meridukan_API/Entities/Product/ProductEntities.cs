﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.Product
{
    public class ProductEntities
    {
        public int Id { get; set; } 
        public string ProductName { get; set; }
        public string ShortDescription { get; set; }
        public string ProductDescription { get; set; }
        public string Tags { get; set; }
        public int UserId { get; set; }
        public string Barcode { get; set; }
        public int CategoryId { get; set; }
        public int SubCategoryId { get; set; }
        public int BrandId { get; set; }
        public List<ProductAttributes> ProductAttributes { get; set; }
    }
    public class AddProductModel
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string ShortDescription { get; set; }
        public string ProductDescription { get; set; }
        public string Tags { get; set; }
        public int UserId { get; set; }
        public string Barcode { get; set; }
        public int CategoryId { get; set; }
        public int SubCategoryId { get; set; }
        public int BrandId { get; set; }

        public float Weight { get; set; }
        public float Length { get; set; }
        public float Height { get; set; }
        public float Width { get; set; }

        public List<ProductAttributes> ProductAtributes { get; set; }
    }
    public class ProductAttributes
    {
        public int Id { get; set; }
        public int PrAttribute_Id { get; set; }
        public string SKU { get; set; }

        public string ImageName { get; set; }
        public string ImagePath { get; set; }

        public string Tags { get; set; }
        public decimal MRP { get; set; }
        public decimal DiscountPerc { get; set; }
        public decimal DiscountPrice { get; set; }

        public decimal SalePrice { get; set; }
        public DateTime SaleStart { get; set; }
        public DateTime SaleEnd { get; set; }
        public int StockQty { get; set; }
        public int MaximumOrderQty { get; set; }
        public int MinimumStockQty { get; set; }

        public bool IsAllowOrder { get; set; }
        public bool IsPublished { get; set; }
         
        public int PrColorId { get; set; }
        public string PrColor { get; set; }

        public int PrSizeId { get; set; }
        public string PrSize { get; set; }

        public string Barcode { get; set; }
        public bool IsAllowNegativeStock { get; set; }

    }

    public class SaleProductAttributes
    {
        public int PrAttributeId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int SizeId { get; set; }
        public string SizeName { get; set; }
        public string Barcode { get; set; }
    }

    public class DeleteProductEntities
    {   
        public int ProductId { get; set; }     
    }

}