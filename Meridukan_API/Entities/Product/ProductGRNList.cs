﻿using System;

namespace Meridukan_API.Entities.Product
{
    public class ProductGRNListEntities
    {
        public int GrnId { get; set; }
        public string GrnNo { get; set; }
        public DateTime GrnDate { get; set; }
        public int ProductId { get; set; }
        public int SizeId { get; set; }
        public string Size { get; set; }
        public string ReceiveQty { get; set; }
        public string SaleQty { get; set; }
        public string Stock { get; set; }
    }
}