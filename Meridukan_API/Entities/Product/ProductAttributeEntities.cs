﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.Product
{
    public class ProductAttributeEntities
    {                                         
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int ColorId { get; set; }
        public int Size { get; set; }
        public int WeightageId { get; set; }
        public string Tags { get; set; }
        public decimal RegularPrice { get; set; }
        public decimal SalePrice { get; set; }
        public decimal DiscountPerc { get; set; }
        public decimal DiscountPrice { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public int MaximumOrderQty { get; set; }
        public int MinimumStockQty { get; set; }
        public decimal NewPrice { get; set; }
        public decimal OldPrice { get; set; }
    }
}