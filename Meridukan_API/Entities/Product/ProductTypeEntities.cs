﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.Product
{
    public class ProductTypeEntities
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}