﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meridukan_API.Entities.User
{
    public class UserLoginEntities
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string GSTNumber { get; set; }
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public DateTime LastLoginDate { get; set; }
        public int MainUserId { get; set; }

        public List<AccessRoleModel> Roles { get; set; }

    }
    public class AccessRoleModel
    {
        public bool IsSelected { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class LoginEntity
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class DeleteUser
    {
        public int UserId { get; set; }        
    }
}