﻿namespace Meridukan_API.Entities.Transport
{
    public class TransportEntities
    {
        public int Id { get; set; }
        public string Partner { get; set; }
        public string LR_No { get; set; }
        public string Description { get; set; }
    }

    public class DeleteTransport
    {
        public int Id { get; set; }
    }
}